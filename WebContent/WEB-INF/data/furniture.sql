/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50542
Source Host           : localhost:3306
Source Database       : furniture

Target Server Type    : MYSQL
Target Server Version : 50542
File Encoding         : 65001

Date: 2015-03-09 21:15:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `bankname` varchar(50) DEFAULT NULL COMMENT '银行名称',
  `cardid` varchar(50) DEFAULT NULL COMMENT '银行卡号',
  `cardname` varchar(50) DEFAULT NULL COMMENT '账户名称',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dingdan
-- ----------------------------
DROP TABLE IF EXISTS `dingdan`;
CREATE TABLE `dingdan` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `ddnum` varchar(50) DEFAULT NULL COMMENT '订单号',
  `userid` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `username` varchar(100) DEFAULT NULL COMMENT '业主名',
  `ddtime` varchar(20) DEFAULT NULL COMMENT '订单时间',
  `yujifahuotime` varchar(20) DEFAULT NULL COMMENT '预计发货时间',
  `fahuotime` varchar(20) DEFAULT NULL COMMENT '发货时间',
  `huohao` varchar(100) DEFAULT NULL COMMENT ' 货号',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `ddstatus` varchar(20) DEFAULT NULL COMMENT '订单状态',
  `info` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `yezhu` varchar(50) DEFAULT NULL COMMENT '业主',
  `excelinfo` varchar(200) DEFAULT NULL COMMENT 'excel版订单详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `norder` int(11) DEFAULT NULL COMMENT '显示顺序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息通知';

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `dengjitime` varchar(20) DEFAULT NULL COMMENT '登记时间',
  `daozhangtime` varchar(20) DEFAULT NULL COMMENT '到账时间',
  `shishoumoney` decimal(10,0) DEFAULT NULL COMMENT '实收金额',
  `huokuanmoney` decimal(10,0) DEFAULT NULL COMMENT '货款金额',
  `shouxufei` decimal(10,0) DEFAULT NULL COMMENT '手续费',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `userid` varchar(32) DEFAULT NULL COMMENT '业主ID',
  `username` varchar(255) DEFAULT NULL COMMENT '业主名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `userid` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话号码',
  `phonesec` varchar(50) DEFAULT NULL COMMENT '对单电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq号码',
  `other` varchar(500) DEFAULT NULL COMMENT '备注',
  `addressto` varchar(200) DEFAULT NULL COMMENT '货到地区',
  `address` varchar(200) DEFAULT NULL COMMENT '所在地',
  `wuliu` varchar(50) DEFAULT NULL COMMENT '要求物流',
  `utype` varchar(50) DEFAULT NULL COMMENT '用户类别:1管理员，2普通用户',
  `dianmianname` varchar(50) DEFAULT NULL COMMENT '店面名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
