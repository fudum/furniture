/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50542
Source Host           : localhost:3306
Source Database       : furniture

Target Server Type    : MYSQL
Target Server Version : 50542
File Encoding         : 65001

Date: 2015-06-09 12:18:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `porder` int(255) DEFAULT NULL COMMENT '显示顺序',
  `flag` varchar(2) DEFAULT NULL,
  `piclink` varchar(200) DEFAULT NULL COMMENT '是否显示',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='首页广告';

-- ----------------------------
-- Records of advertisement
-- ----------------------------
INSERT INTO `advertisement` VALUES ('A20150604184001575', 'advertisement/081b1a365b114231a47288961ac95fbd.jpg', '1', null, '', '2015-06-04 18:40:01');
INSERT INTO `advertisement` VALUES ('A20150604185116257', 'advertisement/beee2d8e1f4146fe90e53e23d835edbf.jpg', '2', null, '', '2015-06-04 18:51:16');

-- ----------------------------
-- Table structure for baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `baseinfo`;
CREATE TABLE `baseinfo` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `fType` varchar(50) DEFAULT NULL COMMENT '类别',
  `content` text COMMENT '内容',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='关于我们、联系我们、人才招聘';

-- ----------------------------
-- Records of baseinfo
-- ----------------------------
INSERT INTO `baseinfo` VALUES ('B20150525074203880', '关于我们', '关于我们&nbsp;&nbsp;&nbsp;&nbsp;', '2015-05-26 19:40:13');
INSERT INTO `baseinfo` VALUES ('B20150525074203881', '人才招聘', '人材招聘', '2015-05-26 19:40:37');
INSERT INTO `baseinfo` VALUES ('B20150525074203882', '联系我们', '联系我们', '2015-05-26 19:40:47');

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `bankname` varchar(50) DEFAULT NULL COMMENT '银行名称',
  `cardid` varchar(50) DEFAULT NULL COMMENT '银行卡号',
  `cardname` varchar(50) DEFAULT NULL COMMENT '账户名称',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card
-- ----------------------------
INSERT INTO `card` VALUES ('C20150307174039708', '中国银行', '6222023202009820020', '于风建', '95566.png', '2015-03-07 17:40:39');
INSERT INTO `card` VALUES ('C20150307174039709', '中国银行', '6222023202009825002', '于风建', '95566.png', '2015-03-07 17:40:39');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `plevel` varchar(50) DEFAULT NULL COMMENT '显示级别，1,2',
  `shunxu` int(11) DEFAULT NULL COMMENT '显示顺序',
  `parentId` varchar(32) DEFAULT NULL COMMENT '父节点id',
  `parentName` varchar(50) DEFAULT NULL COMMENT '父节点名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品类别';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('CO20150527103404206', '彩饰板橱柜门板', '1', null, null, null, '2015-05-27 10:34:04');
INSERT INTO `category` VALUES ('CO20150527113554866', 'UV烤柒橱柜门板', '1', null, null, null, '2015-05-27 11:35:54');
INSERT INTO `category` VALUES ('CT20150527105607710', '彩饰田园系列2', '2', '1', 'CO20150527113554866', 'UV烤柒橱柜门板', '2015-05-27 11:36:07');
INSERT INTO `category` VALUES ('CT20150527205817709', 'test二级', '2', '2', 'CO20150527113554866', 'UV烤柒橱柜门板', '2015-05-27 20:58:17');
INSERT INTO `category` VALUES ('CT20150604071243182', '彩饰板橱', '2', '1', 'CO20150527103404206', '彩饰板橱柜门板', '2015-06-04 07:12:43');

-- ----------------------------
-- Table structure for dingdan
-- ----------------------------
DROP TABLE IF EXISTS `dingdan`;
CREATE TABLE `dingdan` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `ddnum` varchar(50) DEFAULT NULL COMMENT '订单号',
  `userid` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `username` varchar(100) DEFAULT NULL COMMENT '业主名',
  `ddtime` varchar(20) DEFAULT NULL COMMENT '订单时间',
  `yujifahuotime` varchar(20) DEFAULT NULL COMMENT '预计发货时间',
  `fahuotime` varchar(20) DEFAULT NULL COMMENT '发货时间',
  `huohao` varchar(100) DEFAULT NULL COMMENT ' 货号',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `ddstatus` varchar(20) DEFAULT NULL COMMENT '订单状态',
  `info` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `yezhu` varchar(50) DEFAULT NULL COMMENT '业主',
  `excelinfo` varchar(200) DEFAULT NULL COMMENT 'excel版订单详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dingdan
-- ----------------------------
INSERT INTO `dingdan` VALUES ('D20150309200700167', '1', 'U20150307173538834', '于风建', '2015-03-09 20:07:00', '2015-03-09', '2015-03-09', '1', '1.00', '待生产', '', '2015-03-09 20:38:32', '1', 'dingdan/4d66949a959b4d67b9a221b4b60ac016.xlsx');

-- ----------------------------
-- Table structure for lianxi
-- ----------------------------
DROP TABLE IF EXISTS `lianxi`;
CREATE TABLE `lianxi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `scphone` varchar(50) DEFAULT NULL COMMENT '生产中心',
  `xfphone` varchar(50) DEFAULT NULL COMMENT '消费中心',
  `zhanxianphone` varchar(50) DEFAULT NULL COMMENT '专线',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `longitude` varchar(50) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(50) DEFAULT NULL COMMENT '纬度',
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='联系方式';

-- ----------------------------
-- Records of lianxi
-- ----------------------------
INSERT INTO `lianxi` VALUES ('1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of logistics
-- ----------------------------
INSERT INTO `logistics` VALUES ('L20150307173440498', '申通物流', '申通物流', '2015-03-07 17:34:40');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `norder` int(11) DEFAULT NULL COMMENT '显示顺序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息通知';

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('C20150307202448528', '老婆我爱你，我要给你聊天', null, '2015-03-07 20:38:45');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `dengjitime` varchar(20) DEFAULT NULL COMMENT '登记时间',
  `daozhangtime` varchar(20) DEFAULT NULL COMMENT '到账时间',
  `shishoumoney` decimal(10,0) DEFAULT NULL COMMENT '实收金额',
  `huokuanmoney` decimal(10,0) DEFAULT NULL COMMENT '货款金额',
  `shouxufei` decimal(10,0) DEFAULT NULL COMMENT '手续费',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `userid` varchar(32) DEFAULT NULL COMMENT '业主ID',
  `username` varchar(255) DEFAULT NULL COMMENT '业主名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES ('P20150307174319825', '中国银行5588', '2015-03-07 17:43:07', '2015-03-07 17:43:15', '5', '5', '5', '', '2015-03-07 17:43:19', 'U20150307173538834', '于风建');
INSERT INTO `payment` VALUES ('P20150307174413501', '中国银行5588', '2015-03-07 17:44:05', '2015-03-07 17:44:10', '5', '5', '5', '5', '2015-03-07 17:44:13', 'U20150307174358779', '王毅');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `pid` varchar(50) DEFAULT NULL COMMENT '产品编号',
  `name` varchar(50) DEFAULT NULL COMMENT '产品名称',
  `guige` varchar(50) DEFAULT NULL COMMENT '产品规格',
  `categoryid` varchar(32) DEFAULT NULL COMMENT '产品类别id',
  `categoryname` varchar(50) DEFAULT NULL COMMENT '产品类别名称',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `info` text COMMENT '产品说明',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `clicknum` int(11) DEFAULT NULL COMMENT '点击数',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品';

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '001', '产品1', '001', 'CT20150604071243182', '彩饰板橱', '好产品', '', '2015-06-04 13:51:22', null, 'product/3450052f5fcd42348a3a1c860c3996bf.jpg');
INSERT INTO `product` VALUES ('10', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('11', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('12', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('13', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('14', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('15', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('16', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('2', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('3', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('4', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('5', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('6', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('7', '001', '产品1', '001', 'CO20150527103404206', '彩饰板橱柜门板', '好产品', '', '2015-05-28 19:34:42', null, 'product/a3e704024e1e450ba80cd6b50abad0f8.jpg');
INSERT INTO `product` VALUES ('P20150603192202565', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p3', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p4', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p5', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p6', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p7', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p8', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');
INSERT INTO `product` VALUES ('p9', '002', 'M-13樱花灰', 'M-13樱花灰', 'CT20150527205817709', 'test二级', '这是一个不错的产品', '', '2015-06-03 19:22:02', null, '');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `userid` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话号码',
  `phonesec` varchar(50) DEFAULT NULL COMMENT '对单电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq号码',
  `other` varchar(500) DEFAULT NULL COMMENT '备注',
  `addressto` varchar(200) DEFAULT NULL COMMENT '货到地区',
  `address` varchar(200) DEFAULT NULL COMMENT '所在地',
  `wuliu` varchar(50) DEFAULT NULL COMMENT '要求物流',
  `utype` varchar(50) DEFAULT NULL COMMENT '用户类别:1管理员，2普通用户',
  `dianmianname` varchar(50) DEFAULT NULL COMMENT '店面名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('21232F297A57A5A743894A0E4A801FC3', 'admin', 'admin', '21232F297A57A5A743894A0E4A801FC3', null, null, null, null, null, null, null, null, '1', null, null);
INSERT INTO `user` VALUES ('U20150307173538834', '13871043702', '于风建', '666666', null, '13871043702', '13871043702', '974822058', '', '武汉市', '武汉市', '申通物流', '2', '武汉理工', '2015-03-07 17:35:38');
INSERT INTO `user` VALUES ('U20150307174358779', '13949902992', '王毅', '666666', null, '1343434343', '1343434343', '974822058', '', '武汉市', '武汉市', '申通物流', '2', '理工大学', '2015-03-07 17:43:58');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `path` varchar(500) DEFAULT NULL COMMENT '视频路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频';

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', 'XOTY2NDQxNDYw', '2015-06-04 18:35:12');

-- ----------------------------
-- Table structure for xinwen
-- ----------------------------
DROP TABLE IF EXISTS `xinwen`;
CREATE TABLE `xinwen` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `title` varchar(200) DEFAULT NULL COMMENT '新闻标题',
  `category` varchar(50) DEFAULT NULL COMMENT '新闻类别',
  `content` text COMMENT '新闻内容',
  `auther` varchar(50) DEFAULT NULL COMMENT '新闻作者',
  `ip` varchar(50) DEFAULT NULL COMMENT '发布的ip',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `recommend` varchar(50) DEFAULT NULL COMMENT '是否推荐到首页',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xinwen
-- ----------------------------
INSERT INTO `xinwen` VALUES ('1', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('10', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('11', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('12', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('2', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('3', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('4', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('5', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('6', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('7', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('8', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('9', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('X20150525074203880', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('X20150525074203881', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('X20150525074203882', '新闻1', null, '<p>\n	新闻1\n</p>\n<p>\n	<img src=\"/upload/attached/image/20150525/20150525080402_880.png\" alt=\"\" />\n</p>', null, null, '2015-05-25 08:04:04', '2');
INSERT INTO `xinwen` VALUES ('X20150531163034826', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('X20150531163034827', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
INSERT INTO `xinwen` VALUES ('X20150531163034828', '【好教师联盟】课外培优那些事儿——第6章', null, '<a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a><a href=\"http://www.hjslm.com/news/info.html?id=X20150523164337828\" target=\"_blank\" class=\"padleft\">【好教师联盟】课外培优那些事儿——第6章</a>', null, null, '2015-05-31 16:30:34', '2');
