<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>武汉金喜装饰材料厂-新闻详情</title>
<style>
.article{width:1200px;font-family: arial,'Hiragino Sans GB','Microsoft Yahei','微软雅黑','宋体',\5b8b\4f53,Tahoma,Arial,Helvetica,STHeiti;}
#artical h2 {font-size: 34px;line-height: 44px;font-weight: 400;color: #000;padding: 10px 0 20px;}
#artical .infos {font-size: 12px;color: #999;border-bottom: 1px solid #eaeaea;padding-bottom: 10px;line-height: 28px;margin-bottom: 15px;}
</style>
</head>
<body>
<div class="header">
		<div class="header_top">
			<div class="wrap">
				<div class="logo">
					<a href="<%=pre%>/front/index<%=suf%>"><h1>
							<span class="black">武汉金喜装饰材料厂</span>
						</h1></a>
				</div>
				<div class="menu">
					<ul>
						<li><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
						<li><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
						<li  class="active"><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
						<li><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
						<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>

			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="top-head">
			<ul>
				<li><a href="<%=pre%>">首页 / </a></li>
				<li><a href="#"><span>新闻中心</span></a></li>
			</ul>
		</div>
		<div id="artical" class="article">
			<h2>${bean.title }</h2>
			<div class="infos">
				<span class="tm">${bean.inserttime }</span>
			</div>
			<div class="content">
				${bean.content }
			</div>
			<div class="infos" style="margin:20px 0px">
				
			</div>
		</div>
	</div>
	<div style="clear:both;"></div>
	<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>