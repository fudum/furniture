<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=css%>/web/nav.css">
<script type="text/javascript" src="<%=js%>/nav.js"></script>
<title>武汉金喜装饰材料厂-产品分类</title>
<style>
.fudum ul li h4 a.current {  color: #8caf00;}
</style>
<script>
$(document).ready(function(){
	navList(12);
	var id = $('#o_categoryid').val();
});

function changePage(page) {
	$("#o_page").val(page);
	myAjax();
}

function changeParentCategory(categoryid){
	$("#o_page").val(1);
	$("#o_categoryid").val(categoryid);
	$("#o_categoryname").val(1);
	myAjax();
}

function changeCategory(categoryid){
	$("#o_page").val(1);
	$("#o_categoryid").val(categoryid);
	$("#o_categoryname").val(null);
	myAjax();
}

function myAjax() {
	$("#form").submit();
}
function show_info(p_id) {
	$("#info_"+p_id).toggle();
}
</script>
</head>
<body>
	<div class="header">
		<div class="header_top">
			<div class="wrap">
				<div class="logo">
					<a href="<%=pre%>/front/index<%=suf%>">
						<h1>
							<span class="black">武汉金喜装饰材料厂</span>
						</h1>
					</a>
				</div>
				<div class="menu">
					<ul>
						<li><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
						<li class="active"><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
						<li><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
						<li><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
						<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="top-head">
			<ul>
				<li><a href="<%=pre%>">首页 / </a></li>
				<li><a href="#"><span>产品分类</span></a></li>
			</ul>
		</div>
		<div class="p_p" >
			<div class="p_left">
				<div class="fudum">
					<h3>产品分类</h3>
					<ul id="nav_dot">
						<c:forEach items="${listOne}" var="obj" varStatus="status">
							<li>
								<h4 id="${obj.id }">
									
									<a href="javascript:void();" onclick="changeParentCategory('${obj.id}')" <c:if test="${navId == obj.id}">class="current"</c:if> >${obj.name }</a>
								</h4>
								<span></span>
								
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="p_right">
				<div class="p_right_nav">
					<span>产品<c:if test="${cBean.parentname != null }">&gt;&gt;${cBean.parentname}</c:if>&gt;&gt;${cBean.name}</span>
				</div>
				<input type = "hidden" id="categoryid" value="${navId }"/>
				<div class="p_right_content">
					<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
						<div class="p_r_c_a">
							<div class="p_r_c_l">
								<img alt="" src="<%=upload2%>/${obj.picpath}" style="height:100px;">
							</div>
							<div class="p_r_c_r">
								<p>产品编号：<span>${obj.pid }</span></p>
								<p>产品名称：<span>${obj.name }</span></p>
								<p>产品类别：<span>${obj.categoryname }</span></p>
								<p>产品备注：<span>${obj.beizhu }</span></p>
							</div>
							<div class="p_r_c_rr">
								<a href="<%=pre %>/fproduct/info<%=suf %>?id=${obj.id}" target="_blank">详细信息</a>
							</div>
							<div class="clear"></div>
							<div id="info_${obj.id}" style="display:none">
								<div class="p_info_info">
									产品详情
								</div>
								<div class="p_info_neirong">
									${obj.info }
								</div>
							</div>
						</div>
					</c:forEach>
					<div class="fy_top">
					<form id="form" action="<%=pre%>/fproduct/index<%=suf%>" method="get">
						<input type="hidden" id="o_page" name="curtPage" value="${pager.curtPage }" />
						<input type="hidden" id="o_categoryid" name="categoryid" value="${bean.categoryid }"/>
						<input type="hidden" id="o_categoryname" name = "categoryname" value="${bean.categoryname }"/>
					</form>
					
					<div id="divFenye" style="padding-top: 10px; clear: both;">
						<!-- 分页开始 -->
						<div class="manu">
							<c:if test="${pager.totalPage >0}">
								<!-- prev -->
								<c:choose>
									<c:when test="${pager.curtPage == 1 }">
										<span class="disabled">&lt;上一页</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();"
											onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
									</c:otherwise>
								</c:choose>
								<c:if test="${pager.totalPage < 7 }">
									<c:forEach var="item" begin="1" end="${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
												<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
								<c:if test="${pager.totalPage >= 7 }">
									<c:if test="${pager.curtPage <= 4 }">
										<c:forEach var="item" begin="1" end="5">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
													<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
											...
											<a href="javascript:void();"
											onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
									</c:if>
									<c:if test="${pager.curtPage > 4 }">
										<a href="javascript:void();" onclick="changePage('1')">1</a>
											...
											<c:choose>
											<c:when test="${pager.curtPage < pager.totalPage - 3 }">
												<c:forEach var="item" begin="${pager.curtPage - 2 }"
													end="${pager.curtPage + 2 }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
															<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
														...
														<a href="javascript:void();"
													onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
											</c:when>
											<c:otherwise>
												<c:forEach var="item" begin="${pager.totalPage - 4 }"
													end="${pager.totalPage }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
															<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
									</c:if>
								</c:if>
								<!--next-->
								<c:choose>
									<c:when test="${pager.curtPage == pager.totalPage }">
										<span class="disabled">下一页 &gt;</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();"
											onclick="changePage('${pager.curtPage + 1}')">下一页 &gt; </a>
									</c:otherwise>
								</c:choose>
							</c:if>
						</div>
					</div>
					<!--分页结束-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="clear: both;"></div>
	<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>
