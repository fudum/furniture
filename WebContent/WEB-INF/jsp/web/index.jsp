<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<meta name="renderer" content="webkit">
<head>
	<title>金喜装饰材料厂</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<script>
		$(document).ready(function(){
			$('#camera_wrap_1').camera({
				thumbnails: true,
				height:'200px'
			});
			
			$("#btn_sub").click(function(){
				var userid = $("#userid").val();
				var password = $("#password").val();
				var utype = $("input[name='utype']:checked").val();
				if(utype == 1){
					password = faultylabs.MD5(password);
				}
				if(userid == '' || userid ==null ){
					$("#userid").focus();
					return;
				}
				if(password == '' || password == null){
					$("#password").focus();
					return;
				}
				$.ajax({
					url : "<%=pre%>/home/login<%=suf%>",
					type : 'post',
					dataType : 'html',
					data:{userid:userid,password:password,utype:utype},
					success:function(data, status){
						if(data == 1) {							
							window.location = '<%=pre%>/home/index<%=suf%>';
						}
						else{
							$("#tip").html("用户名或密码错误！");
						}
					},
					error : function(xhr, textStatus,
							errorThrown){
						$("#tip").html("用户名或密码错误！");
					}
				});
			});
			
		});
		
	</script>
	<style>
	.padright {float: right;font-family: Candara,arial;font-size:14px;color:#666}
	.textbox{margin-bottom:5px;padding:8px;display: block;width:222px;border: none;outline: none;color: #444;font-size: 14px;font-family: Arial, Helvetica, sans-serif;border: 1px solid rgba(192, 192, 192, 0.31);-webkit-appearance: none;}
	.about_us{border:1px solid #eaeaea;margin-top:20px;}
	.span_lianxi{color:#3e3e3e;line-height:24px;width:218px;display:block;font-size:14px;font-family:'微软雅黑';border-bottom:1px dashed #8caf00}
	</style>
	
</head>
<body>
	<div class="header">
		<div class="header_top">
			<div class="wrap">		
				<div class="logo">
					<a href="<%=pre%>/front/index<%=suf%>"><h1><span class="black">武汉金喜装饰材料厂</span></h1></a>
				</div>	
				<div class="menu">
					<ul>
						<li class="active"><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
						<li><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
						<li><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
						<li><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
						<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
					</ul>
					<div class="clear"></div>
				</div>						
				<div class="clear"></div>
			</div>
		</div>
		<div class="wrap" style="margin-top:3px;">
			<div style="float:left;height:200px;width:200px;padding-right:3px">
				<img src="<%=upload2%>/${advertise_0.picpath}" style="height:200px;width:200px">
			</div>
			<div class="slider" style="float:left;">						    								     
				<div class="fluid_container">
					<div class="camera_wrap camera_azure_skin" id="camera_wrap_1" >
						<c:forEach items="${advertiseList }" var="obj">
							<div  data-src="<%=upload2%>/${obj.picpath}">  </div>
						</c:forEach>
					</div>
				</div>
				<div class="clear"></div>					       
			</div>
			<div style="float:left;height:200px;width:200px;padding-left:3px">
				<img src="<%=upload2%>/${advertise_1.picpath}" style="height:200px;width:200px">
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>		
	<div class="main">
		<div class="wrap">
			<div class="section group">						
				<div class="col_1_of_3 span_1_of_3">
					<div class="div_more">
						<h3>新闻中心</h3>
						<span>
							<a href="<%=pre%>/fnews/index<%=suf%>" target="_blank">更多></a>
						</span>
					</div>
					<ul>
						<c:forEach items="${pagerXinwen.pageList }" var="obj">
							<li>
								<a href="<%=pre%>/fnews/info<%=suf %>?id=${obj.id}" target="_blank">${obj.title }</a>
								<span class="padright">${fn:substring(obj.inserttime,0,10) }</span>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="col_1_of_3 span_1_of_4">
					<h3>公司简介</h3>
					<div>
					<embed src="${videoBean.path}" allowFullScreen="true" quality="high" width="480" height="360" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>
					</div>
				</div>
				<div class="col_1_of_3 span_1_of_5">
					<h3>用户登入</h3>
					<span>
						<input id="userid" name="userid" type="text" class="textbox" placeholder="用户名">
					</span>
					<span>
						<input id="password" name="password" type="password" class="textbox" placeholder="密码">
					</span>
					<!-- 用户类别-->
					<span style="line-height:30px">
						<input type="radio" name="utype" value="1" checked="ture">管理员
						<input type="radio" name="utype" value="2">普通用户
					</span>
					<span id="tip" style="color:red;font-size:10px;display:block;line-height:25px;"></span>
					<span><input type="button" class="mybutton" value="登入" id="btn_sub"></span>
					
					<div style="border:1px solid #8caf00;padding:10px;margin-top:10px;">
						<h3 style="padding-bottom:5px;">联系方式</h3>
						<span class="span_lianxi">生产中心：${lianxibean.scphone }</span>
						<span class="span_lianxi">服务中心：${lianxibean.xfphone }</span>
						<span class="span_lianxi">渠道专线：${lianxibean.zhanxianphone }</span>
						<span class="span_lianxi">工厂传真：${lianxibean.fax }</span>
						<span class="span_lianxi" style="border-bottom:none;">公司地址：${lianxibean.address }</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="clear:both;"></div>
	<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>

