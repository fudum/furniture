<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>武汉金喜装饰材料厂-新闻中心</title>
<style>
.newslist {
	margin-right: 25px;
	padding-top: 5px;
	width: 700px;
}

.newslist ul, .newslist li {
	list-style: none;
	padding: 0px;
	margin: 0px;
	width: 700px;
}

.newslist li {
	line-height: 32px;
	font-size: 14px;
	background-image: url(<%=images%>/web/sign.jpg);
	background-repeat: no-repeat;
	background-position: 5px 10px;
	_padding-right: 45px;
	width: 99.5%;
	font-family: "微软雅黑";
	float: left;
}

.height30 {
	height: 34px;
}

.padleft {
	float: left
}

.padright {
	float: right;
}

.newslist a {
	font-size: 16px;
	margin-left: 25px;
	color: #787878;
	text-decoration: none;
}

.newslist a:hover {
	text-decoration: underline
}

.myli {
	background-color: #FDFDFD;
	border-bottom-width: 1px;
	border-bottom-color: #CCC;
	border-top-width: 1px;
	border-top-color: #CCC;
	border-top-style: dotted;
	border-bottom-style: dotted;
}
</style>
<script>
	function changePage(page) {
		$("#o_page").val(page);
		myAjax();
	}
	function myAjax() {
		$("#form").submit();
	}
</script>
</head>
<body>
	<div class="header">
		<div class="header_top">
			<div class="wrap">
				<div class="logo">
					<a href="<%=pre%>/front/index<%=suf%>"><h1>
							<span class="black">武汉金喜装饰材料厂</span>
						</h1></a>
				</div>
				<div class="menu">
					<ul>
						<li><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
						<li><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
						<li class="active"><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
						<li><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
						<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="top-head">
			<ul>
				<li><a href="<%=pre%>">首页 / </a></li>
				<li><a href="#"><span>新闻中心</span></a></li>
			</ul>
		</div>
		<div class="newslist">
			<ul>
				<c:forEach items="${pager.pageList }" var="obj" varStatus="status">
					<li
						<c:choose>
							<c:when test="${(status.index mod 2) == 1}">
								class="height30 myli"
							</c:when>
							<c:otherwise>
								class="height30"
							</c:otherwise>
						</c:choose>>
						<a href="<%=pre %>/fnews/info<%=suf%>?id=${obj.id}"
						target="_blank" class="padleft">${obj.title }</a> <span
						class="padright">${obj.inserttime }</span>
					</li>
				</c:forEach>
			</ul>
		</div>
		<form id="form" action="<%=pre%>/fnews/index<%=suf%>" method="get">
			<input type="hidden" id="o_page" name="curtPage"
				value="${pager.curtPage }" />
		</form>
		<div id="divFenye"
			style="padding-top: 10px; clear: both; width: 700px">
			<!-- 分页开始 -->
			<div class="manu">
				<c:if test="${pager.totalPage >0}">
					<!-- prev -->
					<c:choose>
						<c:when test="${pager.curtPage == 1 }">
							<span class="disabled">&lt;上一页</span>
						</c:when>
						<c:otherwise>
							<a href="javascript:void();"
								onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
						</c:otherwise>
					</c:choose>
					<c:if test="${pager.totalPage < 7 }">
						<c:forEach var="item" begin="1" end="${pager.totalPage }">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
									<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
					<c:if test="${pager.totalPage >= 7 }">
						<c:if test="${pager.curtPage <= 4 }">
							<c:forEach var="item" begin="1" end="5">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
										<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
								...
								<a href="javascript:void();"
								onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
						</c:if>
						<c:if test="${pager.curtPage > 4 }">
							<a href="javascript:void();" onclick="changePage('1')">1</a>
								...
								<c:choose>
								<c:when test="${pager.curtPage < pager.totalPage - 3 }">
									<c:forEach var="item" begin="${pager.curtPage - 2 }"
										end="${pager.curtPage + 2 }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
												<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
											...
											<a href="javascript:void();"
										onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
								</c:when>
								<c:otherwise>
									<c:forEach var="item" begin="${pager.totalPage - 4 }"
										end="${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
												<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:if>
					<!--next-->
					<c:choose>
						<c:when test="${pager.curtPage == pager.totalPage }">
							<span class="disabled">下一页 &gt;</span>
						</c:when>
						<c:otherwise>
							<a href="javascript:void();"
								onclick="changePage('${pager.curtPage + 1}')">下一页 &gt; </a>
						</c:otherwise>
					</c:choose>
				</c:if>
			</div>
		</div>
		<!--分页结束-->
	</div>
	<div style="clear:both;"></div>
	<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>