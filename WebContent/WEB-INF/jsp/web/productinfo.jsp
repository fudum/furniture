<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=css%>/web/nav.css">
<script type="text/javascript" src="<%=js%>/nav.js"></script>
<title>武汉金喜装饰材料厂-产品分类</title>
<script>
$(document).ready(function(){
	navList(12);
	var id = $('#o_categoryid').val();
});

function changePage(page) {
	$("#o_page").val(page);
	myAjax();
}

function changeParentCategory(categoryid){
	$("#o_page").val(1);
	$("#o_categoryid").val(categoryid);
	$("#o_categoryname").val(1);
	myAjax();
}

function changeCategory(categoryid){
	$("#o_page").val(1);
	$("#o_categoryid").val(categoryid);
	$("#o_categoryname").val(null);
	myAjax();
}

function myAjax() {
	$("#form").submit();
}
</script>
<style>
.p_info_top{margin:10px 0px;border:1px solid #eaeaea;display:block;}
.p_info_top_l{display:block;width:160px;float:left;}
.p_info_top_l img{padding:3px;margin:3px;border:1px solid #eaeaea;max-height:200px}
.p_info_top_m {float: left;padding: 5px 10px;width:800px;}
.p_info_top_m .name{font-size:24px;font-weight:bold;padding:5px 0px;color:#8caf00}
.p_info_top_m p{padding-right:20px;line-height: 30px;color: #666;font-family: "Microsoft YaHei",微软雅黑;font-size: 16px;}
.p_info_top_m p span {color: #222;padding-right:20px;}
.p_info_info{font-family: 'ambleregular';text-transform: uppercase;padding-bottom:10px;margin:20px 0px;border-bottom:1px solid #eaeaea;font-size:24px;font-weight:bold;color:#F95355}
.p_info_neirong{padding:10px;}
a.pre_bu{background: #fff;padding: 5px 20px;border: solid 1px #c4c2b7;color:#666;border-radius:3px;}
a.pre_bu:hover{background-color:#F95355;color:#fff}
</style>
</head>
<body>
	<div class="header">
		<div class="header_top">
			<div class="wrap">
				<div class="logo">
					<a href="<%=pre%>/front/index<%=suf%>">
						<h1>
							<span class="black">武汉金喜装饰材料厂</span>
						</h1>
					</a>
				</div>
				<div class="menu">
					<ul>
						<li><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
						<li class="active"><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
						<li><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
						<li><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
						<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
					</ul>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="wrap">
		<div class="top-head">
			<ul>
				<li><a href="<%=pre%>">首页 </a> / </li>
				<li><a href="#"><span>产品详情</span></a> / </li>
				<li><a href="<%=pre%>/fproduct/index<%=suf%>">上一步</a></li>
			</ul>
		</div>
		<div>
			<div class="p_info_top">
				<div class="p_info_top_l">
					<a href="<%=upload2%>/${bean.picpath}" target="_blank">
						<img src="<%=upload2%>/${bean.picpath}" />
					</a>
				</div>
				<div class="p_info_top_m">
					<div class="name">产品1</div>
					<p>产品编号：<span>${bean.pid }</span> 产品类别：<span>${bean.categoryname }</span></p>
					<p>产品备注：<span>${bean.beizhu }</span></p>
				</div>
				<div class="clear"></div>
			</div>
			<div>
				<div class="p_info_info">
					产品详情
				</div>
				<div class="p_info_neirong">
					${bean.info }
				</div>
				<div style="text-align:center">
					<a href="<%=pre%>/fproduct/index<%=suf%>" class="pre_bu">返回</a>
				</div>
			</div>
		</div>
	</div>
	<div style="clear: both;"></div>
	<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>
