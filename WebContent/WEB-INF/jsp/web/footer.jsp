<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="footer" style="font-size:13px;">
	<div class="wrapper w1000 clearfix">
		<div class="footer-left" style=" text-align:center; margin:0px auto; margin-top:10px;">
			<a href="#">意见反馈</a> |
			<a href="<%=request.getContextPath()%>/home/index.html" target="_blank">后台管理</a>
		</div>
		<div class="footer-right" style="margin:0px auto; padding:3px 0px 15px 0px; text-align:center;font-family:Arial" >
			Copyright © 2015 武汉金喜装饰材料厂 注册号:420100000482560 
		</div>
	</div>
</div>