<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/web.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>武汉金喜装饰材料厂-经典案例</title>
<style>
.anli{margin-top:20px;}
.anli li{float:left;margin:0px 40px 20px 0px;}
.anliinfo{padding:10px;border:1px solid #eee}
.anliinfo img{cursor:pointer;width:150px;height:200px;}
</style>
</head>
<body>
<div class="header">
	<div class="header_top">
		<div class="wrap">
			<div class="logo">
				<a href="<%=pre%>/front/index<%=suf%>"><h1>
						<span class="black">武汉金喜装饰材料厂</span>
					</h1></a>
			</div>
			<div class="menu">
				<ul>
					<li><a href="<%=pre%>/front/index<%=suf%>">首页</a></li>
					<li><a href="<%=pre%>/fproduct/index<%=suf%>">产品分类</a></li>
					<li><a href="<%=pre%>/fnews/index<%=suf%>">新闻中心</a></li>
					<li class="active"><a href="<%=pre%>/fanli/index<%=suf%>">经典案例</a></li>
					<li><a href="<%=pre%>/fbaseinfo/index<%=suf%>?f=2">关于我们</a></li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<div class="top-head">
		<ul>
			<li><a href="<%=pre%>">首页 / </a></li>
			<li><a href="#"><span>经典案例</span></a></li>
		</ul>
	</div>
	<div class="anli">
		<ul>
			<c:forEach items="${pager.pageList }" var="obj" varStatus="status">
				<li>
					<div class="anliinfo">
		    			<div>
		    				<a href="<%=pre%>/fanliinfo/index<%=suf%>?anli_id=${obj.id}">
		    					<img src="<%=upload2%>/${obj.picpath}">
		    				</a>
		    				
		    			</div>
		    			<div>
		    				${obj.name }
		    			</div>		    			
		   			</div>
				</li>
			</c:forEach>
		</ul>
	</div>
	<form id="form" action="<%=pre%>/fnews/index<%=suf%>" method="get">
		<input type="hidden" id="o_page" name="curtPage"
			value="${pager.curtPage }" />
	</form>
	<div id="divFenye"
		style="padding-top: 10px; clear: both; width: 700px">
		<!-- 分页开始 -->
		<div class="manu">
			<c:if test="${pager.totalPage >0}">
				<!-- prev -->
				<c:choose>
					<c:when test="${pager.curtPage == 1 }">
						<span class="disabled">&lt;上一页</span>
					</c:when>
					<c:otherwise>
						<a href="javascript:void();"
							onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
					</c:otherwise>
				</c:choose>
				<c:if test="${pager.totalPage < 7 }">
					<c:forEach var="item" begin="1" end="${pager.totalPage }">
						<c:choose>
							<c:when test="${pager.curtPage == item }">
								<span class="current">${item }</span>
							</c:when>
							<c:otherwise>
								<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:if>
				<c:if test="${pager.totalPage >= 7 }">
					<c:if test="${pager.curtPage <= 4 }">
						<c:forEach var="item" begin="1" end="5">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
									<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
							...
							<a href="javascript:void();"
							onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
					</c:if>
					<c:if test="${pager.curtPage > 4 }">
						<a href="javascript:void();" onclick="changePage('1')">1</a>
							...
							<c:choose>
							<c:when test="${pager.curtPage < pager.totalPage - 3 }">
								<c:forEach var="item" begin="${pager.curtPage - 2 }"
									end="${pager.curtPage + 2 }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
											<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
										...
										<a href="javascript:void();"
									onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
							</c:when>
							<c:otherwise>
								<c:forEach var="item" begin="${pager.totalPage - 4 }"
									end="${pager.totalPage }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
											<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:if>
				<!--next-->
				<c:choose>
					<c:when test="${pager.curtPage == pager.totalPage }">
						<span class="disabled">下一页 &gt;</span>
					</c:when>
					<c:otherwise>
						<a href="javascript:void();"
							onclick="changePage('${pager.curtPage + 1}')">下一页 &gt; </a>
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>
	</div>
	<!--分页结束-->
</div>
<div style="clear:both;"></div>
<%@include file="/WEB-INF/jsp/web/footer.jsp"%>
</body>
</html>