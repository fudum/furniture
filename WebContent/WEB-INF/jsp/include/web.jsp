<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="framework.base.common.BaseConstants"%>
<%@page import="framework.base.utils.PublicCacheUtil"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String path = request.getContextPath();
String common = path + "/resource/common";
String js = path + "/resource/js";
String css = path + "/resource/css";
String images = path + "/resource/images";
String fonts = path + "/resource/fonts";
String artDialog = path + "/resource/common/artDialog";
String upload = path + "/upload/furniture";
String upload2 = "/upload/furniture";
String kePath = path + "/resource/common/kindeditor-4.1.10";
String suf = ".html";
String pre = path;
%>
<link rel="SHORTCUT ICON" HREF="<%=images%>/logo.ico">
<script type="text/javascript" src="<%=js%>/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=js%>/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<%=js%>/camera.min.js"></script>
<script type="text/javascript" src="<%=js%>/md5.js"></script>

<link rel="stylesheet" type="text/css" href="<%=css%>/web/style.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/web/slider.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/web/fenye.css">

<script type="text/javascript" src="<%=artDialog%>/jquery.artDialog.js?skin=default"></script>
<script type="text/javascript" src="<%=artDialog%>/plugins/iframeTools.js"></script>

<link rel="stylesheet" type="text/css" href="<%=common%>/My97DatePicker/skin/WdatePicker.css">
<script type="text/javascript" src="<%=common%>/My97DatePicker/calendar.js"></script>
<script type="text/javascript" src="<%=common%>/My97DatePicker/WdatePicker.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?7066cf4c9bc402fc7da27015a879408c";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
