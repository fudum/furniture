<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户-银行卡信息</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<style>
.bank{
	float:left;
	margin-bottom:10px;
	margin-right:10px;
}
.a_btn{
	background-color: #2981e4;
	padding:5px 10px;
	color:#fff;
	border:none;
	cursor:pointer;
	width:100px;
}
.a_btn[disabled ="disabled"]{
	background-color: #999;
	width:120px;
	cursor:default;
}
</style>
<script>


//timer处理函数
function SetRemainTime(t_id) {
	var temp = $("#b_" + t_id).val();
	var curCount = temp.substring(0,temp.length-4);
    if (curCount == 0) {                
        window.clearInterval(InterValObj);//停止计时器
        $("#b_" + t_id).removeAttr("disabled");//启用按钮
        $("#b_"+ t_id).val("获取账号");
        $("#s_" + t_id).html("");
    }
    else {
    	curCount--;
        $("#b_" + t_id).val(curCount+"秒后获取");
    }
}

function getCardId(id){
	var userphone = $("#p_" + id).val();
	var isMobile=/^(?:13\d|15\d|18\d)\d{5}(\d{3}|\*{3})$/;
	if(!isMobile.test(userphone)){ //如果用户输入的值不同时满足手机号和座机号的正则
	    alert("请正确填写电话号码，例如:13415764179");  //就弹出提示信息
	    $("#p_" +id).focus();       //输入框获得光标
	    return false;         //返回一个错误，不向下执行
	}
	var InterValObj; //timer变量，控制时间
	var curCount = 120;//当前剩余秒数
	
	$.ajax({
		type:'post',
		url:'<%=pre%>/card/c_getcardid<%=suf%>',
		data:{id:id,userphone:userphone},
		dataType:'json',
		success:function(data,status){
			if(status == 'success'){
				if(data == 0){
					$("#s_"+id).html('发送成功！');
					$("#b_" + id).attr("disabled", "true");
				   	$("#b_" + id).val(curCount + "秒后获取");
				    InterValObj = window.setInterval(SetRemainTime, 1000,id); //启动计时器，1秒执行一次
				}
				else{
					$("#s_"+id).html('发送失败！');
				}
				
			}
			else{
				$("#s_"+id).html('发送失败！');
			}
		},
		error:function(xhr, textStatus, errorThrown){
			art.dialog.tips('发送失败！');
		}
	});
}
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">银行账户</span>
	</div>
</div>
<div class="result-wrap">
	<c:forEach items="${list }" var="obj">
		<table class="insert-tab bank" style='width:350px'>
	        <tbody>
	        	<tr>
	             <th style="width:100px;"><i class="require-red"></i>银行：</th>
	             <td><img src="<%=images%>/bank/${obj.picpath}"></td>
	         </tr>
	         <!-- 
	         <tr>
	             <th><i class="require-red"></i>用户手机号：</th>
	             <td><input id="p_${obj.id }" type="text" value="${user_session.userid}" style="width:120px;" /></td>
	         </tr>
             -->
           	<tr>
                <th><i class="require-red"></i>银行账号：</th>
                <td>
                	<span>6222023202009820020</span>
                </td>
            </tr>
	        </tbody>
	    </table>
	</c:forEach>
    <div style="clear:both;"></div>
</div>
</body>
</html>