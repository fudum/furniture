<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户-修改密码</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var passwordold = $("#passwordold").val();
		var password = $("#password").val();
		var passwordtwo = $("#passwordtwo").val();
		if(passwordold == ''){
			$("#passwordoldtip").html('*请输入当前密码');
			$("#passwordold").focus();
			return;
		}
		if(password == ''){
			$("#passwordtip").html('*请输入新密码');
			$("#password").focus();
			return;
		}
		if(passwordtwo == ''){
			$("#passwordtwotip").html('*请输入确认新密码');
			$("#passwordtwo").focus();
			return;
		}
		
		if(passwordtwo != password){
			$("#passwordtwotip").html('*两次输入新密码不一致');
			$("#passwordtwo").focus();
			return;
		}
		
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_user/c_check<%=suf%>',
			data:{password:passwordold},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					$.ajax({
						type:'post',
						url:'<%=pre%>/b_user/c_change<%=suf%>',
						data:{password:password},
						dataType:'json',
						success:function(data,status){
							if(data.ret == 1){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">修改成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										window.location.href="<%=pre%>/b_user/c_changepwd<%=suf%>";
									}
									
								}).show();		
							}
							else{
								$("#rettip").html('*修改失败');
							}
						},
						error:function(xhr, textStatus, errorThrown){
							
						}
					});
				}
				else{
					$("#rettip").html('*当前密码错误');
					$("#passwordold").focus();
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	$("#passwordold").live("click",function(){
		$("#passwordoldtip").html('');
		$("#rettip").html('');
	});
	$("#password").live("click",function(){
		$("#passwordtip").html('');
	});
	$("#passwordtwo").live("click",function(){
		$("#passwordtwotip").html('');
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">修改密码</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab" width="100%">
            <tbody>
            	<tr>
                	<th width="120"><i class="require-red">*</i>当前密码：</th>
	                <td>
	                   <input class="common-text required" id="passwordold" size="30" value="" type="password">
	                   <i class="require-red" id="passwordoldtip"></i>
	                </td>
           	 	</tr>
           	 	<tr>
                	<th><i class="require-red">*</i>新密码：</th>
	                <td>
	                   <input class="common-text required" id="password" size="30" value="" type="password">
	                   <i class="require-red" id="passwordtip"></i>
	                </td>
           	 	</tr>
                <tr>
                    <th><i class="require-red">*</i>确认新密码：</th>
                    <td>
                        <input class="common-text required" id="passwordtwo" size="30" value="" type="password">
                        <i class="require-red" id="passwordtwotip"></i>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="更改" type="button" id="btn_add">
                        <i class="require-red" id="rettip"></i>
                    </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>