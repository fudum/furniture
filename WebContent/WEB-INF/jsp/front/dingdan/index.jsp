<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户-订单管理</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
function dingdaninfo(info){
	art.dialog.open('<%=pre%>/dingdan/c_info<%=suf%>?info=' + info,{
		titile:'订单详情',
		width:800,
		height:400
	});
}
function toDownload(storeName,realName,type){
	if(storeName == ''){
		if(type == 1){
			alert("订单不存在！");
		}
		else{
			alert("附件不存在！");
		}
		return;
	}
	document.getElementById('realName').value = realName;
	document.getElementById('storeName').value = storeName;
	document.getElementById('downForm').submit();
}

</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">我的订单</span>
	</div>
</div>
<div class="search-wrap">
	<div>
		<form id="downForm" action="<%=pre%>/b_dingdan/download<%=suf%>" method="post">
			<input type="hidden" name="storeName" id="storeName" value="" /> 
			<input type="hidden" name="realName" id="realName" value="订单详情" />
		</form>
	</div>	
    <div class="search-content">
        <form action="<%=pre %>/b_dingdan/c_index<%=suf %>" method="post">
            <table class="search-tab">
                <tr>
                    <th width="80">查询日期:</th>
                    <td>
                        <input value="${year}" id="year" name="year" style="width:80px" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy年'})"/>
                    	<input value="${month}" id="month" name="month" style="width:80px" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'MM月'})"/>
                    	<input class="btn btn-primary btn2" name="sub" value="查询" type="submit" id="btn_search">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<table class="result-tab">
	         <tr>
	             <th>序号</th>
	             <th>业主名</th>
	             <th>订货日期</th>
	             <th>预计发货日期</th>
	             <th>发货日期</th>
	             <th>货号</th>
	             <th>物流名称</th>
	             <th>价格</th>
	             <th>订单状态</th>
	             <th>明细</th>
	             <th>附表</th>
	         </tr>
	         <c:forEach items="${list }" var="obj" varStatus="status">
	         	 <tr>
		             <td>${status.index + 1}</td>
		             <td>${obj.yezhu }</td>
		             <td>
		             	${fn:substring(obj.ddtime,0,10)}
		             </td>
		             <td>${obj.yujifahuotime }</td>
		             <td>${obj.fahuotime }</td>
		             <td>${obj.huohao }</td>
		             <td>${user_session.wuliu }</td>
		             <td>${obj.price }</td>
		             <td>
		            	<c:if test="${obj.ddstatus == '待生产' }"><span style="color:green">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '生产中' }"><span style="color:red">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '待发货' }"><span style="color:Purple">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '已发货' }"><span style="color:SaddleBrown">${obj.ddstatus}</span></c:if>
		             </td>
		             <td><a href="javascript:void(0);" onClick="toDownload('${obj.excelinfo}','${obj.ddnum }',1)">详情</a></td>
		             <td><a href="javascript:void(0);" onClick="toDownload('${obj.exceltwo}','${obj.ddnum }',2)">附表</a></td>
		         </tr>
	         </c:forEach>
	         <tr>
	         	<td>合计：</td>
	         	<td colspan="11">${count}元</td>
	         </tr>
	     </table>
	 </div>
</div>
</body>
</html>