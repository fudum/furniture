<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_msg").click(function(){

		var dingdanid = $("#dingdanid").val();
		var phone = $("#phone").val();
		//检验手机正确性
		var isMobile=/^(?:13\d|15\d|18\d)\d{5}(\d{3}|\*{3})$/;
		if(!isMobile.test(phone)){ //如果用户输入的值不同时满足手机号和座机号的正则
		    alert("请正确填写电话号码，例如:13415764179");  //就弹出提示信息
		    $("#phone").focus();       //输入框获得光标
		    return false;         //返回一个错误，不向下执行
		}
		
		var InterValObj; //timer变量，控制时间
		var curCount = 60;//当前剩余秒数
		
		$.ajax({
			type:'post',
			url:'<%=pre%>/y_dingdan/sendmsg<%=suf%>',
			data:{phone:phone,dingdanid:dingdanid},
			dataType:'json',
			success:function(data,status){
				if(data > 0){
					$("#btn_msg").attr("disabled", "true");
				   	$("#btn_msg").val(curCount + "秒");
				   	$("#btn_msg").removeClass();
					$("#msg_tip").html("发送成功！");
					InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次		
				}
				else{
					$("#msg_tip").html("发送失败！");
				}
			},
			error:function(xhr, textStatus, errorThrown){
				$("#msg_tip").html("发送失败！");
			}
		});
	});
});

//timer处理函数
function SetRemainTime() {
	var temp = $("#btn_msg").val();
	var curCount = temp.substring(0,temp.length-1);
    if (curCount == 0) {                
        window.clearInterval(InterValObj);//停止计时器
        $("#btn_msg").removeAttr("disabled");//启用按钮
        $("#btn_msg").val("发送");
        $("#msg_tip").html("");
    }
    else {
    	curCount--;
        $("#btn_msg").val(curCount+"秒");
    }
}
</script>
</head>
<body>
<table class="insert-tab">
	<tbody>
		<tr>
			<th>发送手机号</th>
			<td>
			   <input class="common-text required" id="phone" type="text" value="${phone}">
			   <input type="hidden" id="dingdanid" value="${dingdanid}">
			</td>
	 	</tr>
	 	<tr>
	 		<td></td>
	 		<td colspan="2" id = "td_btn">
	 			<input class="btn btn-primary btn2" value="发送" type="button" id="btn_msg">
	 			<span style="color:red" id="msg_tip" ></span>
	 		</td>
	 	</tr>
	</tbody>
</table>
</body>
</html>