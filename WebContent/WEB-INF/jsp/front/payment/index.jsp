<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户-我的付款</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>

</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">我的付款</span>
	</div>
</div>
<div class="search-wrap">
    <div class="search-content">
        <form action="<%=pre %>/payment/c_index<%=suf %>" method="post">
            <table class="search-tab">
                <tr>
                    <th width="80">查询日期:</th>
                    <td>
                        <input value="${year}" id="year" name="year" style="width:80px" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy年'})"/>
                    	<input value="${month}" id="month" name="month" style="width:80px" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'MM月'})"/>
                    	<input class="btn btn-primary btn2" name="sub" value="查询" type="submit" id="btn_search">
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<table class="result-tab" width="100%">
	         <tr>
	             <th>序号</th>
	             <th>账号</th>
	             <th>登记时间</th>
	             <th>实收金额</th>
	             <th>货款金额</th>
	             <th>手续费</th>
	             <th>到账时间</th>
	             <th>备注</th>
	         </tr>
	         <c:forEach items="${list }" var="obj" varStatus="status">
	         	 <tr>
		             <td>${status.index + 1}</td>
		             <td>${obj.account }</td>
		             <td>${obj.dengjitime }</td>
		             <td>${obj.shishoumoney }</td>
		             <td>${obj.huokuanmoney }</td>
		             <td>${obj.shouxufei }</td>
		             <td>${obj.daozhangtime }</td>
		             <td>${obj.beizhu }</td>
		         </tr>
	         </c:forEach>
	         <tr>
	         	<td>合计：</td>
	         	<td colspan="7">${count}元</td>
	         </tr>
	     </table>
	 </div>
</div>
</body>
</html>