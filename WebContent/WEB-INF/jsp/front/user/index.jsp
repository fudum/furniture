<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>前台-个人信息</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">个人信息</span>
	</div>
</div>
<div class="result-wrap">
    
    <div class="result-content">
        <ul class="sys-info-list">
            <li>
                <label class="res-lab">登入账号</label><span class="res-info">${user_session.userid }</span>
            </li>
            <li>
                <label class="res-lab">店面名称</label><span class="res-info">${user_session.dianmianname }</span>
            </li>
            <li>
                <label class="res-lab">负责人</label><span class="res-info">${user_session.name }</span>
            </li>
            <li>
                <label class="res-lab">电话号码</label><span class="res-info">${user_session.phone }</span>
            </li>
            <li>
                <label class="res-lab">对单电话</label><span class="res-info">${user_session.phonesec }</span>
            </li>
            <li>
                <label class="res-lab">QQ</label><span class="res-info">${user_session.qq }</span>
            </li>
            <li>
                <label class="res-lab">所在地区</label><span class="res-info">${user_session.address }</span>
            </li>
            <li>
                <label class="res-lab">收货地区</label><span class="res-info">${user_session.addressto }</span>
            </li>
            <li>
                <label class="res-lab">要求物流</label><span class="res-info">${user_session.wuliu }</span>
            </li>
        </ul>
    </div>
</div>
</body>
</html>