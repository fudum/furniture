<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理-用户添加</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script type="text/javascript" src="<%=js%>/verification.js"></script>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js %>/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var userid = $("#userid").val();
		if(userid.replace(/\s/g,"")==''){
			$("#useridtip").html('*请输入用户ID');
			return;
		}
		var password = $("#password").val();
		if(password.replace(/\s/g,"")==''){
			$("#passwordtip").html('*请输入密码');
			return;
		}
		var name = $("#name").val();
		if(name.replace(/\s/g,"")==''){
			$("#nametip").html('*请输入负责人名称');
			return;
		}
		var dianmianname = $("#dianmianname").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		var phonesec = $("#phonesec").val();
		var qq = $("#qq").val();
		var addressto = $("#addressto").val();
		var wuliu = $("#wuliu").val();
		var other = $("#other").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_user/insert<%=suf%>',
			data:{userid:userid,dianmianname:dianmianname,password:password,name:name,address:address,phone:phone,phonesec:phonesec,qq:qq,addressto:addressto,wuliu:wuliu,other:other},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">添加成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_user/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'user';
		var inputId = 'excelinfo';
		var url = '<%=pre%>/common/fileupload<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadExcel(url,uploadPath,wjjname,inputId);
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">用户管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">用户添加</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
	        <table class="insert-tab" width="100%">
	            <tbody>
	            	<tr>
	                	<th width="120"><i class="require-red">*</i>登入账号：</th>
		                <td>
		                   <input class="common-text required" id="userid" size="50" value="" type="text">
		                   <i class="require-red" id="useridtip"></i>
		                </td>
	           	 	</tr>
	           	 	<tr>
	                	<th width="120"><i class="require-red">*</i>密码：</th>
		                <td>
		                   <input class="common-text required" id="password" size="50" value="666666" type="text">
		                	<i class="require-red" id="passwordtip"></i>
		                </td>
	           	 	</tr>
	           	 	<tr>
	                    <th><i class="require-red">*</i>店面名称：</th>
	                    <td>
	                        <input class="common-text required" id="dianmianname" size="50" value="" type="text">
	                        <i class="require-red" id="dianmiannametip"></i>
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>负责人：</th>
	                    <td>
	                        <input class="common-text required" id="name" size="50" value="" type="text">
	                        <i class="require-red" id="nametip"></i>
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>所在地区：</th>
	                    <td>
	                        <input class="common-text required" id="address" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>电话号码：</th>
	                    <td>
	                        <input class="common-text required" id="phone" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>对单电话：</th>
	                    <td>
	                        <input class="common-text required" id="phonesec" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red"></i>qq号码：</th>
	                    <td>
	                        <input class="common-text required" id="qq" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>收货地区：</th>
	                    <td>
	                        <input class="common-text required" id="addressto" name="" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red">*</i>要求物流：</th>
	                    <td>
	                         <select id="wuliu" class="required">
	                         	<c:forEach items="${listLog }" var="obj">
	                         		<option>${obj.name }</option>
	                         	</c:forEach>
	                         </select>
	                    </td>
	                </tr>
	                <tr>
	                    <th><i class="require-red"></i>备注：</th>
	                    <td>
	                        <input class="common-text required" id="other" size="50" value="" type="text">
	                    </td>
	                </tr>
	                <tr>
                	<!-- <th>excel明细：</th>
                	<td>
                        <input id="uploadFile" name="uploadFile" type="file" ><i class="require-red">*选择excel文件</i>
                        <input type="hidden" id="excelinfo" value="">
                        <a href="javascript:void();" id="tip" class="tiphidden">下载文件</a>
                        <span id="spantip"></span>
                    </td>
                </tr> -->
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
	          </table>
	     
	</div>
</div>
</body>
</html>