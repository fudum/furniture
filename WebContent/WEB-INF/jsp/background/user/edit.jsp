<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>用户管理-用户编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js %>/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var userid = $("#userid").val();
		var password = $("#password").val();
		var dianmianname = $("#dianmianname").val();
		var name = $("#name").val();
		var address = $("#address").val();
		var phone = $("#phone").val();
		var phonesec = $("#phonesec").val();
		var qq = $("#qq").val();
		var addressto = $("#addressto").val();
		var wuliu = $("#wuliu").val();
		var other = $("#other").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_user/update<%=suf%>',
			data:{id:id,userid:userid,password:password,dianmianname:dianmianname,name:name,address:address,phone:phone,phonesec:phonesec,qq:qq,addressto:addressto,wuliu:wuliu,other:other},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_user/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'user';
		var inputId = 'excelinfo';
		var url = '<%=pre%>/common/fileupload<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadExcel(url,uploadPath,wjjname,inputId);
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">用户管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">用户编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab" width="100%">
            <tbody>
            	<tr>
                	<th width="120"><i class="require-red">*</i>登入账号：</th>
	                <td>
	                	<input type="hidden" value="${bean.id }" id="id" >
	                   <input class="common-text required" id="userid" size="50" value="${bean.userid }" type="text">
	                </td>
           	 	</tr>
           	 	<tr>
                	<th width="120"><i class="require-red">*</i>密码：</th>
	                <td>
	                   <input class="common-text required" id="password" size="50" value="${bean.password }" type="text">
	                </td>
           	 	</tr>
           	 	 <tr>
                    <th><i class="require-red">*</i>店面名称：</th>
                    <td>
                        <input class="common-text required" id="dianmianname" size="50" value="${bean.dianmianname }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>负责人：</th>
                    <td>
                        <input class="common-text required" id="name" size="50" value="${bean.name }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>所在地区：</th>
                    <td>
                        <input class="common-text required" id="address" size="50" value="${bean.address }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>电话号码：</th>
                    <td>
                        <input class="common-text required" id="phone" size="50" value="${bean.phone }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>对单电话：</th>
                    <td>
                        <input class="common-text required" id="phonesec" size="50" value="${bean.phonesec }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>qq号码：</th>
                    <td>
                        <input class="common-text required" id="qq" size="50" value="${bean.qq }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>收货地区：</th>
                    <td>
                        <input class="common-text required" id="addressto" name="" size="50" value="${bean.addressto }" type="text">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>要求物流：</th>
                    <td>
                         <select id="wuliu" class="required">
                         	<c:forEach items="${listLog }" var="obj">
                         		<option <c:if test="${bean.wuliu == obj.name }">selected='selected'</c:if>>${obj.name }</option>
                         	</c:forEach>
                         </select>
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>备注：</th>
                    <td>
                        <input class="common-text required" id="other" size="50" value="${bean.other }" type="text">
                    </td>
                </tr>
                <%--    <tr>
                	<th>excel明细：</th>
                	<td>
                        <input id="uploadFile" name="uploadFile" type="file" ><i class="require-red">*选择excel文件</i>
                        <input type="hidden" id="excelinfo" value="${bean.excelinfo }">
                        
                        	<a href="javascript:void();" id="tip" 
                        		<c:if test="${bean.excelinfo == null || bean.excelinfo == '' }">class="tiphidden"</c:if>>下载文件</a>
                        <span id="spantip"></span>
                    </td>
                </tr> --%>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>