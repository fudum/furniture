<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>经典案例-编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<style>
.anliinfo{float:left;margin:10px;display:block;}
</style>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js%>/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	//保存按钮
	$("#saveButton").click(function() {
		var picpath = $("#picpath").val();
		var name = $("#name").val();
		var id = $("#id").val();
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_anli/update<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,picpath:picpath,name:name},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">编辑成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_anli/index<%=suf%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
	
	//上传图片
	$("#uploadFile").live('change',function(){
		var picFile = $("#uploadFile").val();
		var wjjname = "anli";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".jpeg|.gif|.jpg|.png|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/common/fileupload<%=suf%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'uploadFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#catalogtip").attr("src","<%=upload%>/"+ data.path);
				            		$("#picpath").val(data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：图片格式的文件！");
				}
			}
		}
	});
	
	//添加二级
	$("#btn_add_two").click(function(){
		art.dialog.open("<%=pre%>/b_anliinfo/add<%=suf%>",{
			title:"添加一级",
			width:500,
			height:300,
			cancel: true,
			ok:function(){
				var anli_id = $("#id").val();
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕')
		        	return false;
		        };
		        var picpath = iframe.document.getElementById('picpath').value;
				var name = iframe.document.getElementById('name').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_anliinfo/insert<%=suf%>",
					data:{name:name,picpath:picpath,anli_id:anli_id},
					dataType:"json",
					success:function(data,status){
						if(data > 0){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
});


/**编辑二级页面**/
function editTwoPage(url){
	art.dialog.open(url,{
		title:"编辑基本二级目录",
		width:500,
		height:300,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
			var name = iframe.document.getElementById('name').value;
			var picpath = iframe.document.getElementById('picpath').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_anliinfo/update<%=suf%>",
				data:{id:id,name:name,picpath:picpath},
				dataType:"json",
				success:function(data,status){
					if(data > 0){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">经典案例</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<input type="hidden" id="id" value="${bean.id }">
		<table class="insert-tab">
			<tr>
				<td>名称：</td>
				<td><input type="text" name="name" id="name" value="${bean.name}"></td>
			</tr>
			
			<tr>
				<td>图片文件：</td>
				<td>
					<input type="file" name="uploadFile" id="uploadFile">
					<input  type="hidden" id="picpath" value="${bean.picpath}">
					<img src="<%=upload%>/${bean.picpath}" id="catalogtip" width="90px" height="120px"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="保存" class="button" id="saveButton" />
					<a href="<%=pre%>/b_anli/index<%=suf%>" style="padding-left:10px">返回</a>
				</td>
				<td></td>
			</tr>
		</table>

	 <div class="result-list" style="margin-top:20px;">
	    <a href="javascript:void(0);" id="btn_add_two"><i class="icon-font"></i>添加案例详细内容</a>
    </div>
   
   	<div>
   		<c:forEach items="${infoList}" var="obj" varStatus="status">
   			<div class="anliinfo">
    			<div>
    				<img src="<%=upload%>/${obj.picpath}" style="width:90px;height:120px;">
    			</div>
    			<div>
    				${obj.name }
    			</div>
    			<div>
    				<a href="javascript:void(0)" onclick="javascript:editTwoPage('<%=pre%>/b_anliinfo/edit<%=suf%>?id=${obj.id }')">编辑</a> | 
					<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_anliinfo/delete<%=suf%>?id=${obj.id}');">删除</a>
    			</div>
   			</div>
		</c:forEach>
   	</div>
   	<div style="clear:both;"></div>
   	
</div>
</div>
</body>
</html>