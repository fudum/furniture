<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">经典案例</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
		    <a href="<%=pre%>/b_anli/add<%=suf%>"><i class="icon-font"></i>新增案例</a>
	    </div>
	</div>
	<div class="result-content">
		<table class="result-tab" >
			<tr>
				<td>名称</td>
				<td>图片</td>
				<td>插入时间</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${obj.name }</td>
					<td><img src="<%=upload%>/${obj.picpath}" style="width:90px;height:150px;"></td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="<%=pre%>/b_anli/edit<%=suf%>?id=${obj.id }">编辑</a> |
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_anli/delete<%=suf%>?id=${obj.id}');">删除</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>