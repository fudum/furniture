<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>联系方式</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var scphone = $("#scphone").val();
		var xfphone = $("#xfphone").val();
		var zhanxianphone = $("#zhanxianphone").val();
		var fax = $("#fax").val();
		var address = $("#address").val();
		
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_lianxi/update<%=suf%>',
			data:{id:id,scphone:scphone,xfphone:xfphone,zhanxianphone:zhanxianphone,fax:fax,address:address},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_lianxi/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
			}
		});
	});
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">联系方式</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab">
            <tbody>
            	<tr>
                	<th width="100px"><i class="require-red">*</i>生产中心：</th>
	                <td>
	                	<input type="hidden" value="${bean.id }" id="id" > 
	                   	<input type="text" id="scphone" style="width:400px;" value="${bean.scphone }"/>
	                </td>
           	 	</tr>
                <tr>
                    <th><i class="require-red">*</i>服务中心：</th>
                    <td>
                        <input type="text" id="xfphone" value="${bean.xfphone}" style="width:400px;">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>渠道专线：</th>
                    <td>
                        <input id="zhanxianphone" type="text" value="${bean.zhanxianphone }" style="width:400px;">
                    </td>
                </tr>
                 <tr>
                    <th><i class="require-red">*</i>公司传真：</th>
                    <td>
                        <input id="fax" type="text" value="${bean.fax }" style="width:400px;">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>公司地址：</th>
                    <td>
                        <input id="address" type="text" value="${bean.address }" style="width:600px;">
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="编辑" type="button" id="btn_add">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>