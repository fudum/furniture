<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<title>基础信息模块</title>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">基础信息管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<table class="result-tab">
				<tr>
					<td>序号</td>
					<td>名称</td>
					<td>插入时间</td>
					<td>操作</td>
				</tr>
				<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj.ftype}</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="<%=pre%>/b_baseinfo/edit<%=suf%>?id=${obj.id }">编辑</a>  
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_baseinfo/delete<%=suf%>?id=${obj.id}');">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
	</div>
	<div class="list-page">
     	<!-- 分页条 -->
		<div>
			<div class="manu">
				<c:if test="${pager.totalPage >0}">
					<!-- 上一页 -->
					<c:choose>
						<c:when test="${pager.curtPage == 1 }">
							<span class="disabled">&lt;上一页</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${pager.curtPage - 1}">&lt; 上一页 </a>
						</c:otherwise>
					</c:choose>
					
					<!-- content -->
					<c:if test="${pager.totalPage < 7 }">
						<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
					<c:if test="${pager.totalPage >= 7 }">
						<c:if test="${pager.curtPage <= 4 }">
							<c:forEach var = "item" begin="1" end = "5">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							...
							<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
						</c:if>
						<c:if test="${pager.curtPage > 4 }">
							<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=1">1</a>
							...
							<c:choose>
									
									<c:when test="${pager.curtPage < pager.totalPage - 3 }">
										<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="<%=pre%>/b_baseinfo/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
									</c:when>
									<c:otherwise>
										<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</c:if>
					</c:if>
					<!-- 下一页 -->
					<c:choose>
						<c:when test="${pager.curtPage == pager.totalPage }">
							<span class="disabled">下一页 &gt;</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/b_baseinfo/index<%=suf%>?curtPage=${pager.curtPage + 1}">下一页  &gt; </a>
						</c:otherwise>
					</c:choose>	
				</c:if>
			</div>
		</div><!-- 分页结束 -->
     </div>
</div>
</body>
</html>