<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>消息通知</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">消息通知管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
	        <a href="<%=pre%>/b_notice/add<%=suf%>"><i class="icon-font"></i>新增消息通知</a>
	    </div>
	</div>
	<div class="result-content">
		<table class="result-tab" width="100%">
	         <tr>
	             <th style="width:50px">序号</th>
	             <th>通知内容</th>
	             <th style="width:150px">操作</th>
	         </tr>
	         <c:forEach items="${pager.pageList }" var="obj" varStatus="status">
	         	<tr>
		             <td>${status.index + 1}</td>
		             <td>${obj.content }</td>
		             <td>
		                 <a class="link-update" href="<%=pre%>/b_notice/edit<%=suf%>?id=${obj.id}">编辑详情</a>
		                 <a class="link-del" href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_notice/delete<%=suf%>?id=${obj.id}');">删除</a>
		             </td>
	         	</tr>
	         </c:forEach>
	     </table>
	</div>
</div>
</body>
</html>