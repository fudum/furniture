<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>消息通知编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var content = $("#content").val();
		if(content.replace(/\s/g,"")==''){
			$("#cardidtip").html('*请输入消息通知内容');
			return;
		}
		$.ajax({
			type:'post',
			url:'<%=pre%>/notice/update<%=suf%>',
			data:{id:id,content:content},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_notice/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">消息通知管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">消息通知编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab" width="100%">
            <tbody>
                 <tr>
                    <th style="width:100px"><i class="require-red">*</i>消息内容：</th>
                    <td>
                    	<input type="hidden" value="${bean.id }" id="id"> 
                    	<textarea rows="5" cols="80" id="content">${bean.content }</textarea>
                        <i class="require-red" id="noticetip"></i>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>