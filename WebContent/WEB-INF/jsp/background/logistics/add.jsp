<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>物流管理-物流添加</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var name = $("#name").val();
		if(name.replace(/\s/g,"")==''){
			$("#nametip").html('*请输入物流公司名称');
			return;
		}
		var description = $("#description").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_logistics/insert<%=suf%>',
			data:{name:name,description:description},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">添加成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_logistics/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">物流公司管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">物流公司添加</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
	    <form action="" method="post" id="myform" name="myform" enctype="multipart/form-data">
	        <table class="insert-tab" width="100%">
	            <tbody>
	            	<tr>
	                	<th width="120"><i class="require-red">*</i>物流公司名称：</th>
		                <td>
		                   <input class="common-text required" id="name" size="30" type="text">
		                   <i class="require-red" id="nametip"></i>
		                </td>
	           	 	</tr>
	                <tr>
	                    <th>公司描述：</th>
	                    <td>
	                        <input class="common-text required" id="description" size="60" type="text">
	                    </td>
	                </tr>
	                <tr>
	                    <th></th>
	                    <td>
	                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
	                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
	                    </td>
	                    </tr>
	                </tbody>
	          </table>
	     </form>
	</div>
</div>
</body>
</html>