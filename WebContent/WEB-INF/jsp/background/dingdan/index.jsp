<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单管理-列表</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<link rel="stylesheet" href="<%=common%>/tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="<%=common%>/tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="<%=common%>/tablesorter/jquery.tablesorter.js"></script>

<script>
$(document).ready(function() {
    $(".tablesorter").tablesorter(); 
});
function dingdaninfo(info){
	art.dialog.open('<%=pre%>/b_dingdan/c_info<%=suf%>?info=' + info,{
		titile:'订单详情',
		width:800,
		height:400
	});
}
function toDownload(storeName,realName){
	if(storeName == ''){
		alert('订单详情不存在！');
		return;
	}
	document.getElementById('realName').value = realName;
	document.getElementById('storeName').value = storeName;
	document.getElementById('downForm').submit();
}

function getMsg(dingdanId,userid){
	art.dialog.open('<%=pre%>/y_dingdan/getmsg<%=suf%>?dingdanId=' + dingdanId +'&userid=' + userid,{
		titile:'短信获取',
		width:300,
		height:150
	});
}
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">订单管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
	    	<form id="form1" method="post" action="<%=pre%>/b_dingdan/index<%=suf%>">
		        <a href="<%=pre%>/b_dingdan/add<%=suf%>"><i class="icon-font"></i>新增订单</a>
		        <span>起始时间：</span>
		        <input id="ddtime" name="ddtime" type="text" onClick="WdatePicker()" style="width:100px"/>
		        <span>结束时间：</span>
		        <input id="inserttime" name="inserttime" type="text" onClick="WdatePicker()" style="width:100px" value=""/>
		        <span>经销商：</span>
		        <input value="${bean.username}" id="username" name="username" style="width:100px" type="text"/>
		        <span>订单号：</span>
		        <input value="${bean.ddnum}" id="ddnum" name="ddnum" style="width:100px" type="text"/>
		        <span>区域：</span>
		        <input value="${bean.huohao}" id="huohao" name="huohao" style="width:100px" type="text"/>
	            <input class="btn btn-primary btn2" name="sub" value="查询" type="submit" id="btn_search">
            </form>
	    </div>
	</div>
	<div class="result-content">
		<div>
			<form id="downForm" action="<%=pre%>/b_dingdan/download<%=suf%>" method="post">
				<input type="hidden" name="storeName" id="storeName" value="" /> 
				<input type="hidden" name="realName" id="realName" value="订单详情" />
			</form>
		</div>	
		<table class="result-tab tablesorter">
			<thead>
	         <tr>
	             <th>序号</th>
	             <th>订单号</th>
	             <th>经销商</th>
	             <th>业主名</th>
	             <th>订货日期</th>
	             <th>预计发货日期</th>
	             <th>发货日期</th>
	             <th>货号</th>
	             <th>价格</th>
	             <th>订单状态</th>
	             <th>明细</th>
	             <th>操作</th>
	             <th>短信下发</th>
	         </tr>
	         </thead>
	         <tbody>
	         <c:forEach items="${pager.pageList }" var="obj" varStatus="status">
	         	 <tr>
		             <td>${status.index + 1}</td>
		             <td>${obj.ddnum }</td>
		             <td>${obj.username }</td>
		             <td>${obj.yezhu }</td>
		             <td>
		             	${fn:substring(obj.ddtime,0,10)}
		             </td>
		             <td>${obj.yujifahuotime }</td>
		             <td>${obj.fahuotime }</td>
		             <td>${obj.huohao }</td>
		             <td>${obj.price }</td>
		            <td>
		            	<c:if test="${obj.ddstatus == '待生产' }"><span style="color:green">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '生产中' }"><span style="color:red">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '待发货' }"><span style="color:Purple">${obj.ddstatus}</span></c:if>
		             	<c:if test="${obj.ddstatus == '已发货' }"><span style="color:SaddleBrown">${obj.ddstatus}</span></c:if>
		             </td>
		             <td><a href="javascript:void(0);" onClick="toDownload('${obj.excelinfo}','${obj.ddnum }')">明细</a></td>
		             <td>
		                 <a class="link-update" href="<%=pre%>/b_dingdan/edit<%=suf%>?id=${obj.id}">编辑</a>
		                 <a class="link-del" href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_dingdan/delete<%=suf%>?id=${obj.id}');">删除</a>
		             </td>
		             <td>
		         		<a href="javascript:void(0);" onClick="getMsg('${obj.id}','${obj.userid}');">短信下发</a>
		         	</td>
		         </tr>
	         </c:forEach>
	         </tbody>
	         <tr>
	         	<td>合计：</td>
	         	<td colspan="12" style="text-align:left">${count}元</td>
	         </tr>
	     </table>
	     
	     <div class="list-page">
	     	<!-- 分页条 -->
			<div>
				<div class="manu">
					<c:if test="${pager.totalPage >0}">
						<!-- 上一页 -->
						<c:choose>
							<c:when test="${pager.curtPage == 1 }">
								<span class="disabled">&lt;上一页</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${pager.curtPage - 1}">&lt; 上一页 </a>
							</c:otherwise>
						</c:choose>
						
						<!-- content -->
						<c:if test="${pager.totalPage < 7 }">
							<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
						<c:if test="${pager.totalPage >= 7 }">
							<c:if test="${pager.curtPage <= 4 }">
								<c:forEach var = "item" begin="1" end = "5">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${item}">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								...
								<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
							</c:if>
							<c:if test="${pager.curtPage > 4 }">
								<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=1">1</a>
								...
								<c:choose>
										
										<c:when test="${pager.curtPage < pager.totalPage - 3 }">
											<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${item}">${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											...
											<a href="<%=pre%>/b_dingdan/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
										</c:when>
										<c:otherwise>
											<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${item}">${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:otherwise>
									</c:choose>
							</c:if>
						</c:if>
						<!-- 下一页 -->
						<c:choose>
							<c:when test="${pager.curtPage == pager.totalPage }">
								<span class="disabled">下一页 &gt;</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_dingdan/index<%=suf%>?curtPage=${pager.curtPage + 1}">下一页  &gt; </a>
							</c:otherwise>
						</c:choose>	
					</c:if>
				</div>
			</div><!-- 分页结束 -->
	     </div>
	</div>
</div>
</body>
</html>