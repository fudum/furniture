<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单管理-编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<style>
.tiphidden{
display:none;
}
.tipshow{
display:block;
}
</style>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js %>/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var ddnum = $("#ddnum").val();
		if(ddnum == ''){
			$("#ddnumtip").html('*请输入订单号');
			$("#ddnum").focus();
			return;
		}
		var username = $("#username").val();
		if(username == ''){
			$("#usernametip").html('*请选择业经销商');
			$("#username").focus();
			return;
		}		
		var yezhu = $("#yezhu").val();
		var ddtime = $("#ddtime").val();
		var yujifahuotime = $("#yujifahuotime").val();
		var fahuotime = $("#fahuotime").val();
		var huohao = $("#huohao").val();
		var price = $("#price").val();
		var ddstatus = $("#ddstatus").val();
		var excelinfo = $("#excelinfo").val();
		var exceltwo = $("#exceltwo").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_dingdan/update<%=suf%>',
			data:{id:id,ddnum:ddnum,username:username,ddtime:ddtime,yujifahuotime:yujifahuotime,fahuotime:fahuotime,
				huohao:huohao,price:price,ddstatus:ddstatus,excelinfo:excelinfo,yezhu:yezhu,exceltwo:exceltwo},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_dingdan/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	$("#upload").live('change',function(){
		var wjjname = 'dingdan';
		var imgId = 'pic_img';
		var inputId = 'info';
		var url = '<%=pre%>/common/fileupload<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'dingdan';
		var inputId = 'excelinfo';
		var url = '<%=pre%>/common/fileupload<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadExcel(url,uploadPath,wjjname,inputId);
	});
	
	$("#uploadFileTwo").live('change',function(){
		var wjjname = 'dingdan';
		var inputId = 'exceltwo';
		var url = '<%=pre%>/common/fileuploadtwo<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadExcelTwo(url,uploadPath,wjjname,inputId);
	});
	
	$("#tip").click(function(){
		var storeName = $("#excelinfo").val();
		document.getElementById('storeName').value = storeName;
		document.getElementById('downForm').submit();
	});
	
	$("#tiptwo").click(function(){
		var storeName = $("#exceltwo").val();
		document.getElementById('storeName').value = storeName;
		document.getElementById('downForm').submit();
	});
	
	//区域变化
	$("#quyu").change(function(){
		var quyu = $("#quyu").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_dingdan/quyu<%=suf%>',			
			data:{quyu:quyu},
			dataType:'json',
			success:function(data,status){
				if(status == 'success'){
					$("#username option").remove(); 
					$("#username").append('<option>--请选择经销商--</option>');					
					for(var i = 0; i < data.listU.length; i++){
						var value = data.listU[i].id + ';' + data.listU[i].dianmianname;
						var text = data.listU[i].dianmianname;
						$("#username").append("<option value='"+ value +"'>"+text+"</option>");
					}
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="#">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">订单管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">订单编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<div>
			<form id="downForm" action="<%=pre%>/b_dingdan/download<%=suf%>" method="post">
				<input type="hidden" name="storeName" id="storeName" value="" /> 
				<input type="hidden" name="realName" id="realName" value="订单详情" />
			</form>
		</div>	
        <table class="insert-tab" width="100%">
            <tbody>
            	<tr>
                	<th width="120"><i class="require-red">*</i>订单号：</th>
	                <td>
	                	<input type = "hidden" value="${bean.id }" id="id">
	                   <input class="common-text required" id="ddnum" size="30" type="text" value="${bean.ddnum }">
	                   <i class="require-red" id="ddnumtip">*</i>
	                </td>
           	 	</tr>
                <tr>
                    <th><i class="require-red">*</i>经销商：</th>
                    <td>
                    	<select id="quyu">
                    		<option value="0;全部区域">全部区域</option>
                    		<c:forEach items="${quyuList }" var="obj" varStatus="status">
                    			<option value="${status.index + 1 };${obj}" <c:if test="${quyu == obj}">selected="selected"</c:if>>${obj}</option>
                    		</c:forEach>
                    	</select>
                         <select id="username" class="required">
                         	<option value="">--请选择经销商--</option>
                         	<c:forEach items="${userList}" var="obj">
                         		<option value="${obj.id};${obj.name}" <c:if test="${bean.userid == obj.id }">selected='selected'</c:if>>${obj.dianmianname}</option>
                         	</c:forEach>
                        </select>
                         <i class="require-red" id="usernametip">*</i>
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>业主名：</th>
                    <td>
                        <input class="common-text required" id="yezhu" size="30" type="text" value="${bean.yezhu }">
                    </td>
                </tr>
                <tr>
                    <th>预计发货日期：</th>
                    <td>
                    	<input id="yujifahuotime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="${bean.yujifahuotime }"/>
                    </td>
                </tr>
                 <tr>
                    <th><i class="require-red"></i>发货日期：</th>
                    <td>
                        <input id="fahuotime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})" value="${bean.fahuotime }"/>
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>货号：</th>
                    <td>
                        <input class="common-text required" id="huohao" size="60" type="text" value="${bean.huohao }">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>价格：</th>
                    <td>
                        <input class="common-text required" id="price" size="30" type="text" value="${bean.price }">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>当前状态：</th>
                    <td>
                        <select id="ddstatus" class="required">
                        	<option value="待生产" <c:if test="${bean.ddstatus == '待生产' }">selected='selected'</c:if>>待生产</option>
                        	<option value="生产中" <c:if test="${bean.ddstatus == '生产中' }">selected='selected'</c:if>>生产中</option>
                        	<option value="待发货" <c:if test="${bean.ddstatus == '待发货' }">selected='selected'</c:if>>待发货</option>
                        	<option value="已发货" <c:if test="${bean.ddstatus == '已发货' }">selected='selected'</c:if>>已发货</option>
                        </select>
                    </td>
                </tr>
                <%-- <tr>
                    <th><i class="require-red"></i>明细：</th>
                    <td>
                        <input id="uploadFile" name="uploadFile" type="file" ><i class="require-red">*选择图片文件</i>
                        <input type="hidden" id="info" value="${bean.info }">
                        <img id="pic_img" width="300px" height="150px" src="<%=upload %>/${bean.info}" />
                    </td>
                </tr> --%>
                <tr>
                	<th>excel明细：</th>
                	<td>
                        <input id="uploadFile" name="uploadFile" type="file" ><i class="require-red">*选择excel文件</i>
                        <input type="hidden" id="excelinfo" value="${bean.excelinfo }">
                        
                        	<a href="javascript:void();" id="tip" 
                        		<c:if test="${bean.excelinfo == null || bean.excelinfo == '' }">class="tiphidden"</c:if>>下载文件</a>
                        <span id="spantip"></span>
                    </td>
                </tr>
                <tr>
                	<th>excel副表：</th>
                	<td>
                        <input id="uploadFileTwo" name="uploadFileTwo" type="file" ><i class="require-red">*选择excel文件</i>
                        <input type="hidden" id="exceltwo" value="${bean.exceltwo }">
                        
                        	<a href="javascript:void();" id="tiptwo" 
                        		<c:if test="${bean.exceltwo == null || bean.exceltwo == '' }">class="tiphidden"</c:if>>下载文件</a>
                        <span id="spantip"></span>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>