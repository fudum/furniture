<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>付款管理-编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script src="<%=js%>/fileupload.js"></script>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id=$("#id").val();
		var username = $("#username").val();
		if(username == ''){
			$("#usernametip").html('*请选择经销商名称');
			$("#username").focus();
			return;
		}		
		var account = $("#account").val();
		if(account == ''){
			$("#accounttip").html('*请选择银行账号');
			$("#account").focus();
			return;
		}
		var dengjitime = $("#dengjitime").val();
		var huokuanmoney = $("#huokuanmoney").val();
		var shishoumoney = $("#shishoumoney").val();
		var shouxufei = $("#shouxufei").val();
		var daozhangtime = $("#daozhangtime").val();
		var beizhu = $("#beizhu").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/payment/update<%=suf%>',
			data:{id:id,username:username,account:account,dengjitime:dengjitime,huokuanmoney:huokuanmoney,shishoumoney:shishoumoney,
				shouxufei:shouxufei,daozhangtime:daozhangtime,beizhu:beizhu},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/payment/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'dingdan';
		var imgId = 'pic_img';
		var inputId = 'info';
		var url = '<%=pre%>/common/fileupload<%=suf%>';
		var uploadPath = '<%=upload%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">付款管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">付款编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab" width="100%">
            <tbody>
            	<tr>
                    <th><i class="require-red">*</i>经销商名称：</th>
                    <td>
                         <select id="username" class="required">
                         	<option value="">--请选择--</option>
                         	<c:forEach items="${userList}" var="obj">
                         		<option value="${obj.id};${obj.name}" <c:if test="${bean.username == obj.name }">selected='selected'</c:if>>${obj.name}</option>
                         	</c:forEach>
                        </select>
                         <i class="require-red" id="usernametip">*</i>
                    </td>
                </tr>
            	 <tr>
                    <th><i class="require-red">*</i>账号：</th>
                    <td>
                    	<input type="hidden" value="${bean.id }" id="id">
                         <select id="account" class="required">
                         	<option value="">--请选择--</option>
                         	<c:forEach items="${listCard}" var="obj">
                         		<option value="${obj.bankname}${fn:substring(obj.cardid,15,19) }"
                         			<c:if test="${obj.bankname.concat(fn:substring(obj.cardid,15,19)) == bean.account}">selected='selected'</c:if>>${obj.bankname}${fn:substring(obj.cardid,15,19) }</option>
                         	</c:forEach>
                        </select>
                        <i class="require-red" id="accounttip"></i>
                    </td>
                </tr>
            	<tr>
                	<th width="120"><i class="require-red"></i>登记时间：</th>
	                <td>
	                   <input id="dengjitime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="${bean.dengjitime }"/>
	                </td>
           	 	</tr>
                <tr>
                    <th><i class="require-red"></i>货款金额：</th>
                    <td>
                         <input class="common-text required" id="huokuanmoney" size="30" type="text" value="${bean.huokuanmoney }">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>实收金额：</th>
                    <td>
                         <input class="common-text required" id="shishoumoney" size="30" type="text" value="${bean.shishoumoney }">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>手续费：</th>
                    <td>
                        <input class="common-text required" id="shouxufei" size="30" type="text" value="${bean.shouxufei }">
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>到账时间：</th>
                    <td>
                        <input id="daozhangtime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" value="${bean.daozhangtime }"/>
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red"></i>备注：</th>
                    <td>
                        <input class="common-text required" id="beizhu" size="60" type="text" value="${bean.beizhu }">
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>