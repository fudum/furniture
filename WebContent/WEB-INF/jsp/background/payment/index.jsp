<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>付款管理-列表</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<link rel="stylesheet" href="<%=common%>/tablesorter/themes/blue/style.css" type="text/css" media="print, projection, screen" />
<script type="text/javascript" src="<%=common%>/tablesorter/jquery-latest.js"></script>
<script type="text/javascript" src="<%=common%>/tablesorter/jquery.tablesorter.js"></script>
<script>
$(document).ready(function() {
    $(".tablesorter").tablesorter(); 
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">付款管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
	    	<form id="form1" method="post" action="<%=pre%>/b_payment/index<%=suf%>">
		        <a href="<%=pre%>/b_payment/add<%=suf%>"><i class="icon-font"></i>新增付款</a>
		        <span>按经销商查询：</span>
		        <input value="${bean.username}" id="username" name="username" style="width:100px" type="text"/>
	            <input class="btn btn-primary btn2" name="sub" value="查询" type="submit" id="btn_search">
            </form>
	        
	    </div>
	</div>
	<div class="result-content">
		<table class="result-tab tablesorter" width="100%">
		<thead>
	         <tr>
	             <th>序号</th>
	             <th>经销商</th>
	             <th>账号</th>
	             <th>登记时间</th>
	             <th>实收金额</th>
	             <th>货款金额</th>
	             <th>手续费</th>
	             <th>到账时间</th>
	             <th>备注</th>
	             <th>操作</th>
	         </tr>
	         </thead>
	         <tbody>
	         <c:forEach items="${pager.pageList }" var="obj" varStatus="status">
	         	 <tr>
		             <td>${status.index + 1}</td>
		             <td>${obj.username }</td>
		             <td>${obj.account }</td>
		             <td>${obj.dengjitime }</td>
		             <td>${obj.shishoumoney }</td>
		             <td>${obj.huokuanmoney }</td>
		             <td>${obj.shouxufei }</td>
		             <td>${obj.daozhangtime }</td>
		             <td>${obj.beizhu }</td>
		             <td>
		                 <a class="link-update" href="<%=pre%>/b_payment/edit<%=suf%>?id=${obj.id}">编辑</a>
		                 <a class="link-del" href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_payment/delete<%=suf%>?id=${obj.id}');">删除</a>
		             </td>
		         </tr>
	         </c:forEach>
	         </tbody>
	     </table>
	     
	     <div class="list-page">
	     	<!-- 分页条 -->
			<div>
				<div class="manu">
					<c:if test="${pager.totalPage >0}">
						<!-- 上一页 -->
						<c:choose>
							<c:when test="${pager.curtPage == 1 }">
								<span class="disabled">&lt;上一页</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${pager.curtPage - 1}">&lt; 上一页 </a>
							</c:otherwise>
						</c:choose>
						
						<!-- content -->
						<c:if test="${pager.totalPage < 7 }">
							<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
						<c:if test="${pager.totalPage >= 7 }">
							<c:if test="${pager.curtPage <= 4 }">
								<c:forEach var = "item" begin="1" end = "5">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${item}">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								...
								<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
							</c:if>
							<c:if test="${pager.curtPage > 4 }">
								<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=1">1</a>
								...
								<c:choose>
										
										<c:when test="${pager.curtPage < pager.totalPage - 3 }">
											<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${item}">${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											...
											<a href="<%=pre%>/b_payment/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
										</c:when>
										<c:otherwise>
											<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${item}">${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:otherwise>
									</c:choose>
							</c:if>
						</c:if>
						<!-- 下一页 -->
						<c:choose>
							<c:when test="${pager.curtPage == pager.totalPage }">
								<span class="disabled">下一页 &gt;</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_payment/index<%=suf%>?curtPage=${pager.curtPage + 1}">下一页  &gt; </a>
							</c:otherwise>
						</c:choose>	
					</c:if>
				</div>
			</div><!-- 分页结束 -->
	     </div>
	     
	
	</div>

</div>
</body>
</html>