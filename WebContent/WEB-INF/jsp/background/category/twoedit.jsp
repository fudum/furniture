<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑二级目录</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
	<div class="result-wrap">
		<div class="result-content">
			<input type="hidden" id="id" value="${bean.id }" />
			<input type="hidden" id="old_name" value="${bean.name }" />
			<table class="insert-tab">
				<tr>
					<td>一级目录名称：</td>
					<td>
						<select id="parentname">
							<c:forEach items="${listOne }" var="obj">
								<option value="${obj.id};${obj.name}" <c:if test="${bean.parentid == obj.id}">selected="selected"</c:if>>${obj.name }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>二级名称：</td>
					<td>
						<input type="text" id="name" style="width:300px" value="${bean.name }"/>
					</td>
				</tr>
				<tr>
					<td>显示顺序：</td>
					<td>
						<input type="text" id="border" style="width:300px" value="${bean.shunxu }"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>