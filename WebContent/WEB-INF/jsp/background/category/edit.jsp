<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑目录页面</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
	<div class="result-wrap">
		<div class="result-content">
			<input type="hidden" value="${bean.id }" id="id"> 
			<table class="insert-tab">
				<tr>
					<td>目录名称：</td>
					<td>
						<input type="text" id="name" style="width:300px" value="${bean.name }"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>