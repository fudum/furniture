<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>基本信息管理</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	//添加一级
	$("#btn_add_one").click(function(){
		art.dialog.open("<%=pre%>/b_category/addOne<%=suf%>",{
			title:"添加产品类别",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕')
		        	return false;
		        };
				var name = iframe.document.getElementById('name').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_category/insertOne<%=suf%>",
					data:{name:name},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	
	//添加二级
	$("#btn_add_two").click(function(){
		art.dialog.open("<%=pre%>/b_category/addTwo<%=suf%>",{
			title:"添加二级类别",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕')
		        	return false;
		        };
		        var parentname = iframe.document.getElementById('parentname').value;
				var name = iframe.document.getElementById('name').value;
				var shunxu = iframe.document.getElementById('border').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_category/insertTwo<%=suf%>",
					data:{parentname:parentname,name:name,shunxu:shunxu},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	
});


/**编辑一级页面**/
function editOnePage(url){
	art.dialog.open(url,{
		title:"编辑类别",
		width:500,
		height:200,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
			var name = iframe.document.getElementById('name').value;
			var id = iframe.document.getElementById('id').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_category/updateOne<%=suf%>",
				data:{id:id,name:name},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

/**编辑二级页面**/
function editTwoPage(url){
	art.dialog.open(url,{
		title:"编辑二级产品类别",
		width:500,
		height:200,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
	        var old_name = iframe.document.getElementById('old_name').value;
			var name = iframe.document.getElementById('name').value;
			var parentname = iframe.document.getElementById('parentname').value;
			var shunxu = iframe.document.getElementById('border').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_category/updateTwo<%=suf%>",
				data:{id:id,name:name,parentname:parentname,shunxu:shunxu,old_name:old_name},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		
		<span class="crumb-name">产品类别管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
		    <a href="javascript:void(0);" id="btn_add_one"><i class="icon-font"></i>添加产品类别</a>
	    </div>
	</div>
	<div class="result-content">
		<table class="result-tab">
			<tr>
				<td>序号</td>
				<td>类别名称</td>
				<td>插入时间</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:void(0)" onclick="javascript:editOnePage('<%=pre%>/b_category/editOne<%=suf%>?id=${obj.id }')">编辑</a>  
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_category/delete<%=suf%>?id=${obj.id}');">删除</a></td>
				</tr>
			</c:forEach>
		</table>
		
		<div class="list-page">
	     	<!-- 分页条 -->
				<div>
					<div class="manu">
						<c:if test="${pager.totalPage >0}">
							<!-- 上一页 -->
							<c:choose>
								<c:when test="${pager.curtPage == 1 }">
									<span class="disabled">&lt;上一页</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${pager.curtPage - 1}">&lt; 上一页 </a>
								</c:otherwise>
							</c:choose>
							
							<!-- content -->
							<c:if test="${pager.totalPage < 7 }">
								<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${item}">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
							<c:if test="${pager.totalPage >= 7 }">
								<c:if test="${pager.curtPage <= 4 }">
									<c:forEach var = "item" begin="1" end = "5">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
								</c:if>
								<c:if test="${pager.curtPage > 4 }">
									<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=1">1</a>
									...
									<c:choose>
											
											<c:when test="${pager.curtPage < pager.totalPage - 3 }">
												<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${item}">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												...
												<a href="<%=pre%>/b_category/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
											</c:when>
											<c:otherwise>
												<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${item}">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
								</c:if>
							</c:if>
							<!-- 下一页 -->
							<c:choose>
								<c:when test="${pager.curtPage == pager.totalPage }">
									<span class="disabled">下一页 &gt;</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_category/index<%=suf%>?curtPage=${pager.curtPage + 1}">下一页  &gt; </a>
								</c:otherwise>
							</c:choose>	
						</c:if>
					</div>
				</div><!-- 分页结束 -->
	     </div>
	</div>
</div>
</body>
</html>