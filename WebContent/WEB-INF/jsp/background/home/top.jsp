<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>头部</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">
            <h1 class="topbar-logo none"><a href="index.html" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="#">金喜装饰材料厂</a></li>
            </ul>
        </div>
        <div style="float:left">
	        <marquee width="800px" direction="left" scrollamount="2"  onmouseover="this.stop()" onmouseout="this.start()">
		        <c:forEach items="${listtop }" var="obj" varStatus="status">
		        	<a href="">${obj.content}</a>&nbsp;
		        </c:forEach>
	    	</marquee>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a href="#">你好，${user_session.name }</a></li>
                <li><a href="<%=pre%>/home/loginout<%=suf%>" target="_top">退出</a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>