<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>右侧导航</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font">&#xe06b;</i><span>欢迎使用金喜橱柜加工厂管理系统。</span></div>
    </div>
    <div class="result-wrap">
        <div class="result-content">
            <div class="short-wrap">
            	${cxbean.content}
            </div>
        </div>
    </div>    
</div>
<!--/main-->
</body>
</html>