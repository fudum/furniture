<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>金喜橱柜加工厂-登入页面</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/admin_login.css"/>
<script>
$(document).ready(function(){
	$("#btn_sub").click(function(){
		var userid = $("#userid").val();
		var password = $("#password").val();
		var utype = $("input[name='utype']:checked").val();
		if(userid == '' || userid ==null ){
			$("#userid").focus();
			return;
		}
		if(password == '' || password == null){
			$("#password").focus();
			return;
		}
		if(utype == '' || utype == null){
			return;
		}
		if(utype == 1){
			password = faultylabs.MD5(password);
		}
		$.ajax({
			url : "<%=pre%>/home/login<%=suf%>",
			type : 'post',
			dataType : 'html',
			data:{userid:userid,password:password,utype:utype},
			success:function(data, status){
				if(data == 1) {
					window.location.href = '<%=pre%>/home/index<%=suf%>';
				}
				else{
					$("#tip").html("用户名或密码错误！");
				}
			},
			error : function(xhr, textStatus,
					errorThrown){
				$("#tip").html("用户名或密码错误！");
			}
		});
	});
	
	$("#form1").keypress(function(e) { 
		if(e.which == 13){ 
			$("#btn_sub").click();
		} 
	}); 
});
</script>
</head>
<body>
<div class="admin_login_wrap">
    <h1>金喜橱柜加工厂登入页面</h1>
    <div class="adming_login_border">
        <div class="admin_input">
            <form method="post" id="form1">
                <ul class="admin_items">
                    <li>
                        <label for="userid">用户名：</label>
                        <input type="text" name="userid" id="userid" size="30" class="admin_input_style" />
                    </li>
                    <li>
                        <label for="password">密码：</label>
                        <input type="password" name="password" id="password" size="30" class="admin_input_style" />
                    </li>
                    <li>
                    	<input type="radio" name="utype" value="2" >普通用户
                    	<input type="radio" name="utype" value="1" checked="checked">管理员
                    	<span id="tip" style="color:red;font-size:12px;"></span>
                    </li>
                    <li>
                        <input id="btn_sub" type="button" tabindex="3" value="登入" class="btn btn-primary" />
                    </li>
                </ul>
            </form>
        </div>
    </div>
    <p class="admin_copyright"> &copy; 2015 Powered by web</p>
</div>
</body>
</html>