<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>金喜橱柜加工厂-登入页面</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/login01.css"/>
<script>
$(document).ready(function(){
	$("#btn_sub").click(function(){
		var userid = $("#userid").val();
		var password = $("#password").val();
		var utype = $("input[name='utype']:checked").val();
		if(userid == '' || userid ==null ){
			$("#userid").focus();
			return;
		}
		if(password == '' || password == null){
			$("#password").focus();
			return;
		}
		if(utype == '' || utype == null){
			return;
		}
		if(utype == 1){
			password = faultylabs.MD5(password);
		}
		$.ajax({
			url : "<%=pre%>/home/login<%=suf%>",
			type : 'post',
			dataType : 'html',
			data:{userid:userid,password:password,utype:utype},
			success:function(data, status){
				if(data == 1) {
					window.location.href = '<%=pre%>/home/index<%=suf%>';
				}
				else{
					$("#tip").html("用户名或密码错误！");
				}
			},
			error : function(xhr, textStatus,
					errorThrown){
				$("#tip").html("用户名或密码错误！");
			}
		});
	});
	
	$("#form1").keypress(function(e) { 
		if(e.which == 13){ 
			$("#btn_sub").click();
		} 
	}); 
});
</script>
</head>
<body>
<div class="top">
	<form method="post" id="form1">
		<input type="text" autofocus="true" id="userid" name="userid" placeholder="用户名"/>
	    <input type="password" id="password" name="password" placeholder="密码"/>
	    <li id="usertype">
	    	<input type="radio" name="utype" value="2" checked="checked">普通用户
	    	<input type="radio" name="utype" value="1" >管理员
	    	<span id="tip" style="color:red;font-size:12px;"></span>
	    </li>
	    <input type="button" value="" id="btn_sub" name="login_bt"/>
	</form>
</div>
<div class="bottom">
	<div class="item_list">
    	<ul>
	    	<c:forEach items="${list}" var="obj" varStatus="status">
	    		<li>${status.index + 1 }、${obj.content }</li>
	    	</c:forEach>
        </ul>
    </div>
   
</div> 

</body>
</html>