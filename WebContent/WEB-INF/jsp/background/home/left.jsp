<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>左侧导航</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<style>
.sub-menu li a.current{
	color:#006dcc;
}
</style>
<script>
$(document).ready(function(){
	$(".sub-menu li a").click(function(){
		$(".sub-menu li a").removeClass("current");
		$(this).addClass("current");
	});
});
</script>
</head>
<body>
<div class="sidebar-wrap">
    <div class="sidebar-content">
        <ul class="sidebar-list">
            <li>
                <a href="#"><i class="icon-font">&#xe003;</i>常用操作</a>
                <ul class="sub-menu">
                	<c:if test="${user_session.utype == 1 }">
                		<li><a href="<%=pre%>/b_notice/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>消息通知管理</a></li>
	                	<li><a href="<%=pre%>/b_logistics/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>物流公司管理</a></li>
	                    <li><a href="<%=pre%>/b_user/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>用户管理</a></li>
	                    <li><a href="<%=pre%>/b_card/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>银行卡管理</a></li>
	                    <li><a href="<%=pre%>/b_dingdan/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>订单管理</a></li>
	                    <li><a href="<%=pre%>/b_payment/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>付款管理</a></li>
	                    <li><a href="<%=pre%>/b_xinwen/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>新闻管理</a></li>
	                    <li><a href="<%=pre%>/b_baseinfo/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>基础信息</a></li>
	                    <li><a href="<%=pre%>/b_adv/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>首页图片</a></li>
	                    <li><a href="<%=pre%>/b_category/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>产品类别管理</a></li>
                    	<li><a href="<%=pre%>/b_product/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>产品管理</a></li>
                    	<li><a href="<%=pre%>/b_video/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>视频管理</a></li>
                    	<li><a href="<%=pre%>/b_lianxi/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>联系方式</a></li>
                    	<li><a href="<%=pre%>/b_cuxiao/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>商务信息</a></li>
                    	<li><a href="<%=pre%>/b_anli/index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe005;</i>经典案例</a></li>
                    	<li><a href="<%=pre%>/b_user/changepwd<%=suf%>" target="mainFrame"><i class="icon-font">&#xe033;</i>修改密码</a></li>
                    </c:if>
                    <c:if test= "${user_session.utype == 2 }">
                    	<li><a href="<%=pre%>/b_cuxiao/c_index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe004;</i>商务信息</a></li>
                    	<li><a href="<%=pre%>/b_dingdan/c_index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe004;</i>我的订单</a></li>
                    	<li><a href="<%=pre%>/b_payment/c_index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe012;</i>我的付款</a></li>
	                    <li><a href="<%=pre%>/b_user/c_index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe052;</i>个人信息</a></li>
	                    <li><a href="<%=pre%>/b_user/c_changepwd<%=suf%>" target="mainFrame"><i class="icon-font">&#xe033;</i>修改密码</a></li>
	                    <li><a href="<%=pre%>/b_card/c_index<%=suf%>" target="mainFrame"><i class="icon-font">&#xe033;</i>银行账号</a></li>
                    </c:if>
                </ul>
            </li>
           
        </ul>
    </div>
</div>
<!--/sidebar-->
</body>
</html>