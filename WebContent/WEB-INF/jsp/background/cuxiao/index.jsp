<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData = request.getParameter("content") != null ? request.getParameter("content") : "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>促销页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新闻编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/autoheight/autoheight.js"></script>
<style>
#saveButton{padding:5px 15px;margin:10px 0px 20px 0px;}
.cuxiao_title{padding-bottom:5px;font-size:18px;}
</style>
<script>
KindEditor.ready(function(K) {
	var editor = K.create('textarea[name="content"]', {
		cssPath : '<%=kePath %>/plugins/code/prettify.css',
		uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
		fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
		autoHeightMode:true
	});
	prettyPrint();
	
	$("#saveButton").click(function(){
		editor.sync();
		var id = $('#id').val();
		var content = $('#content').val();
		$.ajax({
			url : "<%=pre%>/b_cuxiao/update<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,content:content},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						art.dialog.tips('<span style="color:#f00">保存成功！</span>', 1.5);
					} else {
						art.dialog.tips('<span style="color:#f00">保存失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">保存失败！</span>', 1);
			}
		});
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">商务信息</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<div>
			<input type="hidden" id="id" value="${bean.id }">
			<%=htmlData%>
	    	<textarea id="content" name="content" style="width:100%;height:400px;">
				<%=htmlspecialchars(htmlData)%>${bean.content }
			</textarea>
		</div>
		<input type="button" value="保存" class="button" id="saveButton" />
	</div>
</div>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>
</body>
</html>