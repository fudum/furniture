<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData = request.getParameter("content") != null ? request.getParameter("content") : "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>产品添加</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/autoheight/autoheight.js"></script>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js%>/ajaxfileupload.js"></script>
<script>
KindEditor.ready(function(K) {
	var editor = K.create('textarea[name="content"]', {
		cssPath : '<%=kePath %>/plugins/code/prettify.css',
		uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
		fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
		autoHeightMode:true
	});
	prettyPrint();
	
	$("#saveButton").click(function(){
		editor.sync();
		var id = $('#id').val();
		var pid = $('#pid').val();
		var name = $('#name').val();
		var guige = $('#guige').val();
		var beizhu = $('#beizhu').val();
		var info = $('#content').val();
		var picpath = $('#picpath').val();
		var categoryid = $('#categoryid').val();
		var categoryname =  $("#categoryid").find("option:selected").text();
		if(categoryid == '0'){
			alert('请选择产品类别');
			return;
		}
		
		$.ajax({
			url : "<%=pre%>/b_product/update<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,pid:pid,name:name,guige:guige,beizhu:beizhu,info:info,picpath:picpath,
				categoryid:categoryid,categoryname:categoryname},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">编辑成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_product/index<%=suf%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
			}
		});
	});
});

$(document).ready(function(){
	$("#categoryid_old").change(function(){
		getSecCategory();
	});
	//上传图片
	$("#uploadFile").live('change',function(){
		var picFile = $("#uploadFile").val();
		var wjjname = "product";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".jpeg|.gif|.jpg|.png|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/common/fileupload<%=suf%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'uploadFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#picpath").val(data.path);
				            		$("#catalogtip").attr("src","<%=upload%>/"+ data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：图片格式的文件！");
				}
			}
		}
	});
	
});

function getSecCategory(){
	var categoryid = $("#categoryid").val();
	if(categoryid == 0){
		$("#secDiv").hide();
	}
	else{
		$.ajax({
			url : "<%=pre%>/b_product/getSecCategory<%=suf%>",
			type : 'post',
			dataType : 'json',
			data : {parentid:categoryid},
			success : function(data, status) {
				if (status == 'success') {
					var tmp = "";
					if(data == null){
						tmp += "<option value='0'>没有二级产品类别</option>";
					}
					else{
						$.each(data.category, function(i, item) {
							tmp += "<option value='"+item.id+"'>"+item.name+"</option>";
						});
					}
					$("#categoryidSec").empty();
					$("#categoryidSec").append(tmp);
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">保存失败！</span>', 1);
			}
		});
		$("#secDiv").show();
	}
}
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">产品管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">产品编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<table class="insert-tab">
			<tr>
				<td width="100px">产品编号：</td>
				<td>
					<input id ="id" type="hidden" value="${bean.id }"/>
					<input id="pid" name="pid" type="text" value="${bean.pid }"/>
				</td>
				<td width="100px">产品名称：</td>
				<td>
					<input id="name" name="name" type="text" value="${bean.name }"/>
				</td>
			</tr>
			<tr>
				<td width="100px">产品规格：</td>
				<td>
					<input id="guige" name="guige" type="text" value="${bean.guige}"/>
				</td>
				<td width="100px">产品备注：</td>
				<td>
					<input id="beizhu" name="beizhu" type="text" value="${bean.beizhu }"/>
				</td>
			</tr>
			<tr>
				<td width="100px">产品类别：</td>
				<td colspan="3">
					<div style="float:left;">
						<select id="categoryid">
							<option value="0">--请选择产品类别--</option>
							<c:forEach items="${listOne }" var = "obj" varStatus = "status">
								<option <c:if test="${categoryid == obj.id }">selected="selected"</c:if> value="${obj.id}">${obj.name}</option>
							</c:forEach>
						</select>
					</div>
					
					
				</td>
			</tr>
			<tr>
				<td>产品图片：</td>
				<td colspan="3">
					<input type="file" name="uploadFile" id="uploadFile">
					<input type="hidden" id="picpath" value="${bean.picpath }">
					<img src="<%=upload %>/${bean.picpath}" id="catalogtip"  width="200px" height="100px"/>
					<span style="color:red"></span>
				</td>
			</tr>
			<tr>
				<td>产品详情：</td>
				<td colspan="3">
					<%=htmlData%>
			    	<textarea id="content" name="content" style="width:95%;height:400px;">
						<%=htmlspecialchars(htmlData)%>${bean.info}
					</textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" value="保存" class="button" id="saveButton" />
					<a href="<%=pre%>/b_product/index<%=suf%>" style="padding-left:10px">返回</a>
				</td>
				<td></td>
			</tr>										
		</table>
	</div>
</div>
</body>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>
</html>