<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页广告-编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script src="<%=js%>/fileupload.js"></script>
<script src="<%=js%>/ajaxfileupload.js"></script>
<script>
$(document).ready(function(){
	//保存按钮
	$("#saveButton").click(function() {
		var picpath = $("#picpath").val();
		var porder = $("#porder").val();
		var id = $("#id").val();
		var piclink = $("#piclink").val();
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_adv/update<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,picpath:picpath,porder:porder,piclink:piclink},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">编辑成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_adv/index<%=suf%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
	
	//上传图片
	$("#uploadFile").live('change',function(){
		var picFile = $("#uploadFile").val();
		var wjjname = "advertisement";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".jpeg|.gif|.jpg|.png|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/common/fileupload<%=suf%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'uploadFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#catalogtip").attr("src","<%=upload%>/"+ data.path);
				            		$("#picpath").val(data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：图片格式的文件！");
				}
			}
		}
	});
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">首页图片管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">图片编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<input type="hidden" id="id" value="${bean.id }">
		<table class="insert-tab">
		<tr>
			<td>显示顺序：</td>
			<td><input type="text" name="porder" id="porder" value="${bean.porder}"> <span style="color:red">*0代表：工厂商标，1代表：工厂理念</span></td>
		</tr>
		<tr>
			<td>跳转到的链接：</td>
			<td><input type="text" name="piclink" id="piclink" style="width:80%" value="${bean.piclink }"></td>
		</tr>
		<tr>
			<td>图片文件：</td>
			<td>
				<input type="file" name="uploadFile" id="uploadFile">
				<input id="picpath" type="hidden" value="${bean.picpath}">
				<img src="<%=upload%>/${bean.picpath}" width="200px" height="100px"/>
				<span style="color:red">*建议图片大小1200*470px</span>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="保存" class="button" id="saveButton" />
				<a href="<%=pre%>/b_adv/index<%=suf%>" style="padding-left:10px">返回</a>
			</td>
			<td></td>
		</tr>
	</table>
	</div>
</div>
</body>
</html>