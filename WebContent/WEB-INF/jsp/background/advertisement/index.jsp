<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首页广告管理</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">首页图片管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-title">
	    <div class="result-list">
		    <a href="<%=pre%>/b_adv/add<%=suf%>"><i class="icon-font"></i>新增图片</a>
	    </div>
	</div>
	<div class="result-content">
		<table class="result-tab" >
			<tr>
				<td>图片</td>
				<td>排序</td>
				<td>跳转到的链接</td>
				<td>插入时间</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
				<tr bgcolor="#FFFFFF" align="center">
					<c:if test="${status.index <= 1 }">
						<td><img src="<%=upload%>/${obj.picpath}" style="width:100px;height:100px;"></td>
					</c:if>
					<c:if test="${status.index > 1 }">
						<td><img src="<%=upload%>/${obj.picpath}" style="width:200px;height:100px;"></td>
					</c:if>
					<td>${obj.porder}</td>
					<c:if test="${status.index == 0 }">
						<td style="color:red">首页左侧图片</td>
					</c:if>
					<c:if test="${status.index == 1 }">
						<td style="color:red">首页右侧图片</td>
					</c:if>
					<c:if test="${status.index > 1 }">
						<td style="color:red"></td>
					</c:if>
					<td>${obj.inserttime }</td>
					<td>
						<a href="<%=pre%>/b_adv/edit<%=suf%>?id=${obj.id }">编辑</a>
						<c:if test="${status.index > 1  }">
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_adv/delete<%=suf%>?id=${obj.id}');">删除</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>