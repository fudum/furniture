<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>银行卡-编辑</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var bankname = $("#bankname").val();
		var cardid = $("#cardid").val();
		var cardname = $("#cardname").val();
		if(cardid.replace(/\s/g,"")==''){
			$("#cardidtip").html('*请输入银行账号');
			return;
		}
		if(cardname.replace(/\s/g,"")==''){
			$("#cardnametip").html('*请输入开户名称');
			return;
		}
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_card/update<%=suf%>',
			data:{id:id,bankname:bankname,cardid:cardid,cardname:cardname},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_card/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">银行卡管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">银行卡编辑</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab" width="100%">
            <tbody>
            	<tr>
                	<th width="120"><i class="require-red">*</i>开户银行：</th>
	                <td>
	                	<input type="hidden" value="${bean.id }" id="id"> 
	                   <select id="bankname" class="required">
                        	<c:forEach items="${listBank }" var="obj">
                        		<option value="${obj}" <c:if test="${bean.bankname == fn:substring(obj,6,fn:length(obj)) }">selected="selected"</c:if>>${fn:substring(obj,6,fn:length(obj))}</option>
                        	</c:forEach>
                        </select>
	                </td>
           	 	</tr>
                <tr>
                    <th><i class="require-red">*</i>开户名称：</th>
                    <td>
                        <input class="common-text required" id="cardname" size="30" type="text" value="${bean.cardname }">
                    	<i class="require-red" id="cardnametip"></i>
                    </td>
                </tr>
                <tr>
                    <th><i class="require-red">*</i>银行卡号：</th>
                    <td>
                        <input class="common-text required" id="cardid" size="60" type="text" value="${bean.cardid }">
                    	<i class="require-red" id="cardidtip"></i>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="提交" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>