<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>视频管理</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<script>
$(document).ready(function(){
	$("#btn_add").click(function(){
		var id = $("#id").val();
		var path = $("#path").val();
		$.ajax({
			type:'post',
			url:'<%=pre%>/b_video/update<%=suf%>',
			data:{id:id,path:path},
			dataType:'json',
			success:function(data,status){
				if(data.ret == 1){
					var timer;
					art.dialog({
						width:150,
						height:80,
						content : '<span style="color:#f00">编辑成功!</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							window.location.href="<%=pre%>/b_video/index<%=suf%>";
						}
						
					}).show();			
				}
			},
			error:function(xhr, textStatus, errorThrown){
				
			}
		});
	});
	
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<span class="crumb-name">视频管理</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
        <table class="insert-tab">
            <tbody>
            	<tr>
                	<th width="120"><i class="require-red">*</i>视频路径：</th>
	                <td>
	                	<input type="hidden" value="${bean.id }" id="id"> 
	                	<input type="text" value="${bean.path }" id="path" style="width:600px;">
	                </td>
           	 	</tr>
                <tr>
                    <th></th>
                    <td>
                        <input class="btn btn-primary btn6 mr10" value="编辑" type="button" id="btn_add">
                        <input class="btn btn6" onClick="history.go(-1)" value="返回" type="button">
                        </td>
                    </tr>
                </tbody>
          </table>
	</div>
</div>
</body>
</html>