<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/background.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData = request.getParameter("content") != null ? request.getParameter("content") : "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新闻添加</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css"/>
<link rel="stylesheet" type="text/css" href="<%=css%>/main.css"/>
<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/autoheight/autoheight.js"></script>
<script>
KindEditor.ready(function(K) {
	var editor = K.create('textarea[name="content"]', {
		cssPath : '<%=kePath %>/plugins/code/prettify.css',
		uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
		fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
		autoHeightMode:true
	});
	prettyPrint();
	
	$("#saveButton").click(function(){
		editor.sync();
		var content = $('#content').val();
		var title=$("#title").val();
		var recommend = $("input[name='recommend']:checked").val();
		$.ajax({
			url : "<%=pre%>/b_xinwen/insert<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {title:title,content:content,recommend:recommend},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						art.dialog.tips('<span style="color:#f00">保存成功！</span>', 1.5);
					} else {
						art.dialog.tips('<span style="color:#f00">保存失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">保存失败！</span>', 1);
			}
		});
	});
});
</script>
</head>
<body>
<div class="crumb-wrap">
	<div class="crumb-list">
		<i class="icon-font"></i>
		<a href="index.html">首页</a>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">新闻管理</span>
		<span class="crumb-step">&gt;</span>
		<span class="crumb-name">新闻添加</span>
	</div>
</div>
<div class="result-wrap">
	<div class="result-content">
		<table class="insert-tab" style="width:100%">
			<tr>
				<td width="100px">新闻标题：</td>
				<td>
					<input id="title" name="title" type="text" style="width:600px"/>
				</td>
			</tr>
			<tr>
				<td width="100px">推荐到首页：</td>
				<td>
					<input type="radio" name="recommend" value="2" checked="checked"/>否
					<input type="radio" name = "recommend" value="1"/>是
				</td>
			</tr>
			<tr>
				<td>新闻内容：</td>
				<td>
					<%=htmlData%>
			    	<textarea id="content" name="content" style="width:95%;height:400px;">
						<%=htmlspecialchars(htmlData)%>
					</textarea>
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" value="保存" class="button" id="saveButton" />
					<a href="<%=pre%>/b_xinwen/index<%=suf%>" style="padding-left:10px">返回</a>
				</td>
				<td></td>
			</tr>										
		</table>
	</div>
</div>
</body>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>
</html>