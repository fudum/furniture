/*
Navicat MySQL Data Transfer

Source Server         : 115.28.68.104
Source Server Version : 50546
Source Host           : 115.28.68.104:3306
Source Database       : furniture

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2016-03-13 09:19:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `porder` int(255) DEFAULT NULL COMMENT '显示顺序',
  `flag` varchar(2) DEFAULT NULL,
  `piclink` varchar(200) DEFAULT NULL COMMENT '是否显示',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='首页广告';

-- ----------------------------
-- Records of advertisement
-- ----------------------------
INSERT INTO `advertisement` VALUES ('A20150604210402665', 'advertisement/5facd63ea828421f8d252fb68f245fdf.jpg', '1', null, '', '2015-09-15 13:16:03');
INSERT INTO `advertisement` VALUES ('A20150604210429266', 'advertisement/3ec30fd4e21942b3b91dc47a092576bf.jpg', '2', null, '', '2015-09-19 14:40:44');
INSERT INTO `advertisement` VALUES ('A20150830214511512', 'advertisement/5e3fcfcc02cc4ecdbc0ebbce2e53c340.png', '0', null, '', '2015-09-15 13:18:49');
INSERT INTO `advertisement` VALUES ('A20150915132445820', 'advertisement/4b97466f50514a9a9cb5dfb5cf92e1c6.jpg', '3', null, '', '2015-09-15 13:25:31');

-- ----------------------------
-- Table structure for anli
-- ----------------------------
DROP TABLE IF EXISTS `anli`;
CREATE TABLE `anli` (
  `id` varchar(32) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `picpath` varchar(300) DEFAULT NULL,
  `miaoshu` varchar(200) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of anli
-- ----------------------------
INSERT INTO `anli` VALUES ('A20160201154103865', '样板房', 'anli/8d51feb6d56e4b5f921db4a58c82d0bc.jpg', null, '2016-02-23 12:59:44');
INSERT INTO `anli` VALUES ('A20160201155107538', '英山某温泉酒店', '', null, '2016-02-29 15:18:58');

-- ----------------------------
-- Table structure for anliinfo
-- ----------------------------
DROP TABLE IF EXISTS `anliinfo`;
CREATE TABLE `anliinfo` (
  `id` varchar(32) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `picpath` varchar(200) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  `anli_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of anliinfo
-- ----------------------------
INSERT INTO `anliinfo` VALUES ('A20160201154217976', '橱柜', 'anliinfo/48338fa9301144e88d757bbc3f79143b.jpg', '2016-02-01 15:42:17', 'A20160201154103865');
INSERT INTO `anliinfo` VALUES ('A20160201154314683', '书柜', 'anliinfo/96ebc56cd2614aa5bfb3ddd8383431b0.jpg', '2016-02-01 15:43:14', 'A20160201154103865');
INSERT INTO `anliinfo` VALUES ('A20160223130022693', 'wergw ', 'anliinfo/2a2937bbf8ad456eae4cf125627bc001.jpg', '2016-02-23 13:00:22', 'A20160201154103865');
INSERT INTO `anliinfo` VALUES ('A20160223130102658', '', '', '2016-02-23 13:01:02', 'A20160201155107538');

-- ----------------------------
-- Table structure for baseinfo
-- ----------------------------
DROP TABLE IF EXISTS `baseinfo`;
CREATE TABLE `baseinfo` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `fType` varchar(50) DEFAULT NULL COMMENT '类别',
  `content` text COMMENT '内容',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='关于我们、联系我们、人才招聘';

-- ----------------------------
-- Records of baseinfo
-- ----------------------------
INSERT INTO `baseinfo` VALUES ('B20150525074203880', '关于我们', '关于我们 &nbsp; 武汉金喜装饰材料厂<img src=\"/upload/furniture/attached/image/20160124/20160124101800_769.jpg\" alt=\"\" />', '2016-01-24 10:18:05');
INSERT INTO `baseinfo` VALUES ('B20150525074203881', '人才招聘', '人材招聘', '2015-05-26 19:40:37');
INSERT INTO `baseinfo` VALUES ('B20150525074203882', '联系我们', '联系我们', '2015-05-26 19:40:47');

-- ----------------------------
-- Table structure for card
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `bankname` varchar(50) DEFAULT NULL COMMENT '银行名称',
  `cardid` varchar(50) DEFAULT NULL COMMENT '银行卡号',
  `cardname` varchar(50) DEFAULT NULL COMMENT '账户名称',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card
-- ----------------------------
INSERT INTO `card` VALUES ('C20150205161005641', '中国建设银行', '4367 8888888888', '张国兵', '95533.png', '2015-10-29 12:52:54');
INSERT INTO `card` VALUES ('C20150205161044538', '中国农业银行', '95599 88888888888888', '张国兵', '95599.png', '2015-10-29 12:52:45');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `plevel` varchar(50) DEFAULT NULL COMMENT '显示级别，1,2',
  `shunxu` int(11) DEFAULT NULL COMMENT '显示顺序',
  `parentId` varchar(32) DEFAULT NULL COMMENT '父节点id',
  `parentName` varchar(50) DEFAULT NULL COMMENT '父节点名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品类别';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('CO20150604212851641', '彩饰板橱柜门板', '1', null, null, null, '2015-06-04 21:28:51');
INSERT INTO `category` VALUES ('CO20150604212900358', 'UV烤柒橱柜门板', '1', null, null, null, '2015-06-04 21:29:00');
INSERT INTO `category` VALUES ('CO20150619082108713', '亚克力门板', '1', null, null, null, '2015-06-22 10:03:56');
INSERT INTO `category` VALUES ('CO20150915132827986', '橱柜台面', '1', null, null, null, '2015-09-15 13:28:27');
INSERT INTO `category` VALUES ('CO20150915132855638', '橱柜五金', '1', null, null, null, '2015-09-15 13:28:55');
INSERT INTO `category` VALUES ('CO20151029125409727', '衣柜五金', '1', null, null, null, '2015-10-29 12:54:17');
INSERT INTO `category` VALUES ('CO20151108113741400', '兔宝宝E0级生态板衣柜', '1', null, null, null, '2015-11-14 14:36:42');
INSERT INTO `category` VALUES ('CO20151114143706556', '露水河E0级衣柜', '1', null, null, null, '2015-11-14 14:37:06');
INSERT INTO `category` VALUES ('CT20150604212923554', '彩饰田园系列2', '2', '1', 'CO20150604212851641', '彩饰板橱柜门板', '2015-06-19 08:21:27');

-- ----------------------------
-- Table structure for cuxiao
-- ----------------------------
DROP TABLE IF EXISTS `cuxiao`;
CREATE TABLE `cuxiao` (
  `id` varchar(32) NOT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '促销内容',
  `inserttime` text COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cuxiao
-- ----------------------------
INSERT INTO `cuxiao` VALUES ('1', '<p>\n	兔宝宝衣柜上市\n</p>\n<p>\n	亚克力新色全面上市\n</p>\n<p>\n	衣柜配件<img src=\"/upload/furniture/attached/image/20160124/20160124101356_846.jpg\" alt=\"\" />\n</p>\n<p>\n	<br />\n</p>', '2016-01-24 10:14:01');

-- ----------------------------
-- Table structure for dingdan
-- ----------------------------
DROP TABLE IF EXISTS `dingdan`;
CREATE TABLE `dingdan` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `ddnum` varchar(50) DEFAULT NULL COMMENT '订单号',
  `userid` varchar(32) DEFAULT NULL COMMENT '用户编号',
  `username` varchar(100) DEFAULT NULL COMMENT '业主名',
  `ddtime` varchar(20) DEFAULT NULL COMMENT '订单时间',
  `yujifahuotime` varchar(20) DEFAULT NULL COMMENT '预计发货时间',
  `fahuotime` varchar(20) DEFAULT NULL COMMENT '发货时间',
  `huohao` varchar(100) DEFAULT NULL COMMENT ' 货号',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `ddstatus` varchar(20) DEFAULT NULL COMMENT '订单状态',
  `info` varchar(200) DEFAULT NULL COMMENT '订单详情',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `yezhu` varchar(50) DEFAULT NULL COMMENT '业主',
  `excelinfo` varchar(200) DEFAULT NULL COMMENT 'excel版订单详情',
  `exceltwo` varchar(200) DEFAULT NULL COMMENT 'excel订单详情2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dingdan
-- ----------------------------
INSERT INTO `dingdan` VALUES ('D20160118093408914', '001', 'U20151116141549583', '张国兵', '2016-01-18 09:34:08', '2015-12-29', '2016-01-06', '大道002', '888.00', '已发货', null, '2016-01-23 14:15:21', '文先生', 'dingdan/45d971d26cbf446fb8775bf7d61dc67a.xls', 'dingdan/2feb035f4891468f98e436ff870fbcb8.xls');
INSERT INTO `dingdan` VALUES ('D20160124101118930', '002', 'U20151116141549583', '张国兵', '2016-01-24 10:11:18', '2015-12-29', '2016-01-25', '田园98', '1000.00', '已发货', null, '2016-01-24 10:12:43', '沈世伟', 'dingdan/dba03914f925412ab7ed14c2ca0adc30.xls', 'dingdan/12e38fc5e7a843e3b924757489a17c0b.xls');
INSERT INTO `dingdan` VALUES ('D20160309100249551', '101', 'U20160309100206389', '杨欣', '2016-03-09 10:02:49', '', '2016-03-09', '大道542-3', null, '已发货', null, '2016-03-09 10:04:10', '黄小说', 'dingdan/2013ef2f12a84949a486d70d5fc9c3ac.xls', '');
INSERT INTO `dingdan` VALUES ('D20160309100818892', '102', 'U20160309100206389', '杨欣', '2016-03-09 10:08:18', '', '2016-03-08', '金星69-7', '5000.00', '待生产', null, '2016-03-09 10:08:18', '虎小姐', 'dingdan/e956929f1a8c4c6aa82778358afc72e7.xls', '');

-- ----------------------------
-- Table structure for lianxi
-- ----------------------------
DROP TABLE IF EXISTS `lianxi`;
CREATE TABLE `lianxi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `scphone` varchar(50) DEFAULT NULL COMMENT '生产中心',
  `xfphone` varchar(50) DEFAULT NULL COMMENT '消费中心',
  `zhanxianphone` varchar(50) DEFAULT NULL COMMENT '专线',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `longitude` varchar(50) DEFAULT NULL COMMENT '经度',
  `latitude` varchar(50) DEFAULT NULL COMMENT '纬度',
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='联系方式';

-- ----------------------------
-- Records of lianxi
-- ----------------------------
INSERT INTO `lianxi` VALUES ('1', '武汉市汉阳区磨山集团山南湾41号（百度导航搜索“金喜装饰材料厂”可直接导航）', '027-86582243', '027-84631533（对账及发货专用）', '13098805148（沈先生）   13098817570（张先生）', '027-84627919', null, null, '2015-10-21 12:13:06');

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of logistics
-- ----------------------------
INSERT INTO `logistics` VALUES ('L20150201094208866', '大道物流', '', '2015-03-08 09:32:01');
INSERT INTO `logistics` VALUES ('L20150308093224827', '小愚公', '', '2015-03-08 09:32:24');
INSERT INTO `logistics` VALUES ('L20150308093236939', '金典物流', '', '2015-03-08 09:32:36');
INSERT INTO `logistics` VALUES ('L20150309135115787', '五通货运', '', '2015-03-09 13:51:15');
INSERT INTO `logistics` VALUES ('L20150309135215431', '新时代', '', '2015-03-09 13:52:15');
INSERT INTO `logistics` VALUES ('L20150309135249262', '桥运天天', '', '2015-03-09 13:52:49');
INSERT INTO `logistics` VALUES ('L20150309135315248', '中运通', '', '2015-03-09 13:53:15');
INSERT INTO `logistics` VALUES ('L20150309135328373', '大地物流', '', '2015-03-09 13:53:28');
INSERT INTO `logistics` VALUES ('L20150309135354108', '益多物流', '', '2015-03-09 13:53:54');
INSERT INTO `logistics` VALUES ('L20150309135957236', '非凡物流', '', '2015-03-09 13:59:57');
INSERT INTO `logistics` VALUES ('L20150309150959591', '自提', '', '2015-03-09 15:09:59');
INSERT INTO `logistics` VALUES ('L20150311094112870', '裕发物流', '', '2015-03-11 09:41:12');
INSERT INTO `logistics` VALUES ('L20150311095554570', '联创方圆', '', '2015-03-11 09:55:54');
INSERT INTO `logistics` VALUES ('L20151114144039122', '武汉自提', '客户上门提货，运费自付', '2015-11-14 14:40:39');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `norder` int(11) DEFAULT NULL COMMENT '显示顺序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息通知';

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('C20160108131711548', '新系统2016年1月8日正式开始试用', null, '2016-01-08 13:17:11');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `dengjitime` varchar(20) DEFAULT NULL COMMENT '登记时间',
  `daozhangtime` varchar(20) DEFAULT NULL COMMENT '到账时间',
  `shishoumoney` decimal(10,0) DEFAULT NULL COMMENT '实收金额',
  `huokuanmoney` decimal(10,0) DEFAULT NULL COMMENT '货款金额',
  `shouxufei` decimal(10,0) DEFAULT NULL COMMENT '手续费',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `userid` varchar(32) DEFAULT NULL COMMENT '业主ID',
  `username` varchar(255) DEFAULT NULL COMMENT '业主名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payment
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `pid` varchar(50) DEFAULT NULL COMMENT '产品编号',
  `name` varchar(50) DEFAULT NULL COMMENT '产品名称',
  `guige` varchar(50) DEFAULT NULL COMMENT '产品规格',
  `categoryid` varchar(32) DEFAULT NULL COMMENT '产品类别id',
  `categoryname` varchar(50) DEFAULT NULL COMMENT '产品类别名称',
  `beizhu` varchar(200) DEFAULT NULL COMMENT '备注',
  `info` text COMMENT '产品说明',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `clicknum` int(11) DEFAULT NULL COMMENT '点击数',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品';

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('P20160107094033759', 'A-801', '枣红', '定做', 'CO20150604212851641', '彩饰板橱柜门板', '基材为E-1级颗粒板', '<p>\n	枣红色<img src=\"/upload/furniture/attached/image/20160122/20160122160501_675.jpg\" alt=\"\" />\n</p>\n<p>\n	<br />\n</p>', '2016-01-22 16:05:26', null, 'product/a7d6a06cc1344adaa882a35fd883dd20.jpg');
INSERT INTO `product` VALUES ('P20160107094309109', 'A-802', '橘黄', '定做', 'CO20150604212851641', '彩饰板橱柜门板', '颗粒板', '', '2016-01-07 09:43:09', null, 'product/418ad58588f7418ea07a304c5d06eaa5.jpg');
INSERT INTO `product` VALUES ('P20160107094530890', 'L-01', 'G型隐形拉手铝合金本色', '定做', 'CO20150915132855638', '橱柜五金', '', '', '2016-01-07 09:47:05', null, 'product/2140650446ca44199cb8b5ae425f41b5.jpg');
INSERT INTO `product` VALUES ('P20160120102748441', 'A-811', '竹节木', '定做', 'CO20150604212851641', '彩饰板橱柜门板', '', '海尔竹节木', '2016-01-22 16:09:44', null, 'product/4e586ccff0194bd6af755c695d376106.png');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `userid` varchar(50) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话号码',
  `phonesec` varchar(50) DEFAULT NULL COMMENT '对单电话',
  `qq` varchar(50) DEFAULT NULL COMMENT 'qq号码',
  `other` varchar(500) DEFAULT NULL COMMENT '备注',
  `addressto` varchar(200) DEFAULT NULL COMMENT '货到地区',
  `address` varchar(200) DEFAULT NULL COMMENT '所在地',
  `wuliu` varchar(50) DEFAULT NULL COMMENT '要求物流',
  `utype` varchar(50) DEFAULT NULL COMMENT '用户类别:1管理员，2普通用户',
  `dianmianname` varchar(50) DEFAULT NULL COMMENT '店面名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `excelinfo` varchar(200) DEFAULT NULL COMMENT '用户excel信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '某某公司', '21232F297A57A5A743894A0E4A801FC3', '男', '13871043702', null, null, null, null, '武汉理工', null, '1', null, null, null);
INSERT INTO `user` VALUES ('U20150308093554277', '0717-6096566', '申胤', '666666', null, '133333', '144444', '', '', '宜昌', '宜昌', '金典物流', '2', '宜昌鼎派', '2015-11-30 15:04:21', null);
INSERT INTO `user` VALUES ('U20150309135031716', '13581496097', '李建波', '666666', null, '13581496097', '13581496097', '2911410161', '', '当阳', '当阳', '金典物流', '2', '当阳李建波', '2015-11-17 11:31:31', null);
INSERT INTO `user` VALUES ('U20150309135631389', '13995951498', '唐巨明', '666666', null, '13995951498', '13995951498', '', '', '黄石', '黄石', '小愚公', '2', '黄石万家', '2015-11-17 11:32:49', null);
INSERT INTO `user` VALUES ('U20150309135802571', '13035306868', '郑', '666666', null, '13035306868', '13035306868', '', '', '松滋', '松滋', '中运通', '2', '松滋爱之味', '2015-11-17 11:33:09', null);
INSERT INTO `user` VALUES ('U20150309135917104', '15826844415', '张国斌', '666666', null, '15826844415', '15826844415', '', '', '汉川', '汉川', '大道物流', '2', '汉川贝斯特', '2015-11-17 11:33:26', null);
INSERT INTO `user` VALUES ('U20150309140105363', '13972495926', '十堰索华', '666666', null, '13972495926', '13972495926', '', '', '十堰', '十堰', '非凡物流', '2', '十堰索华', '2015-03-09 14:01:05', null);
INSERT INTO `user` VALUES ('U20150309150930983', '13377873103', '袁峰', '666666', null, '13377873103', '13377873103', '', '以网店为主要金鹰模式', '武汉', '武汉', '大道物流', '2', '武汉袁木匠', '2015-11-17 11:33:43', null);
INSERT INTO `user` VALUES ('U20150309151642361', '13971430333', '于', '666666', null, '13971430333', '13971430333', '', '', '武汉', '武汉', '自提', '2', '武汉柏恩', '2015-11-17 11:34:24', null);
INSERT INTO `user` VALUES ('U20150309151826659', '18908637578', '代化冰', '666666', null, '18908637578', '18908637578', '', '', '武汉', '武汉', '自提', '2', '武汉玉立', '2015-11-17 11:34:08', null);
INSERT INTO `user` VALUES ('U20150309152306873', '13217160601', '荆州爱普', '666666', null, '13217160601', '13217160601', '', '', '荆州', '荆州', '五通货运', '2', '荆州爱普', '2015-03-09 15:23:06', null);
INSERT INTO `user` VALUES ('U20150309152528225', '18671123665', '鄂州家之橱', '666666', null, '18671123665', '18671123665', '', '', '鄂州', '鄂州', '大道物流', '2', '鄂州家之橱', '2015-03-09 15:25:28', null);
INSERT INTO `user` VALUES ('U20150309152741219', '13886079557', '魏文祥', '666666', null, '13886079557', '13886079557', '', '', '江夏', '江夏', '益多物流', '2', '江夏爱之味', '2015-11-17 11:34:58', null);
INSERT INTO `user` VALUES ('U20150311085121919', '15997890818', '应山嘉禾', '666666', null, '15997890818', '15997890818', '', '', '应山', '应山', '大道物流', '2', '应山嘉禾', '2015-03-11 08:51:21', null);
INSERT INTO `user` VALUES ('U20150311085739814', '0710-3234302', '襄樊帅康', '666666', null, '0710-3234302', '0710-3234302', '', '', '襄樊', '襄樊', '非凡物流', '2', '襄樊帅康', '2015-03-11 08:57:39', null);
INSERT INTO `user` VALUES ('U20150311090133276', '13886871278', '刘瑞成', '666666', null, '13886871278', '13886871278', '', '', '随州', '随州', '大道物流', '2', '随州刘瑞成', '2015-11-17 11:35:15', null);
INSERT INTO `user` VALUES ('U20150311090527106', '15387192708', '鄂州奥田', '666666', null, '15387192708', '15387192708', '', '', '鄂州', '鄂州', '大道物流', '2', '鄂州奥田', '2015-03-11 09:05:27', null);
INSERT INTO `user` VALUES ('U20150311094002145', '15299820277', '新疆小天鹅', '666666', null, '15299820277', '15299820277', '', '', '新疆', '新疆', '裕发物流', '2', '新疆小天鹅', '2015-03-11 09:41:26', null);
INSERT INTO `user` VALUES ('U20150311095538347', '0716-4160395', '沙市郑伟', '666666', null, '0716-4160395', '0716-4160395', '', '', '沙市', '沙市', '联创方圆', '2', '沙市郑伟', '2015-03-11 09:56:19', null);
INSERT INTO `user` VALUES ('U20150311100109476', '15327612888', '秭归刘爱国', '666666', null, '15327612888', '15327612888', '', '', '秭归', '秭归', '大道物流', '2', '秭归刘爱国', '2015-03-11 10:01:09', null);
INSERT INTO `user` VALUES ('U20150311100347748', '13797751860', '襄樊李莉', '666666', null, '13797751860', '13797751860', '', '', '襄樊', '襄樊', '大道物流', '2', '襄樊李莉', '2015-03-11 10:03:47', null);
INSERT INTO `user` VALUES ('U20151116141549583', '84631533', '张国兵', '666666', null, '13098817570', '13098805148', '', '', '武汉', '武汉', '武汉自提', '2', '武汉金喜', '2015-11-22 15:08:16', null);
INSERT INTO `user` VALUES ('U20160309100206389', '15927352009', '杨欣', '666666', null, '1592735009', '', '', '', '武汉', '武汉', '武汉自提', '2', '杨欣', '2016-03-09 10:02:06', null);

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `path` varchar(500) DEFAULT NULL COMMENT '视频路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频';

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES ('1', 'http://v.youku.com/v_show/id_XMTQ2MjM0OTc3Ng==.html', '2016-02-03 12:51:27');

-- ----------------------------
-- Table structure for xinwen
-- ----------------------------
DROP TABLE IF EXISTS `xinwen`;
CREATE TABLE `xinwen` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `title` varchar(200) DEFAULT NULL COMMENT '新闻标题',
  `category` varchar(50) DEFAULT NULL COMMENT '新闻类别',
  `content` text COMMENT '新闻内容',
  `auther` varchar(50) DEFAULT NULL COMMENT '新闻作者',
  `ip` varchar(50) DEFAULT NULL COMMENT '发布的ip',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `recommend` varchar(50) DEFAULT NULL COMMENT '是否推荐到首页',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xinwen
-- ----------------------------
INSERT INTO `xinwen` VALUES ('X20160108131230992', '新系统正式试用', null, '从2016年1月8日，新系统正式试用', null, null, '2016-01-08 13:13:32', '1');
