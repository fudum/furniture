function navList(id) {
    var $obj = $("#nav_dot"), $item = $("#J_nav_" + id);
    $item.addClass("on").parent().removeClass("none").parent().addClass("selected");
    
    $obj.find("p").hover(function () {
        if ($(this).hasClass("on")) { return; }
        $(this).addClass("hover");
    }, function () {
        if ($(this).hasClass("on")) { return; }
        $(this).removeClass("hover");
    });
    
    //初始化展开菜单
    var navId = $("#categoryid").val();
    var $div = $("#" + navId).siblings(".list-item");
    if ($("#" + navId).parent().hasClass("selected")) {
        $div.slideUp();
        $("#" + navId).parent().removeClass("selected");
    }
    if ($div.is(":hidden")) {
        $("#nav_dot li").find(".list-item").slideUp();
        $("#nav_dot li").removeClass("selected");
        $("#" + navId).parent().addClass("selected");
        $div.show();
    } else {
        $div.slideUp();
    }
    
    
    $obj.find("span").click(function () {
        var $div = $(this).siblings(".list-item");
        if ($(this).parent().hasClass("selected")) {
            $div.slideUp(600);
            $(this).parent().removeClass("selected");
        }
        if ($div.is(":hidden")) {
            $("#nav_dot li").find(".list-item").slideUp(600);
            $("#nav_dot li").removeClass("selected");
            $(this).parent().addClass("selected");
            $div.slideDown(600);
        } else {
            $div.slideUp(600);
        }
    });
}