/**
 * 上传图片
 * @param url 上传路径
 * @param uploadPath upload路径
 * @param wjjname 保存文件夹名称
 * @param imgId 回传展示图id
 * @param inputId 回传inputid
 */
function uploadFile(url,uploadPath,wjjname,imgId,inputId){
	var picFile = $("#uploadFile").val();
	if(picFile == ""){
		art.dialog.alert("请选择文件!");
		return;
	}
	else{
		var strFilter = ".jpeg|.gif|.jpg|.png|";
		if(picFile.indexOf(".") > -1){
			var p = picFile.lastIndexOf(".");
			var strPostfix = picFile.substring(p,picFile.length) + '|';
			strPostfix = strPostfix.toLowerCase();
			if(strFilter.indexOf(strPostfix) > -1){
				$.ajaxFileUpload({
					url: url, 
					data:{wjjname:wjjname},
					type: 'post',
					fileElementId: 'uploadFile',
					dataType:'json', 
					success: function(data, status){ 
			            	if(data.ret == 1){
			            		$("#" + imgId).attr("src",uploadPath + data.path);
			            		$("#" + inputId).val(data.path);
			            	}
			            	else{
			            		art.dialog.alert("上传失败,请重新上传！");
			            	}
			            },
			            error: function(data, status, e){ 
			            	art.dialog.tips('<span style="color:#f00">上传失败，请重新上传！</span>', 1.5);
			            }
				});
			}
			else{
				art.dialog.alert("请选择jpeg,gif,jpg,png格式的文件！");
			}
		}
	}
}

function uploadExcel(url,uploadPath,wjjname,inputId){
	var picFile = $("#uploadFile").val();
	
	if(picFile == ""){
		art.dialog.alert("请选择文件!");
		return;
	}
	else{
		var name_index = picFile.lastIndexOf("\\");
		var picFileName = picFile.substring(name_index + 1,picFile.length)
		
		var strFilter = ".xlsx|.xls|";
		if(picFile.indexOf(".") > -1){
			var p = picFile.lastIndexOf(".");
			var strPostfix = picFile.substring(p,picFile.length) + '|';
			strPostfix = strPostfix.toLowerCase();
			if(strFilter.indexOf(strPostfix) > -1){
				$.ajaxFileUpload({
					url: url, 
					data:{wjjname:wjjname},
					type: 'post',
					fileElementId: 'uploadFile',
					dataType:'json', 
					success: function(data, status){ 
			            	if(data.ret == 1){
			            		$("#" + inputId).val(data.path);
			            		$("#tip").show();
			            		$("#tip").text(picFileName);
			            		$("#" + spanId).html(data.path);
			            	}
			            	else{
			            		art.dialog.alert("上传失败,请重新上传！");
			            	}
			            },
			            error: function(data, status, e){ 
			            	art.dialog.tips('<span style="color:#f00">上传失败，请重新上传！</span>', 1.5);
			            }
				});
			}
			else{
				art.dialog.alert("请选择xlsx,xls格式的文件！");
			}
		}
	}
}

function uploadExcelTwo(url,uploadPath,wjjname,inputId){
	var picFile = $("#uploadFileTwo").val();
	
	if(picFile == ""){
		art.dialog.alert("请选择文件!");
		return;
	}
	else{
		var name_index = picFile.lastIndexOf("\\");
		var picFileName = picFile.substring(name_index + 1,picFile.length)
		
		var strFilter = ".xlsx|.xls|";
		if(picFile.indexOf(".") > -1){
			var p = picFile.lastIndexOf(".");
			var strPostfix = picFile.substring(p,picFile.length) + '|';
			strPostfix = strPostfix.toLowerCase();
			if(strFilter.indexOf(strPostfix) > -1){
				$.ajaxFileUpload({
					url: url, 
					data:{wjjname:wjjname},
					type: 'post',
					fileElementId: 'uploadFileTwo',
					dataType:'json', 
					success: function(data, status){ 
			            	if(data.ret == 1){
			            		$("#" + inputId).val(data.path);
			            		$("#tiptwo").show();
			            		$("#tiptwo").text(picFileName);
			            	}
			            	else{
			            		art.dialog.alert("上传失败,请重新上传！");
			            	}
			            },
			            error: function(data, status, e){ 
			            	art.dialog.tips('<span style="color:#f00">上传失败，请重新上传！</span>', 1.5);
			            }
				});
			}
			else{
				art.dialog.alert("请选择xlsx,xls格式的文件！");
			}
		}
	}
}