package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.ILianxiDAO;

@Repository
public class LianxiDAOImpl extends BaseDAOImpl implements ILianxiDAO{

	@Override
	public String getNamespace() {
		return "furniture.mapper.Lianxi.";
	}
	
}
