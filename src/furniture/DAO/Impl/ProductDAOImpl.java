package furniture.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import furniture.Bean.ProductBean;
import furniture.DAO.IProductDAO;

@Repository
public class ProductDAOImpl extends BaseDAOImpl implements IProductDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "furniture.mapper.Product.";
	}

	@Override
	public Pager<ProductBean> selectParentCategoryPager(ProductBean bean,
			Pager<ProductBean> pager) {
		return support.selectPage(getNamespace() + "selectParentCategoryPager",bean, pager);
	}
	
}
