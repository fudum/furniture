package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.ICategoryDAO;

@Repository
public class CategoryDAOImpl extends BaseDAOImpl implements ICategoryDAO{

	@Override
	public String getNamespace() {
		return "furniture.mapper.Category.";
	}
	
}
