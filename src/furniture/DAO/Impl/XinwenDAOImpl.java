package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.IXinwenDAO;

@Repository
public class XinwenDAOImpl extends BaseDAOImpl implements IXinwenDAO{

	@Override
	public String getNamespace() {
		return "furniture.mapper.Xinwen.";
	}
	
}
