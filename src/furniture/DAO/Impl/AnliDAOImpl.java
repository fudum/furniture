package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.IAnliDAO;

@Repository
public class AnliDAOImpl extends BaseDAOImpl implements IAnliDAO{

	@Override
	public String getNamespace() {
		return "furniture.mapper.Anli.";
	}
	
}
