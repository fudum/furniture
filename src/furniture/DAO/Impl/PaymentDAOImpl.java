package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.IPaymentDAO;

/**
 * 
 * @author yfj
 * @cratedate 2015-01-26 20:19:58
 * 
 */
@Repository
public class PaymentDAOImpl extends BaseDAOImpl implements IPaymentDAO
{

	@Override
	public String getNamespace()
	{
		return "furniture.mapper.Payment.";
	}
}
