package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.IAdvertisementDAO;

@Repository
public class AdvertisementDAOImpl extends BaseDAOImpl implements IAdvertisementDAO{

	@Override
	public String getNamespace() {
		return "furniture.mapper.Advertisement.";
	}
	
}
