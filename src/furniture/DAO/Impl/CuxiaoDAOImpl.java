package furniture.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import furniture.DAO.ICuxiaoDAO;

/**
 * 
 * @author yfj
 * @cratedate 2015-01-26 20:19:58
 * 
 */
@Repository
public class CuxiaoDAOImpl extends BaseDAOImpl implements ICuxiaoDAO
{

	@Override
	public String getNamespace()
	{
		return "furniture.mapper.Cuxiao.";
	}
}
