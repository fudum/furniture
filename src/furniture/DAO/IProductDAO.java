package furniture.DAO;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;
import furniture.Bean.ProductBean;

public interface IProductDAO extends IBaseDAO {
	public Pager<ProductBean> selectParentCategoryPager(ProductBean bean,
			Pager<ProductBean> pager);
}
