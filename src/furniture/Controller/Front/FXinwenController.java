package furniture.Controller.Front;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.XinwenBean;
import furniture.Service.IXinwenService;

@Controller
@RequestMapping("/fnews")
public class FXinwenController extends BaseController{

	/**
	 * 新闻中心
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IXinwenService xinwenService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(XinwenBean bean, Pager<XinwenBean> pager){
		ModelAndView modelAndView = new ModelAndView("web/news");
		pager = xinwenService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(XinwenBean bean){
		ModelAndView modelAndView = new ModelAndView("web/newsinfo");
		bean = xinwenService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

}
