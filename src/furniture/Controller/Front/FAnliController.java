package furniture.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.AnliBean;
import furniture.Service.IAnliService;

@Controller
@RequestMapping("/fanli")
public class FAnliController  extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAnliService anliService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(AnliBean bean,Pager<AnliBean> pager){
		ModelAndView modelAndView = new ModelAndView("web/anli");
		pager = anliService.selectPage(bean, pager);
		return modelAndView;
	}

}
