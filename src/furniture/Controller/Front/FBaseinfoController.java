package furniture.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import furniture.Bean.BaseinfoBean;
import furniture.Service.IBaseinfoService;

@Controller
@RequestMapping("/fbaseinfo")
public class FBaseinfoController extends BaseController {

	/**
	 * 基础信息管理
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IBaseinfoService baseinfoService;
	
	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(String f){
		ModelAndView modelAndView = new ModelAndView("web/baseinfo");
		BaseinfoBean bean = new BaseinfoBean();
		switch (f) {
		case "1":
			bean.setFtype("人才招聘");
			break;
		case "2":
			bean.setFtype("关于我们");
			break;
		}
		modelAndView.addObject("f", f);
		bean = baseinfoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

}
