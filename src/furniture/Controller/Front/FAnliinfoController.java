package furniture.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.AnliBean;
import furniture.Bean.AnliinfoBean;
import furniture.Service.IAnliService;
import furniture.Service.IAnliinfoService;

@Controller
@RequestMapping("/fanliinfo")
public class FAnliinfoController  extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAnliService anliService;
	
	@Autowired
	IAnliinfoService anliinfoService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(AnliinfoBean bean, Pager<AnliinfoBean> pager){
		ModelAndView modelAndView = new ModelAndView("web/anliinfo");
		pager = anliinfoService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		
		AnliBean anliBean = new AnliBean();
		anliBean.setId(bean.getAnli_id());
		anliBean = anliService.selectOne(anliBean);
		modelAndView.addObject("anliBean", anliBean);
		
		return modelAndView;
	}

}
