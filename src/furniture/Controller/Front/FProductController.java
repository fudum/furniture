package furniture.Controller.Front;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.CategoryBean;
import furniture.Bean.ProductBean;
import furniture.Service.ICategoryService;
import furniture.Service.IProductService;

@Controller
@RequestMapping("fproduct")
public class FProductController extends BaseController{

	/**
	 * 前台产品控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICategoryService categoryService;
	
	@Autowired
	IProductService productService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(ProductBean bean,Pager<ProductBean> pager){
		ModelAndView modelAndView = new ModelAndView("web/product");
		String navId = "";
		
		CategoryBean cBean = new CategoryBean();
		cBean.setPlevel("1");
		List<CategoryBean> listOne = categoryService.selectList(cBean);
		modelAndView.addObject("listOne", listOne);
		
		cBean.setPlevel("2");
		List<CategoryBean> listTwo = categoryService.selectList(cBean);
		modelAndView.addObject("listTwo", listTwo);
		
		cBean.setPlevel(null);
		if (bean.getCategoryid() == null) {
			cBean.setName("全部");
			navId = "0";
		}
		else{
			cBean.setId(bean.getCategoryid());
			cBean  = categoryService.selectOne(cBean);
			navId = cBean.getParentid();
		}
		modelAndView.addObject("cBean", cBean);
		
		pager.setCountPerPage(10);//设置每页的数量为10
		if ("1".equals(bean.getCategoryname())) {
			pager = productService.selectParentCategoryPager(bean, pager);
			navId = bean.getCategoryid();
		}
		else{
			bean.setCategoryname(null);
			pager = productService.selectPage(bean, pager);
		}
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("navId", navId);
		
		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(ProductBean bean){
		ModelAndView modelAndView = new ModelAndView("web/productinfo");
		bean = productService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

}
