package furniture.Controller.Front;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.AdvertisementBean;
import furniture.Bean.LianxiBean;
import furniture.Bean.VideoBean;
import furniture.Bean.XinwenBean;
import furniture.Service.IAdvertisementService;
import furniture.Service.ILianxiService;
import furniture.Service.IVideoService;
import furniture.Service.IXinwenService;

@Controller
@RequestMapping("/front")
public class FrontController extends BaseController{

	/**
	 * 前台控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IXinwenService xinwenService;
	
	@Autowired
	IVideoService videoService;
	
	@Autowired
	IAdvertisementService advertisementService;
	
	@Autowired
	ILianxiService lianxiService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(){
		ModelAndView modelAndView = new ModelAndView("web/index");
		
		//新闻
		Pager<XinwenBean> pagerXinwen = new Pager<XinwenBean>();
		pagerXinwen.setCountPerPage(10);
		XinwenBean xinwenBean = new XinwenBean();
		xinwenBean.setRecommend("1");
		pagerXinwen = xinwenService.selectPage(xinwenBean, pagerXinwen);
		modelAndView.addObject("pagerXinwen", pagerXinwen);
		
		//视频
		VideoBean videoBean = new VideoBean();
		videoBean.setId("1");
		videoBean = videoService.selectOne(videoBean);
		modelAndView.addObject("videoBean", videoBean);
		
		//广告
		List<AdvertisementBean> advertiseList = advertisementService.selectList(null);
		modelAndView.addObject("advertiseList", advertiseList);
		if(advertiseList.size() > 1) {
			modelAndView.addObject("advertise_0", advertiseList.get(0));
		}
		if(advertiseList.size() > 2) {
			modelAndView.addObject("advertise_1", advertiseList.get(1));
		}
		//除去两个特殊的图片
		if(advertiseList.size() > 1) {
			advertiseList.remove(0);
		}
		if(advertiseList.size() > 1) {
			advertiseList.remove(0);
		}
		
		//联系地址
		LianxiBean lianxibean = new LianxiBean();
		lianxibean.setId("1");
		lianxibean = lianxiService.selectOne(lianxibean);
		modelAndView.addObject("lianxibean", lianxibean);
		
		
		return modelAndView;
	}

}
