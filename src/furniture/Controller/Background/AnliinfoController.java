package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.AnliinfoBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.IAnliinfoService;

@Controller
@RequestMapping("b_anliinfo")
public class AnliinfoController  extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAnliinfoService anliinfoService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(AnliinfoBean bean, Pager<AnliinfoBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/anli/index");
		pager = anliinfoService.selectPage(bean, pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/anli/anliinfo/add");
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(AnliinfoBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("A"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = anliinfoService.insert(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(AnliinfoBean bean){
		ModelAndView modelAndView = new ModelAndView("background/anli/anliinfo/edit");
		bean = anliinfoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public void update(AnliinfoBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (anliinfoService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public void Delete(AnliinfoBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = anliinfoService.delete(bean)>0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
