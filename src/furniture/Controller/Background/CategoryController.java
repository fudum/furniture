package furniture.Controller.Background;

import furniture.Common.Impl.CommonFunImpl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;













import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.CategoryBean;
import furniture.Service.ICategoryService;

@Controller
@RequestMapping("b_category")
public class CategoryController extends BaseController{

	/**
	 * 产品类别控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICategoryService categoryService;

	@Override
	public void setLogger() {
	}

	@RequestMapping("/index")
	public ModelAndView index(CategoryBean bean,Pager<CategoryBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/category/index");
		bean.setPlevel("1");
		pager = categoryService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/addOne")
	public ModelAndView addOne() {
		ModelAndView modelAndView = new ModelAndView("background/category/add");
		return modelAndView;
	}
	
	@RequestMapping("/insertOne")
	public void insertOne(CategoryBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("CO"));
		bean.setPlevel("1");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (categoryService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping("/editOne")
	public ModelAndView editOne(CategoryBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/category/edit");
		bean = categoryService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/updateOne")
	public void updateOne(CategoryBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (categoryService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void delete(CategoryBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = 1;
		ret = categoryService.delete(bean);
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("/addTwo")
	public ModelAndView addTwo() {
		ModelAndView modelAndView = new ModelAndView("background/category/addtwo");
		CategoryBean bean = new CategoryBean();
		bean.setPlevel("1");
		List<CategoryBean> listOne = categoryService.selectList(bean);
		modelAndView.addObject("listOne", listOne);
		return modelAndView;
	}
	
	@RequestMapping("/insertTwo")
	public void insertTwo(CategoryBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("CT"));
		bean.setParentid(bean.getParentname().substring(0, bean.getParentname().indexOf(';')));
		int i = bean.getParentname().indexOf(';');
		i++;
		bean.setParentname(bean.getParentname().substring(i));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		bean.setPlevel("2");
		String ret ="";
		JSONObject jb = new JSONObject();
		if (categoryService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/editTwo")
	public ModelAndView editTwo(CategoryBean bean){
		ModelAndView modelAndView = new ModelAndView("background/category/twoedit");
		bean = categoryService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		CategoryBean bean2 = new CategoryBean();
		bean2.setPlevel("1");
		List<CategoryBean> listOne = categoryService.selectList(bean2);
		modelAndView.addObject("listOne", listOne);
		return modelAndView;
	}
	
	@RequestMapping("/updateTwo")
	public void updateTwo(CategoryBean bean,String old_name, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setParentid(bean.getParentname().substring(0, bean.getParentname().indexOf(';')));
		int i = bean.getParentname().indexOf(';');
		i++;
		bean.setParentname(bean.getParentname().substring(i));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (categoryService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
