package furniture.Controller.Background;

import furniture.Bean.XinwenBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.IXinwenService;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("b_xinwen")
public class XinwenController extends BaseController{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Autowired
	IXinwenService xinwenService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(XinwenBean bean, Pager<XinwenBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/xinwen/index");
		pager = xinwenService.selectPage(bean, pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/xinwen/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(XinwenBean bean){
		ModelAndView modelAndView = new ModelAndView("background/xinwen/edit");
		bean = xinwenService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	
	@RequestMapping("/insert")
	public void insert(XinwenBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("X"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = xinwenService.insert(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(XinwenBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = xinwenService.update(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public void delete(XinwenBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = xinwenService.delete(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
