package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.CategoryBean;
import furniture.Bean.ProductBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ICategoryService;
import furniture.Service.IProductService;

@Controller
@RequestMapping("/b_product")
public class ProductController extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICategoryService categoryService;
	
	@Autowired
	IProductService  productService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(ProductBean bean,Pager<ProductBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/product/index");
		pager = productService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public  ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/product/add");
		CategoryBean bean = new CategoryBean();
		bean.setPlevel("1");
		List<CategoryBean> listOne = categoryService.selectList(bean);
		modelAndView.addObject("listOne", listOne);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public  ModelAndView edit(ProductBean pBean){
		ModelAndView modelAndView = new ModelAndView("background/product/edit");
		//产品bean
		pBean = productService.selectOne(pBean);
		modelAndView.addObject("bean", pBean);
		//判断类别的级别
		CategoryBean categoryBean = new CategoryBean();
		categoryBean.setId(pBean.getCategoryid());
		categoryBean = categoryService.selectOne(categoryBean);
		//如果是第一级别
		if ("1".equals(categoryBean.getPlevel())) {
			modelAndView.addObject("categoryid", categoryBean.getId());
		}
		else {
			//如果为第二级别
			modelAndView.addObject("categoryid", categoryBean.getParentid());
		}
		
		//一级类别
		CategoryBean bean = new CategoryBean();
		bean.setPlevel("1");
		List<CategoryBean> listOne = categoryService.selectList(bean);
		modelAndView.addObject("listOne", listOne);
		
		return modelAndView;
	}
	
	@RequestMapping("/getSecCategory")
	public void getSecCategory(CategoryBean bean,HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		List<CategoryBean> list = categoryService.selectList(bean);
		String ret = "";
		if (list.size() > 0) {
			JSONArray newArray = new JSONArray();
			for (int i = 0; i < list.size(); i++) {
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", list.get(i).getId());
				jsonObject.put("name", list.get(i).getName());
				newArray.add(jsonObject);
			}
			JSONObject jo = new JSONObject();
			jo.put("category", newArray);
			ret = jo.toString();
		}
		
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/insert")
	public void insert(ProductBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("P"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal =productService.insert(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(ProductBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal =productService.update(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	

	@RequestMapping("/delete")
	public void delete(ProductBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = productService.delete(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
