package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.LogisticsBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ILogisticsService;

@Controller
@RequestMapping("/b_logistics")
public class LogisticsController extends BaseController {

	/**
	 * 物流控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ILogisticsService logisticsService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(LogisticsBean bean, Pager<LogisticsBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/logistics/index");
		pager = logisticsService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/logistics/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(LogisticsBean bean){
		ModelAndView modelAndView = new ModelAndView("background/logistics/edit");
		bean = logisticsService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(LogisticsBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("L"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = logisticsService.insert(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(LogisticsBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = logisticsService.update(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void Delete(LogisticsBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = logisticsService.delete(bean) > 0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
