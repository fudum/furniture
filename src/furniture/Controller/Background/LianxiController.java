package furniture.Controller.Background;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import furniture.Bean.LianxiBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ILianxiService;

@Controller
@RequestMapping("b_lianxi")
public class LianxiController extends BaseController{

	/**
	 * 联系方式控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ILianxiService lianxiService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(LianxiBean bean){
		ModelAndView modelAndView= new ModelAndView("background/lianxi/index");
		bean.setId("1");
		bean = lianxiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public void update(LianxiBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = lianxiService.update(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
