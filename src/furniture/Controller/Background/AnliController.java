package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.AnliBean;
import furniture.Bean.AnliinfoBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.IAnliService;
import furniture.Service.IAnliinfoService;

@Controller
@RequestMapping("b_anli")
public class AnliController  extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAnliService anliService;
	
	@Autowired
	IAnliinfoService anliinfoService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(AnliBean bean, Pager<AnliBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/anli/index");
		pager = anliService.selectPage(bean, pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/anli/add");
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(AnliBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("A"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = anliService.insert(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(AnliBean bean){
		ModelAndView modelAndView = new ModelAndView("background/anli/edit");
		bean = anliService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		AnliinfoBean infoBean = new AnliinfoBean();
		infoBean.setAnli_id(bean.getId());
		List<AnliinfoBean> infoList = anliinfoService.selectList(infoBean);
		modelAndView.addObject("infoList", infoList);
		
		
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public void update(AnliBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (anliService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public void Delete(AnliBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = anliService.delete(bean)>0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
