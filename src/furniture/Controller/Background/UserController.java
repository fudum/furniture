package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.LogisticsBean;
import furniture.Bean.UserBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ILogisticsService;
import furniture.Service.IUserService;

@Controller
@RequestMapping("/b_user")
public class UserController extends BaseController{

	/**
	 * 用户控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	ILogisticsService logisticsService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(UserBean bean,Pager<UserBean> pager){
		bean.setUtype("2");//只显示普通用户
		ModelAndView modelAndView = new ModelAndView("background/user/index");
		pager = userService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/user/add");
		List<LogisticsBean> listLog = logisticsService.selectList(null);
		modelAndView.addObject("listLog", listLog);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(UserBean bean){
		ModelAndView modelAndView = new ModelAndView("background/user/edit");
		bean = userService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		List<LogisticsBean> listLog = logisticsService.selectList(null);
		modelAndView.addObject("listLog", listLog);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(UserBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("U"));
		bean.setUtype("2");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = userService.insert(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(UserBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = userService.update(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public void Delete(UserBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = userService.delete(bean) > 0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("changepwd")
	public ModelAndView changepwd(){
		ModelAndView modelAndView = new ModelAndView("background/pwd/index");
		return modelAndView;
	}
	
	/**
	 * 用户端
	 * 前面都加一个c
	 */
	@RequestMapping("c_index")
	public ModelAndView c_index(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("front/user/index");
		return modelAndView;
	}
	
	@RequestMapping("c_changepwd")
	public ModelAndView c_changepwd(){
		ModelAndView modelAndView = new ModelAndView("front/pwd/index");
		return modelAndView;
	}
	
	@RequestMapping("c_check")
	public void c_check(UserBean bean, HttpSession session,HttpServletResponse response){
		UserBean sessionBean = (UserBean)session.getAttribute("user_session");
		bean.setUserid(sessionBean.getId());
		bean.setUserid(sessionBean.getUserid());
		
		String ret = userService.selectOne(bean) != null ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/c_change")
	public void c_change(UserBean bean,HttpSession session,HttpServletResponse response) {
		UserBean sessionBean = (UserBean)session.getAttribute("user_session");
		bean.setId(sessionBean.getId());
		String ret = userService.update(bean) > 0 ? "1" : "0";
		if("1".equals(ret))
		{
			sessionBean.setUserid(bean.getUserid());
			session.setAttribute("user_session", bean);//把用户存入session中
		}
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
