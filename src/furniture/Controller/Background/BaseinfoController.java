package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.BaseinfoBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.IBaseinfoService;

@Controller
@RequestMapping("b_baseinfo")
public class BaseinfoController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setLogger() {
	}
	
	@Autowired
	IBaseinfoService baseinfoService;
	
	
	/**
	 * 基础信息
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(Pager<BaseinfoBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/baseinfo/index");
		pager = baseinfoService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	/**
	 * 关于我们
	 * @return
	 */
	public ModelAndView aboutus(){
		ModelAndView modelAndView = new ModelAndView("background/baseinfo/aboutus");
		return modelAndView;
	}
	
	/**
	 * 编辑
	 * @param bean
	 * @return
	 */
	@RequestMapping("/edit")
	public ModelAndView edit(BaseinfoBean bean){
		ModelAndView modelAndView = new ModelAndView("background/baseinfo/edit");
		bean = baseinfoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public void update(BaseinfoBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = baseinfoService.update(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}

}
