package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.CardBean;
import furniture.Bean.PaymentBean;
import furniture.Bean.UserBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ICardService;
import furniture.Service.IPaymentService;
import furniture.Service.IUserService;

@Controller
@RequestMapping("/b_payment")
public class PaymentController extends BaseController{

	/**
	 * 账号控制层
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IPaymentService paymentService;

	@Autowired
	ICardService cardService;
	
	@Autowired
	IUserService userService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(PaymentBean bean, Pager<PaymentBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/payment/index");
		pager = paymentService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		modelAndView.addObject("bean",bean);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/payment/add");
		
		List<CardBean> listCard = cardService.selectList(null);
		modelAndView.addObject("listCard", listCard);
		
		UserBean userBean = new UserBean();
		userBean.setUtype("2");
		List<UserBean> userList = userService.selectList(userBean);
		modelAndView.addObject("userList", userList);
		
		modelAndView.addObject("listBank", getBank());
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(PaymentBean bean){
		ModelAndView modelAndView = new ModelAndView("background/payment/edit");
		bean = paymentService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		List<CardBean> listCard = cardService.selectList(null);
		modelAndView.addObject("listCard", listCard);
		
		UserBean userBean = new UserBean();
		userBean.setUtype("2");
		List<UserBean> userList = userService.selectList(userBean);
		modelAndView.addObject("userList", userList);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(PaymentBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("P"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getUsername().split(";");
		bean.setUsername(arr[1]);
		bean.setUserid(arr[0]);
		if ("".equals(bean.getShishoumoney())) {
			bean.setShishoumoney(null);
		}
		if ("".equals(bean.getHuokuanmoney())) {
			bean.setHuokuanmoney(null);
		}
		if ("".equals(bean.getShouxufei())) {
			bean.setShouxufei(null);
		}
		String ret = paymentService.insert(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(PaymentBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getUsername().split(";");
		bean.setUsername(arr[1]);
		bean.setUserid(arr[0]);
		String ret = paymentService.update(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<String> getBank(){
		List<String> list = new ArrayList<String>();
		list.add("95566;中国银行");
		list.add("95599;中国农业银行");
		list.add("95533;中国建设银行");
		list.add("95588;中国工商银行");
		list.add("95559;交通银行");
		list.add("95555;招商银行");
		list.add("95580;中国邮政储蓄银行");
		return list;
	}
	
	@RequestMapping("/delete")
	public void Delete(PaymentBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = paymentService.delete(bean) > 0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 用户
	 */
	@RequestMapping("/c_index")
	public ModelAndView c_index(String year,String month,PaymentBean bean,HttpSession session){
		ModelAndView modelAndView = new ModelAndView("front/payment/index");
		UserBean userBean = (UserBean)(session.getAttribute("user_session"));
		String tj = "";
		if (year == null || "".equals(year)) {
			Calendar calendar = Calendar.getInstance();
			year = Integer.toString(calendar.get(Calendar.YEAR)) + "年";
		}
		else{
			tj = year.substring(0,4);
		}
		if (!(month == null || "".endsWith(month))) {
			tj +="-";
			tj +=month.substring(0,2);
		}
		modelAndView.addObject("year", year);
		modelAndView.addObject("month", month);
		
		bean.setDengjitime(tj);
		bean.setUserid(userBean.getId());
		List<PaymentBean> list = paymentService.selectList(bean);
		modelAndView.addObject("list", list);
		double count = 0;
		for (int i = 0; i < list.size(); i++) {
			try {
				count += Double.parseDouble(list.get(i).getShishoumoney());
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		modelAndView.addObject("count", count);
		return modelAndView;
	}
	
}
