package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import framework.base.controller.BaseController;
import furniture.Bean.NoticeBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.INoticeService;

@Controller
@RequestMapping("/b_notice")
public class NoticeController extends BaseController{

	/**
	 * 消息通知控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	INoticeService noticeService;

	@Override
	public void setLogger() {
		// TODO Auto-generated method stub
	}
	
	@RequestMapping("/index")
	public ModelAndView index(NoticeBean bean, Pager<NoticeBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/notice/index");
		pager = noticeService.selectPage(bean, pager);
		modelAndView.addObject(pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/notice/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(NoticeBean bean){
		ModelAndView modelAndView = new ModelAndView("background/notice/edit");
		bean = noticeService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(NoticeBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("C"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = noticeService.insert(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(NoticeBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret = noticeService.update(bean) > 0 ? "1" : "0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void Delete(NoticeBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = noticeService.delete(bean)>0 ? 1 : 0;
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
