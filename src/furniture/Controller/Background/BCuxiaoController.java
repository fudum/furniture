package furniture.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import furniture.Bean.CuxiaoBean;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Service.ICuxiaoService;

@Controller
@RequestMapping("b_cuxiao")
public class BCuxiaoController extends BaseController {

	/**
	 * 序列化编号
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICuxiaoService cuxiaoService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(CuxiaoBean bean){
		ModelAndView modelAndView = new ModelAndView("background/cuxiao/index");
		bean.setId("1");
		bean = cuxiaoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/xinwen/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(CuxiaoBean bean){
		ModelAndView modelAndView = new ModelAndView("background/xinwen/edit");
		bean = cuxiaoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	
	@RequestMapping("/insert")
	public void insert(CuxiaoBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("X"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = cuxiaoService.insert(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(CuxiaoBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal = cuxiaoService.update(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public void delete(CuxiaoBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		int ret = cuxiaoService.delete(bean);
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/c_index")
	public ModelAndView cindex(CuxiaoBean bean){
		ModelAndView modelAndView = new ModelAndView("front/cuxiao/index");
		bean.setId("1");
		bean = cuxiaoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	

}
