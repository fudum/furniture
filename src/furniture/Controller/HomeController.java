package furniture.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import furniture.Bean.CuxiaoBean;
import furniture.Bean.NoticeBean;
import furniture.Bean.UserBean;
import furniture.Service.ICuxiaoService;
import furniture.Service.INoticeService;
import furniture.Service.IUserService;

@Controller
@RequestMapping("/home")
public class HomeController extends BaseController{

	/**
	 * 登入模块
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	INoticeService noticeServie;
	
	@Autowired
	ICuxiaoService cuxiaoService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(HttpSession session){
		if (session.getAttribute("user_session") == null) {
			ModelAndView modelAndView = new ModelAndView("background/home/login01");
			List<NoticeBean> list = noticeServie.selectList(null);
			modelAndView.addObject("list",list);
			
			return modelAndView;
		}
		else{
			ModelAndView modelAndView = new ModelAndView("background/home/index");
			
			return modelAndView;
		}
	}
	
	@RequestMapping("/login")
	public void login(UserBean bean, HttpSession session, HttpServletResponse response) {
		String retVal = "0";
		if (bean.getUserid() == null || bean.getPassword() == null || bean.getUtype() == null){
			//判断用户是否为null
		}
		else if(bean.getPassword().trim() == "" || bean.getUserid().trim() =="" || bean.getUtype().trim() == ""){
			//判断是否为""
		}
		else{
			bean = userService.selectOne(bean);
			if (bean != null) {//验证成功
				//把基本信息存入session中
				bean.setPassword(null);
				session.setAttribute("user_session", bean);//把用户存入session中
				retVal = "1";
			} else {
				//验证失败
			}
		}
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();//打印异常
		}
	}
	
	//退出
	@RequestMapping("/loginout")
	public  ModelAndView loginOut(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("background/home/login01");
		session.removeAttribute("user_session");
		List<NoticeBean> list = noticeServie.selectList(null);
		modelAndView.addObject("list",list);
		return modelAndView;
	}
	
	@RequestMapping("/top")
	public ModelAndView top() {
		ModelAndView modelAndView = new ModelAndView("background/home/top");
		List<NoticeBean> list = noticeServie.selectList(null);
		modelAndView.addObject("listtop",list);
		return modelAndView;
	}
	
	@RequestMapping("/left")
	public ModelAndView left() {
		ModelAndView modelAndView = new ModelAndView("background/home/left");
		return modelAndView;
	}
	
	@RequestMapping("/right")
	public ModelAndView right() {
		ModelAndView modelAndView = new ModelAndView("background/home/right");
		CuxiaoBean cxBean = new CuxiaoBean();
		cxBean.setId("1");
		cxBean = cuxiaoService.selectOne(cxBean);
		modelAndView.addObject("cxbean", cxBean);
		return modelAndView;
	}

}
