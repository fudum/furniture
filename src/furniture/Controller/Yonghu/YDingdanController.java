package furniture.Controller.Yonghu;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import furniture.Bean.DingdanBean;
import furniture.Bean.UserBean;
import furniture.Service.IDingdanService;
import furniture.Service.IUserService;

@Controller
@RequestMapping("/y_dingdan")
public class YDingdanController extends BaseController{

	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IDingdanService dingdanService;
	
	@Autowired
	IUserService userService;

	@Override
	public void setLogger() {
		// TODO Auto-generated method stub
	}
	
	@RequestMapping("/getmsg")
	public ModelAndView getMsg(String dingdanId,String userid,HttpSession session){
		ModelAndView modelAndView = new ModelAndView("front/dingdan/getmsg");
		UserBean user = new UserBean();
		user.setId(userid);
		user = userService.selectOne(user);
		modelAndView.addObject("phone", user.getPhone());
		modelAndView.addObject("dingdanid",dingdanId);
		return modelAndView;
	}

	@RequestMapping("/sendmsg")
	public void sendMsg(String dingdanid,String phone,HttpServletResponse response) throws UnsupportedEncodingException, IOException{
		response.setContentType("text/html");
		//String result = "-2";
		String result = "0";
		//获取订单状态
		DingdanBean bean = new DingdanBean();
		bean.setId(dingdanid);
		bean = dingdanService.selectOne(bean);
		
		String smsText = "金喜公司提示：";
		if (bean != null) {
			smsText += bean.getUsername();
			smsText +="您好，您的订单";
			smsText += bean.getYezhu();
			smsText += "的货品于";
			smsText += bean.getFahuotime();
			smsText += "已发出，物流及单号为";
			smsText += bean.getHuohao();
			smsText += "，请注意查收。顺祝生意兴隆。服务电话：027-84631533。或请登录www.whjinxi.com网站查询。";
		}

		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://gbk.sms.webchinese.cn"); 
		post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=gbk");//在头文件中设置转码
		NameValuePair[] data ={ new NameValuePair("Uid", "fudum"),new NameValuePair("Key", "77ebd6cda9e5b172af29"),new NameValuePair("smsMob",phone),new NameValuePair("smsText",smsText)};
		post.setRequestBody(data);

		try {
			client.executeMethod(post);
			Header[] headers = post.getResponseHeaders();
			int statusCode = post.getStatusCode();
			System.out.println("statusCode:"+statusCode);
			for(Header h : headers)
			{
			System.out.println(h.toString());
			}
			
			result = new String(post.getResponseBodyAsString().getBytes("gbk")); 
			post.releaseConnection();
		} catch (HttpException e) {
			e.printStackTrace();
			result = "-2";
		} catch (IOException e) {
			e.printStackTrace();
			result = "-2";
		}
		
		//返回ajax调用
		try {
			PrintWriter out = response.getWriter();
			out.print(result);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
