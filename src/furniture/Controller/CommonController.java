package furniture.Controller;

import java.io.File;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import framework.base.controller.BaseController;
import furniture.Common.Impl.CommonConstant;
import furniture.Common.Impl.CommonFunImpl;
import furniture.Common.Impl.OSinfo;

@Controller
@RequestMapping("/common")
public class CommonController extends BaseController{

	/**
	 * 通用控制层
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/fileupload")
	public void fileUpload(MultipartFile uploadFile,String wjjname,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(uploadFile != null){
			if (!uploadFile.isEmpty()) {
				String path = getWjjName(wjjname);
				String tmp = uploadFile.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	uploadFile.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path", wjjname + "/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/fileuploadtwo")
	public void fileUploadTwo(MultipartFile uploadFileTwo,String wjjname,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(uploadFileTwo != null){
			if (!uploadFileTwo.isEmpty()) {
				String path = getWjjName(wjjname);
				String tmp = uploadFileTwo.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	uploadFileTwo.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path", wjjname + "/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 获取文件的路径
	 * @param wjjName
	 * @return
	 */
	private String getWjjName(String wjjName) {
		String retVal = "";
		switch (wjjName) {
		case "dingdan"://试卷路径
			retVal = OSinfo.isWindows() ? CommonConstant.w_dingdanPath :CommonConstant.dingdanPath;
			break;
		case "advertisement"://首页图片
			retVal = OSinfo.isWindows() ? CommonConstant.w_advertisementPath :CommonConstant.advertisementPath;
			break;
		case "product"://产品管理
			retVal = OSinfo.isWindows() ? CommonConstant.w_productPath:CommonConstant.productPath;
			break;
		case "user"://用户管理
			retVal = OSinfo.isWindows() ? CommonConstant.w_userPath:CommonConstant.userPath;
			break;
		case "anli"://用户管理
			retVal = OSinfo.isWindows() ? CommonConstant.w_anliPath:CommonConstant.anliPath;
			break;
		case "anliinfo"://用户管理
			retVal = OSinfo.isWindows() ? CommonConstant.w_anliinfoPath:CommonConstant.anliinfoPath;
			break;
		}
		
		return retVal;
	}

}
