package furniture.Common.Impl;

public class CommonConstant {
	
	/******************前后缀**********************************/
	public static String suf = ".html";
	
	public static String pre = "/furniture";
	
	/***********************linux版本路径***********************/
	public static String uploadPath = "/webupload/furniture";//上传总目录
	
	public static String dingdanPath = uploadPath + "/dingdan";//订单目录
	
	public static String xinwenPath = uploadPath + "/xinwen";//新闻目录
	
	public static String attachedPath = uploadPath + "/attached";//附件目录
	
	public static String advertisementPath = uploadPath + "/advertisement";//新闻目录
	
	public static String productPath = uploadPath + "/product";//产品目录
	
	public static String userPath = uploadPath + "/user";//用户目录
	
	public static String anliPath = uploadPath + "/anli";//经典案例
	
	public static String anliinfoPath = uploadPath + "/anliinfo";//经典案例
	
	/*********************windows版本路径***********************/
	public static String w_uploadPath = "d:\\webupload\\furniture";//上传总目录
	
	public static String w_dingdanPath = w_uploadPath + "\\dingdan";//订单目录
	
	public static String w_xinwenPath = w_uploadPath + "\\xinwen";//新闻目录
	
	public static String w_attachedPath = w_uploadPath + "\\attached";//附件目录
	
	public static String w_advertisementPath = w_uploadPath + "\\advertisement";//新闻目录　
	
	public static String w_productPath = w_uploadPath + "\\product";//产品目录
	
	public static String w_userPath = w_uploadPath + "\\user";//用户目录
	
	public static String w_anliPath = w_uploadPath + "\\anli";//经典案例
	
	public static String w_anliinfoPath = w_uploadPath + "\\anliinfo";//经典案例
	
}
