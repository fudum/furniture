package furniture.Bean;

import framework.base.bean.BaseBean;


public class BaseinfoBean  extends BaseBean {
	/**
	 * 关于我们、人才招聘、联系我们
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String ftype;// 类别
	private String content;// 内容
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFtype() {
		return ftype;
	}
	public void setFtype(String ftype) {
		this.ftype = ftype;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}