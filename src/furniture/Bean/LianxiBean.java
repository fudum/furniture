package furniture.Bean;

import framework.base.bean.BaseBean;


@SuppressWarnings("serial")
public class LianxiBean extends BaseBean  {
	private String id;// 主键
	private String address;// 地址
	private String scphone;// 生产中心
	private String xfphone;// 消费中心
	private String zhanxianphone;// 专线
	private String fax;// 传真
	private String longitude;// 经度
	private String latitude;// 纬度
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getScphone() {
		return scphone;
	}
	public void setScphone(String scphone) {
		this.scphone = scphone;
	}
	public String getXfphone() {
		return xfphone;
	}
	public void setXfphone(String xfphone) {
		this.xfphone = xfphone;
	}
	public String getZhanxianphone() {
		return zhanxianphone;
	}
	public void setZhanxianphone(String zhanxianphone) {
		this.zhanxianphone = zhanxianphone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}