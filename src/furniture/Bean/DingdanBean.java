package furniture.Bean;

import framework.base.bean.BaseBean;

public class DingdanBean extends BaseBean {
	/**
	 * 订单bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String ddnum;// 订单号
	private String userid;// 用户编号
	private String username;// 经销商
	private String ddtime;// 订单时间
	private String yujifahuotime;// 预计发货时间
	private String fahuotime;// 发货时间
	private String huohao;//  货号
	private String price;// 价格
	private String ddstatus;// 订单状态
	private String info;// 订单详情
	private String inserttime;// 插入时间
	private String yezhu;// 业主
	private String excelinfo;// excel版订单详情
	private String exceltwo;//第二个excel
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDdnum() {
		return ddnum;
	}
	public void setDdnum(String ddnum) {
		this.ddnum = ddnum;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDdtime() {
		return ddtime;
	}
	public void setDdtime(String ddtime) {
		this.ddtime = ddtime;
	}
	public String getYujifahuotime() {
		return yujifahuotime;
	}
	public void setYujifahuotime(String yujifahuotime) {
		this.yujifahuotime = yujifahuotime;
	}
	public String getFahuotime() {
		return fahuotime;
	}
	public void setFahuotime(String fahuotime) {
		this.fahuotime = fahuotime;
	}
	public String getHuohao() {
		return huohao;
	}
	public void setHuohao(String huohao) {
		this.huohao = huohao;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDdstatus() {
		return ddstatus;
	}
	public void setDdstatus(String ddstatus) {
		this.ddstatus = ddstatus;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getYezhu() {
		return yezhu;
	}
	public void setYezhu(String yezhu) {
		this.yezhu = yezhu;
	}
	public String getExcelinfo() {
		return excelinfo;
	}
	public void setExcelinfo(String excelinfo) {
		this.excelinfo = excelinfo;
	}
	public String getExceltwo() {
		return exceltwo;
	}
	public void setExceltwo(String exceltwo) {
		this.exceltwo = exceltwo;
	}

}