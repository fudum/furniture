package furniture.Bean;

import framework.base.bean.BaseBean;

public class LogisticsBean extends BaseBean {
	/**
	 * 物流公司bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 名称
	private String description;// 描述
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}