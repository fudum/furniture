package furniture.Bean;

import framework.base.bean.BaseBean;

@SuppressWarnings("serial")
public class VideoBean extends BaseBean {
	private String id;// 主键
	private String path;// 视频路径
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}