package furniture.Bean;

import framework.base.bean.BaseBean;

public class NoticeBean extends BaseBean {
	/**
	 * 消息通知bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String content;// 内容
	private String norder;// 显示顺序
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getNorder() {
		return norder;
	}
	public void setNorder(String norder) {
		this.norder = norder;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}