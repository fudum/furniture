package furniture.Bean;


import framework.base.bean.BaseBean;

@SuppressWarnings("serial")
public class AnliinfoBean extends BaseBean {
	private String id;// 
	private String name;// 
	private String picpath;// 
	private String inserttime;// 
	private String anli_id;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getAnli_id() {
		return anli_id;
	}
	public void setAnli_id(String anli_id) {
		this.anli_id = anli_id;
	}
}