package furniture.Bean;

import framework.base.bean.BaseBean;


public class CategoryBean extends BaseBean {
	/**
	 * 产品类别列表
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 名称
	private String plevel;// 显示级别，1,2
	private String shunxu;// 显示顺序
	private String parentid;// 父节点id
	private String parentname;// 父节点名称
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPlevel() {
		return plevel;
	}
	public void setPlevel(String plevel) {
		this.plevel = plevel;
	}
	public String getShunxu() {
		return shunxu;
	}
	public void setShunxu(String shunxu) {
		this.shunxu = shunxu;
	}
	public String getParentid() {
		return parentid;
	}
	public void setParentid(String parentid) {
		this.parentid = parentid;
	}
	public String getParentname() {
		return parentname;
	}
	public void setParentname(String parentname) {
		this.parentname = parentname;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}