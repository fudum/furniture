package furniture.Bean;

import framework.base.bean.BaseBean;

@SuppressWarnings("serial")
public class CuxiaoBean extends BaseBean {
	private String id;// 
	private String content;// 促销内容
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}