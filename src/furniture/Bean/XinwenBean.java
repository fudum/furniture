package furniture.Bean;

import framework.base.bean.BaseBean;

public class XinwenBean extends BaseBean {
	/**
	 * 新闻bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String title;// 新闻标题
	private String category;// 新闻类别
	private String content;// 新闻内容
	private String auther;// 新闻作者
	private String ip;// 发布的ip
	private String inserttime;// 插入时间
	private String recommend;// 是否推荐到首页
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuther() {
		return auther;
	}
	public void setAuther(String auther) {
		this.auther = auther;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getRecommend() {
		return recommend;
	}
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}

}