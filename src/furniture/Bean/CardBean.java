package furniture.Bean;

import framework.base.bean.BaseBean;

public class CardBean extends BaseBean {
	/**
	 * 银行bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;//主键 
	private String bankname;//银行名称 
	private String cardid;//银行卡号 
	private String cardname;//卡号名称 
	private String picpath;//照片路径 
	private String inserttime;//插入时间 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getCardname() {
		return cardname;
	}
	public void setCardname(String cardname) {
		this.cardname = cardname;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}