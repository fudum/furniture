package furniture.Bean;

import framework.base.bean.BaseBean;

public class PaymentBean extends BaseBean {
	/**
	 * 付款bean
	 */
	private static final long serialVersionUID = 1;
	private String id;// 主键
	private String account;// 账号
	private String dengjitime;// 登记时间
	private String daozhangtime;// 到账时间
	private String shishoumoney;// 实收金额
	private String huokuanmoney;// 货款金额
	private String shouxufei;// 手续费
	private String beizhu;// 备注
	private String inserttime;// 插入时间
	private String userid;// 业主ID
	private String username;// 业主名
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getDengjitime() {
		return dengjitime;
	}
	public void setDengjitime(String dengjitime) {
		this.dengjitime = dengjitime;
	}
	public String getDaozhangtime() {
		return daozhangtime;
	}
	public void setDaozhangtime(String daozhangtime) {
		this.daozhangtime = daozhangtime;
	}
	public String getShishoumoney() {
		return shishoumoney;
	}
	public void setShishoumoney(String shishoumoney) {
		this.shishoumoney = shishoumoney;
	}
	public String getHuokuanmoney() {
		return huokuanmoney;
	}
	public void setHuokuanmoney(String huokuanmoney) {
		this.huokuanmoney = huokuanmoney;
	}
	public String getShouxufei() {
		return shouxufei;
	}
	public void setShouxufei(String shouxufei) {
		this.shouxufei = shouxufei;
	}
	public String getBeizhu() {
		return beizhu;
	}
	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

}