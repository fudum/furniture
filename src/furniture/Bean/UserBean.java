package furniture.Bean;

import framework.base.bean.BaseBean;

public class UserBean extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String userid;// 用户ID
	private String name;// 用户名
	private String password;// 密码
	private String sex;// 性别
	private String phone;// 电话号码
	private String phonesec;// 对单电话
	private String qq;// qq号码
	private String other;// 备注
	private String addressto;// 货到地区
	private String address;// 所在地
	private String wuliu;// 要求物流
	private String dianmianname;// 店面名称
	private String utype;// 用户类别:1管理员，2普通用户
	private String inserttime;// 插入时间
	private String excelinfo;//用户详情
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhonesec() {
		return phonesec;
	}
	public void setPhonesec(String phonesec) {
		this.phonesec = phonesec;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public String getAddressto() {
		return addressto;
	}
	public void setAddressto(String addressto) {
		this.addressto = addressto;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWuliu() {
		return wuliu;
	}
	public void setWuliu(String wuliu) {
		this.wuliu = wuliu;
	}
	public String getDianmianname() {
		return dianmianname;
	}
	public void setDianmianname(String dianmianname) {
		this.dianmianname = dianmianname;
	}
	public String getUtype() {
		return utype;
	}
	public void setUtype(String utype) {
		this.utype = utype;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getExcelinfo() {
		return excelinfo;
	}
	public void setExcelinfo(String excelinfo) {
		this.excelinfo = excelinfo;
	}
}