package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.CuxiaoBean;

public interface ICuxiaoService extends IBaseService{

	public CuxiaoBean selectOne(CuxiaoBean bean);
	
	public List<CuxiaoBean> selectList(CuxiaoBean bean);
	
	public int update(CuxiaoBean bean);
	
	public int insert(CuxiaoBean bean);
	
	public int delete(CuxiaoBean bean);
	
	public Pager<CuxiaoBean> selectPage(CuxiaoBean bean,Pager<CuxiaoBean> pager);
	
}
