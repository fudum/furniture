package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.PaymentBean;

public interface IPaymentService extends IBaseService{

	public PaymentBean selectOne(PaymentBean bean);
	
	public List<PaymentBean> selectList(PaymentBean bean);
	
	public int update(PaymentBean bean);
	
	public int insert(PaymentBean bean);
	
	public int delete(PaymentBean bean);
	
	public Pager<PaymentBean> selectPage(PaymentBean bean,Pager<PaymentBean> pager);
	
}
