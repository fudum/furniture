package furniture.Service;

import java.util.List;

import furniture.Bean.VideoBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IVideoService extends IBaseService{

	public VideoBean selectOne(VideoBean bean);
	
	public List<VideoBean> selectList(VideoBean bean);
	
	public int update(VideoBean bean);
	
	public int insert(VideoBean bean);
	
	public int delete(VideoBean bean);
	
	public Pager<VideoBean> selectPage(VideoBean bean,Pager<VideoBean> pager);
	
}
