package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.LogisticsBean;

public interface ILogisticsService extends IBaseService{

	public LogisticsBean selectOne(LogisticsBean bean);
	
	public List<LogisticsBean> selectList(LogisticsBean bean);
	
	public int update(LogisticsBean bean);
	
	public int insert(LogisticsBean bean);
	
	public int delete(LogisticsBean bean);
	
	public Pager<LogisticsBean> selectPage(LogisticsBean bean,Pager<LogisticsBean> pager);
	
}
