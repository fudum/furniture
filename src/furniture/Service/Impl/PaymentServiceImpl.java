package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.PaymentBean;
import furniture.DAO.IPaymentDAO;
import furniture.Service.IPaymentService;

@Service
public class PaymentServiceImpl implements IPaymentService{

	@Autowired
	IPaymentDAO dao;
	
	@Override
	public PaymentBean selectOne(PaymentBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<PaymentBean> selectList(PaymentBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(PaymentBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(PaymentBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(PaymentBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<PaymentBean> selectPage(PaymentBean bean, Pager<PaymentBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
