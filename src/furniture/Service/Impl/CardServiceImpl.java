package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.CardBean;
import furniture.DAO.ICardDAO;
import furniture.Service.ICardService;

@Service
public class CardServiceImpl implements ICardService{

	@Autowired
	ICardDAO dao;
	
	@Override
	public CardBean selectOne(CardBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<CardBean> selectList(CardBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(CardBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(CardBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(CardBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<CardBean> selectPage(CardBean bean, Pager<CardBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
