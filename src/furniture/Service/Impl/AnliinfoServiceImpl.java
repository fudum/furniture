package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.AnliinfoBean;
import furniture.DAO.IAnliinfoDAO;
import furniture.Service.IAnliinfoService;

@Service
public class AnliinfoServiceImpl implements IAnliinfoService{
	@Autowired
	IAnliinfoDAO dao;
	
	@Override
	public AnliinfoBean selectOne(AnliinfoBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<AnliinfoBean> selectList(AnliinfoBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(AnliinfoBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(AnliinfoBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(AnliinfoBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<AnliinfoBean> selectPage(AnliinfoBean bean, Pager<AnliinfoBean> pager) {
		return dao.selectPage(bean, pager);
	}

}
