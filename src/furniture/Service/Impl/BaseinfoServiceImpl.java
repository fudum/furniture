package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.BaseinfoBean;
import furniture.DAO.IBaseinfoDAO;
import furniture.Service.IBaseinfoService;

@Service
public class BaseinfoServiceImpl implements IBaseinfoService{

	@Autowired
	IBaseinfoDAO dao;
	
	@Override
	public BaseinfoBean selectOne(BaseinfoBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BaseinfoBean> selectList(BaseinfoBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(BaseinfoBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(BaseinfoBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(BaseinfoBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BaseinfoBean> selectPage(BaseinfoBean bean, Pager<BaseinfoBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
