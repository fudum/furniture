package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.LogisticsBean;
import furniture.DAO.ILogisticsDAO;
import furniture.Service.ILogisticsService;

@Service
public class LogisticsServiceImpl implements ILogisticsService{

	@Autowired
	ILogisticsDAO dao;
	
	@Override
	public LogisticsBean selectOne(LogisticsBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<LogisticsBean> selectList(LogisticsBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(LogisticsBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(LogisticsBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(LogisticsBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<LogisticsBean> selectPage(LogisticsBean bean, Pager<LogisticsBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
