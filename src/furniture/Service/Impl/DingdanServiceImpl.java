package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.DingdanBean;
import furniture.DAO.IDingdanDAO;
import furniture.Service.IDingdanService;

@Service
public class DingdanServiceImpl implements IDingdanService{

	@Autowired
	IDingdanDAO dao;
	
	@Override
	public DingdanBean selectOne(DingdanBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<DingdanBean> selectList(DingdanBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(DingdanBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(DingdanBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(DingdanBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<DingdanBean> selectPage(DingdanBean bean, Pager<DingdanBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
