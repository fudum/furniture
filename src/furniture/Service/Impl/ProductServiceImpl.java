package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.ProductBean;
import furniture.DAO.IProductDAO;
import furniture.Service.IProductService;

@Service
public class ProductServiceImpl implements IProductService{
	@Autowired
	IProductDAO dao;
	
	@Override
	public ProductBean selectOne(ProductBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<ProductBean> selectList(ProductBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(ProductBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(ProductBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(ProductBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<ProductBean> selectPage(ProductBean bean, Pager<ProductBean> pager) {
		return dao.selectPage(bean, pager);
	}

	@Override
	public Pager<ProductBean> selectParentCategoryPager(ProductBean bean,
			Pager<ProductBean> pager) {
		return dao.selectParentCategoryPager(bean,pager);
	}
	

}
