package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.LianxiBean;
import furniture.DAO.ILianxiDAO;
import furniture.Service.ILianxiService;

@Service
public class LianxiServiceImpl implements ILianxiService{
	@Autowired
	ILianxiDAO dao;
	
	@Override
	public LianxiBean selectOne(LianxiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<LianxiBean> selectList(LianxiBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(LianxiBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(LianxiBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(LianxiBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<LianxiBean> selectPage(LianxiBean bean, Pager<LianxiBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
