package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.AnliBean;
import furniture.DAO.IAnliDAO;
import furniture.Service.IAnliService;

@Service
public class AnliServiceImpl implements IAnliService{
	@Autowired
	IAnliDAO dao;
	
	@Override
	public AnliBean selectOne(AnliBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<AnliBean> selectList(AnliBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(AnliBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(AnliBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(AnliBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<AnliBean> selectPage(AnliBean bean, Pager<AnliBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
