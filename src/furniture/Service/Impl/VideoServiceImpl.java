package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.VideoBean;
import furniture.DAO.IVideoDAO;
import furniture.Service.IVideoService;

@Service
public class VideoServiceImpl implements IVideoService{
	@Autowired
	IVideoDAO dao;
	
	@Override
	public VideoBean selectOne(VideoBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<VideoBean> selectList(VideoBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(VideoBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(VideoBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(VideoBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<VideoBean> selectPage(VideoBean bean, Pager<VideoBean> pager) {
		return dao.selectPage(bean, pager);
	}

}
