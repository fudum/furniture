package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.CategoryBean;
import furniture.DAO.ICategoryDAO;
import furniture.Service.ICategoryService;

@Service
public class CategoryServiceImpl implements ICategoryService{
	@Autowired
	ICategoryDAO dao;
	
	@Override
	public CategoryBean selectOne(CategoryBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<CategoryBean> selectList(CategoryBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(CategoryBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(CategoryBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(CategoryBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<CategoryBean> selectPage(CategoryBean bean, Pager<CategoryBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
