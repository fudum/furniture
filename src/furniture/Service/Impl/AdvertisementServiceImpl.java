package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.AdvertisementBean;
import furniture.DAO.IAdvertisementDAO;
import furniture.Service.IAdvertisementService;

@Service
public class AdvertisementServiceImpl implements IAdvertisementService{
	@Autowired
	IAdvertisementDAO dao;
	
	@Override
	public AdvertisementBean selectOne(AdvertisementBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<AdvertisementBean> selectList(AdvertisementBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(AdvertisementBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(AdvertisementBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(AdvertisementBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<AdvertisementBean> selectPage(AdvertisementBean bean, Pager<AdvertisementBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
