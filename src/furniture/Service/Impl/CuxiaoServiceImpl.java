package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.CuxiaoBean;
import furniture.DAO.ICuxiaoDAO;
import furniture.Service.ICuxiaoService;

@Service
public class CuxiaoServiceImpl implements ICuxiaoService{

	@Autowired
	ICuxiaoDAO dao;
	
	@Override
	public CuxiaoBean selectOne(CuxiaoBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<CuxiaoBean> selectList(CuxiaoBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(CuxiaoBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(CuxiaoBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(CuxiaoBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<CuxiaoBean> selectPage(CuxiaoBean bean, Pager<CuxiaoBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
