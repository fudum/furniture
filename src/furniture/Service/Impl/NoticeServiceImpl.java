package furniture.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import furniture.Bean.NoticeBean;
import furniture.DAO.INoticeDAO;
import furniture.Service.INoticeService;

@Service
public class NoticeServiceImpl implements INoticeService{

	@Autowired
	INoticeDAO dao;
	
	@Override
	public NoticeBean selectOne(NoticeBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<NoticeBean> selectList(NoticeBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(NoticeBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(NoticeBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(NoticeBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<NoticeBean> selectPage(NoticeBean bean, Pager<NoticeBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	

}
