package furniture.Service;

import java.util.List;

import furniture.Bean.ProductBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IProductService extends IBaseService{

	public ProductBean selectOne(ProductBean bean);
	
	public List<ProductBean> selectList(ProductBean bean);
	
	public int update(ProductBean bean);
	
	public int insert(ProductBean bean);
	
	public int delete(ProductBean bean);
	
	public Pager<ProductBean> selectPage(ProductBean bean,Pager<ProductBean> pager);
	
	public Pager<ProductBean> selectParentCategoryPager(ProductBean bean,Pager<ProductBean>pager);
	
}
