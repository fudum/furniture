package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.BaseinfoBean;

public interface IBaseinfoService extends IBaseService{

	public BaseinfoBean selectOne(BaseinfoBean bean);
	
	public List<BaseinfoBean> selectList(BaseinfoBean bean);
	
	public int update(BaseinfoBean bean);
	
	public int insert(BaseinfoBean bean);
	
	public int delete(BaseinfoBean bean);
	
	public Pager<BaseinfoBean> selectPage(BaseinfoBean bean,Pager<BaseinfoBean> pager);
	
}
