package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.CardBean;

public interface ICardService extends IBaseService{

	public CardBean selectOne(CardBean bean);
	
	public List<CardBean> selectList(CardBean bean);
	
	public int update(CardBean bean);
	
	public int insert(CardBean bean);
	
	public int delete(CardBean bean);
	
	public Pager<CardBean> selectPage(CardBean bean,Pager<CardBean> pager);
	
}
