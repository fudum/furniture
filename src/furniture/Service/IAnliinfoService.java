package furniture.Service;

import java.util.List;

import furniture.Bean.AnliinfoBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IAnliinfoService extends IBaseService{

	public AnliinfoBean selectOne(AnliinfoBean bean);
	
	public List<AnliinfoBean> selectList(AnliinfoBean bean);
	
	public int update(AnliinfoBean bean);
	
	public int insert(AnliinfoBean bean);
	
	public int delete(AnliinfoBean bean);
	
	public Pager<AnliinfoBean> selectPage(AnliinfoBean bean,Pager<AnliinfoBean> pager);
	
}
