package furniture.Service;

import java.util.List;

import furniture.Bean.CategoryBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ICategoryService extends IBaseService{

	public CategoryBean selectOne(CategoryBean bean);
	
	public List<CategoryBean> selectList(CategoryBean bean);
	
	public int update(CategoryBean bean);
	
	public int insert(CategoryBean bean);
	
	public int delete(CategoryBean bean);
	
	public Pager<CategoryBean> selectPage(CategoryBean bean,Pager<CategoryBean> pager);
	
}
