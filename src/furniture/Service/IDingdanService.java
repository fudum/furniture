package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.DingdanBean;

public interface IDingdanService extends IBaseService{

	public DingdanBean selectOne(DingdanBean bean);
	
	public List<DingdanBean> selectList(DingdanBean bean);
	
	public int update(DingdanBean bean);
	
	public int insert(DingdanBean bean);
	
	public int delete(DingdanBean bean);
	
	public Pager<DingdanBean> selectPage(DingdanBean bean,Pager<DingdanBean> pager);
	
}
