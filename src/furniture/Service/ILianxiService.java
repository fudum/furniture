package furniture.Service;

import java.util.List;

import furniture.Bean.LianxiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ILianxiService extends IBaseService{

	public LianxiBean selectOne(LianxiBean bean);
	
	public List<LianxiBean> selectList(LianxiBean bean);
	
	public int update(LianxiBean bean);
	
	public int insert(LianxiBean bean);
	
	public int delete(LianxiBean bean);
	
	public Pager<LianxiBean> selectPage(LianxiBean bean,Pager<LianxiBean> pager);
	
}
