package furniture.Service;

import java.util.List;

import furniture.Bean.XinwenBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IXinwenService extends IBaseService{

	public XinwenBean selectOne(XinwenBean bean);
	
	public List<XinwenBean> selectList(XinwenBean bean);
	
	public int update(XinwenBean bean);
	
	public int insert(XinwenBean bean);
	
	public int delete(XinwenBean bean);
	
	public Pager<XinwenBean> selectPage(XinwenBean bean,Pager<XinwenBean> pager);
	
}
