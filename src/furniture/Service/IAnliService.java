package furniture.Service;

import java.util.List;

import furniture.Bean.AnliBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IAnliService extends IBaseService{

	public AnliBean selectOne(AnliBean bean);
	
	public List<AnliBean> selectList(AnliBean bean);
	
	public int update(AnliBean bean);
	
	public int insert(AnliBean bean);
	
	public int delete(AnliBean bean);
	
	public Pager<AnliBean> selectPage(AnliBean bean,Pager<AnliBean> pager);
	
}
