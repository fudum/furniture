package furniture.Service;

import java.util.List;

import framework.base.common.Pager;
import framework.base.service.IBaseService;
import furniture.Bean.NoticeBean;

public interface INoticeService extends IBaseService{

	public NoticeBean selectOne(NoticeBean bean);
	
	public List<NoticeBean> selectList(NoticeBean bean);
	
	public int update(NoticeBean bean);
	
	public int insert(NoticeBean bean);
	
	public int delete(NoticeBean bean);
	
	public Pager<NoticeBean> selectPage(NoticeBean bean,Pager<NoticeBean> pager);
	
}
