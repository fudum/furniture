package furniture.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import furniture.Bean.UserBean;
import furniture.Common.Impl.CommonConstant;

public class UserSessionInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
		UserBean user = (UserBean) request.getSession().getAttribute("user_session");
		if(user == null){
			response.sendRedirect(CommonConstant.pre + "/home/index" + CommonConstant.suf);			
			return false;
		}
		else {
			return true;
		}
	}

}
